<script>
    jQuery(document).ready(function ($) {
        $(".iconbox a").click(function () {
            $(".togglebox").toggle();
        });
        $(".search-tradesmen a").click(function () {
            $(".search_sec input").attr("placeholder", "Search Tradesmen").blur();
        });
        $(".find-jobs a").click(function () {
            $(".search_sec input").attr("placeholder", "Find Jobs").blur();
        });
    });

</script>
<?php
session_start();
$_SESSION["find-jobs"] = "Find Jobs";
$_SESSION["search-tradesmen"] = "Search Tradesmen";

// echo "<pre>"; print_r($_SESSION);
/**
 * @package     Joomla.Site
 * @subpackage  mod_search
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Including fallback code for the placeholder attribute in the search field.
JHtml::_('jquery.framework');
JHtml::_('script', 'system/html5fallback.js', false, true);

if ($width) {
    $moduleclass_sfx .= ' ' . 'mod_search' . $module->id;
    $css = 'div.mod_search' . $module->id . ' input[type="search"]{ width:auto; }';
    JFactory::getDocument()->addStyleDeclaration($css);
    $width = ' size="' . $width . '"';
} else {
    $width = '';
}
?>
<div class="search<?php echo $moduleclass_sfx ?>">
    <form action="<?php echo JRoute::_('index.php'); ?>" method="get" class="form-inline">


        <?php
        $output = '<label for="mod-search-searchword" class="element-invisible">' . $label . '</label> ';
        $output .= '<div class="search_sec"><div class="iconbox"><a href="#"><img src="images/search.png"/><img class="search-arrow" src="images/dropdown-arrow.png"/></a></div><input name="searchword" id="mod-search-searchword" maxlength="' . $maxlength . '"  class="inputbox search-query" type="search"' . $width;
        $output .= ' placeholder="' . $text . '" /></div>';
        ?>



        <?php
        if ($button) :
            if ($imagebutton) :
                $btn_output = ' <input type="image" alt="' . $button_text . '" class="button" src="' . $img . '" onclick="this.form.searchword.focus();"/>';
            else :
                $btn_output = ' <button class="button btn btn-primary" onclick="this.form.searchword.focus();">' . $button_text . '</button>';
            endif;

            switch ($button_pos) :
                case 'top' :
                    $output = $btn_output . '<br />' . $output;
                    break;

                case 'bottom' :
                    $output .= '<br />' . $btn_output;
                    break;

                case 'right' :
                    $output .= $btn_output;
                    break;

                case 'left' :
                default :
                    $output = $btn_output . $output;
                    break;
            endswitch;

        endif;

        echo $output;
        ?>
        <div class="togglebox" style="display: none;">
            <ul>
                <li class="search-tradesmen">
                    <a href="#">Search Tradesmen</a>
                </li>
                <li class="find-jobs">
                    <a href="#">Find Jobs</a>
                </li>
            </ul>

        </div>
        <input type="hidden" name="task" value="search" />
        <input type="hidden" name="option" value="com_search" />
        <input type="hidden" name="Itemid" value="" />
        <input type="hidden" name="type" value="<?php echo $text; ?>" />
        <input type="hidden" name="tabname" value="">

    </form>
</div>
