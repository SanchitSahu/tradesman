<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_reviewstream
 * @copyright   Copyright (C) 2015 Grade Us, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class ModReviewStreamHelper
{

    public static function getReviews(&$params) {
        if(JHttpTransportCurl::isSupported()) {
            $base_url = "https://www.grade.us/api/v1/reviews";
            $token = $params->get('rs_api_token');
            $type = $params->get('rs_type');
            $format = $params->get('rs_schema');
            $show_aggregate_rating = $params->get('rs_show_aggregate_rating');
            $show_reviews = $params->get('rs_show_reviews');
            $path = $params->get('rs_path');
            $count = $params->get('rs_default_count');
            $query = '/'.$path.'/?count='.$count;
            while(strpos($query, '//') > -1) {
                $query = str_replace('//', '/', $query);
            }
            $url = $base_url.$query;
            $document = JFactory::getDocument();
            $document->addStyleSheet('https://static.grade.us/assets/reviewstream.css?v='.date('ymd'));
            $headers = array(
                'Authorization' => 'Token token='.$token,
                'Accept' => 'application/json',
            );
            $options = new Joomla\Registry\Registry();
            $http = JHttpFactory::getHttp($options);
            $http->setOption(CURLOPT_RETURNTRANSFER, 1);
            $req = $http->get($url, $headers, 30);
            $response = json_decode($req->body, true);
            $output = '';
            // Add aggregate rating content
            if($show_aggregate_rating) {
                $template_content = file_get_contents(dirname(__FILE__).'/tmpl/aggregate_rating_'.$format.'.php');
                $widget = '<div class="rating-widget"><span class="stars">';
                for($s=1;$s<=5;$s++) {
                    $class = 'star-md';
                    if($response['ratings_average'] >= $s) {

                    } elseif($response['ratings_average'] > $s-1) {
                        $class .= '-half';
                    } else {
                        $class .= '-off';
                    }
                    $widget .= '<i class="'.$class.'">&nbsp;</i>';
                }
                $widget .= '</span></div>';
                $template_content = str_replace('[[ratings_widget]]', $widget, $template_content);
                $template_content = str_replace('[[ratings_average]]', $response['ratings_average'], $template_content);
                $template_content = str_replace('[[ratings_max]]', $response['ratings_max'], $template_content);
                $template_content = str_replace('[[reviews_count]]', $response['reviews_count'], $template_content);
                $output .= $template_content;
            }
            // Add review content
            if($show_reviews) {
                $template_content = file_get_contents(dirname(__FILE__).'/tmpl/review_'.$format.'.php');
                $tokens = array('category', 'attribution', 'snippet', 'rating', 'url');
                foreach($response['reviews'] as $indresp) {
                $widget = '<div class="rating-widget"><span class="stars">';
                for($s=1;$s<=5;$s++) {
                  $class = 'star-sm';
                  if($indresp['rating'] >= $s) {
                  } elseif($indresp['rating'] > $s-1) {
                    $class .= '-half';
                  } else {
                    $class .= '-off';
                  }
                  $widget .= '<i class="'.$class.'">&nbsp;</i>';
                }
                $widget .= '</span></div>';
                $tempcontent = $template_content;
                $dt = date('m/d/Y', strtotime($indresp['date']));
                $tempcontent = str_replace('[[reviewdate]]', $dt, $tempcontent);
                $tempcontent = str_replace('[[ratingwidget]]', $widget, $tempcontent);
                foreach($tokens as $token) {
                  $tempcontent = str_replace('[['.$token.']]', $indresp[$token], $tempcontent);
                }
                $output .= $tempcontent;
                }
            }
            $template_wrapper = file_get_contents(dirname(__FILE__).'/tmpl/wrapper_'.$format.'.php');
            $template_wrapper = str_replace('[[type]]', $type, $template_wrapper);
            $output = str_replace('[[content]]', $output, $template_wrapper);
            $output = str_replace('[[name]]', $response['name'], $output);
            return $output;           
        } else {
            return "Curl not enabled.";
        }
    }
}