<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_reviewstream
 * @copyright   Copyright (C) 2015 Grade Us, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$reviews = ModReviewStreamHelper::getReviews($params);
require JModuleHelper::getLayoutPath('mod_reviewstream', $params->get('layout', 'default'));