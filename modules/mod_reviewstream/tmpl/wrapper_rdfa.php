<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_reviewstream
 * @copyright   Copyright (C) 2015 Grade Us, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
  defined('_JEXEC') or die('Restricted access');
?>
<div vocab="http://schema.org" typeof="[[type]]" id="reviewstream">
<meta property="name" content="[[name]]">
[[content]]
</div>
