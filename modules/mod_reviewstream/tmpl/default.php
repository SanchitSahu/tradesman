<?php 
/**
 * @package     Joomla.Site
 * @subpackage  mod_reviewstream
 * @copyright   Copyright (C) 2015 Grade Us, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die; ?>
<div class="rs"><?php echo $reviews; ?></div>