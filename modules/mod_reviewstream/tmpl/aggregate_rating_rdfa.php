<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_reviewstream
 * @copyright   Copyright (C) 2015 Grade Us, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
  defined('_JEXEC') or die('Restricted access');
?>
  <div class="aggregate">
    [[ratings_widget]]
    <div class="aggregate-text">
      <span property="name">[[name]]</span> is rated
      <span property="aggregateRating" typeof="AggregateRating">
        <span property="ratingValue">[[ratings_average]]</span> out of
        <span property="bestRating">[[ratings_max]]</span> based on
        <span property="reviewCount">[[reviews_count]]</span> reviews from around the Web.
      </span>
    </div>
  </div>
