<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_reviewstream
 * @copyright   Copyright (C) 2015 Grade Us, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
  defined('_JEXEC') or die('Restricted access');
?>
<div itemscope itemtype="http://schema.org/[[type]]" id="reviewstream">
<meta itemprop="name" content="[[name]]">
[[content]]
</div>
