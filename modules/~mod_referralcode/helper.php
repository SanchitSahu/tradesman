<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_referralcode
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_referralcode
 *
 * @package     Joomla.Site
 * @subpackage  mod_referralcode
 * @since       1.5
 */
class ModReferralcodeHelper
{
	/**
	 * Retrieve list of referralcode
	 *
	 * @param   \Joomla\Registry\Registry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getList(&$params)
	{
		JModelLegacy::addIncludePath(JPATH_ROOT . '/components/com_referralcode/models', 'ReferralcodeModel');
		$document = JFactory::getDocument();
		$app      = JFactory::getApplication();
		$keywords = explode(',', $document->getMetaData('keywords'));

		$model = JModelLegacy::getInstance('ReferralCode', 'ReferralcodeModel', array('ignore_request' => true));
		$model->setState('filter.code', (int) $params->get('code'));
		$model->setState('filter.title', $params->get('title', array()));
		$model->setState('list.limit', (int) $params->get('count', 1));
		//$model->setState('list.start', 0);
		$model->setState('filter.ordering', $params->get('ordering'));
		$model->setState('filter.tag_search', $params->get('tag_search'));
		$model->setState('filter.keywords', $keywords);
		$model->setState('filter.language', $app->getLanguageFilter());

		$banners = $model->getItems();
		$model->impress();

		return $banners;
	}
}
