<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_referralcode
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$headerText = trim($params->get('header_text'));
$footerText = trim($params->get('footer_text'));

require_once JPATH_ADMINISTRATOR . '/components/com_referralcode/helpers/referralcode.php';
ReferralcodeHelper::updateReset();
$list = &ModReferralcodeHelper::getList($params);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_referralcode', $params->get('layout', 'default'));
