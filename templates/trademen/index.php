<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.trademen
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/style.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/responsive-style.css');
$jbuser = JblanceHelper::get('helper.user');
$user = JFactory::getUser();

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8')) {
    $span = "span6";
} elseif ($this->countModules('position-7') && !$this->countModules('position-8')) {
    $span = "span9";
} elseif (!$this->countModules('position-7') && $this->countModules('position-8')) {
    $span = "span9";
} else {
    $span = "span12";
}

// Logo file or site title param
if ($this->params->get('logoFile')) {
    $logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
} elseif ($this->params->get('sitetitle')) {
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle')) . '</span>';
} else {
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<?php
$current_userid = $user->id;

$db = JFactory::getDbo();

$query = $db->getQuery(true);

$query->select($db->quoteName(array('ug_id')));
$query->from($db->quoteName('#__jblance_user'));
$query->where($db->quoteName('user_id') . " = " . $db->quote($current_userid));

$db->setQuery($query);

$results = $db->loadObjectList();

foreach ($results as $result) {
    $users_group = $result->ug_id;
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>

        <?php if ($users_group == 1) { ?>
            <style>
                .jbMenuBar #jbnav li:nth-child(3), .jbMenuBar #jbnav li:nth-child(4) ul li:nth-child(3) {
                    display: none !important;
                }
            </style>
        <?php } ?>
        <?php
        if (!$user->guest) {
            $planStatus = JblanceHelper::planStatus($user->id);

            if ($planStatus == '1') {
                ?>
                <style>
                    #jbMenu {
                        display:none !important;
                    }
                    .text-background, .contact-us {
                        display: none !important;
                    }
                    .header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201
                    {
                        display: none !important;
                    }
                    .search-options {
                        display: none !important;
                    }
                </style>
            <?php } elseif ($planStatus == '2') {
                ?>
                <style>
                    #jbMenu {
                        display:none !important;
                    }
                    .text-background, .contact-us {
                        display: none !important;
                    }
                    .header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201
                    {
                        display: none !important;
                    }
                    .search-options {
                        display: none !important;
                    }
                </style>
                <?php
            }
        }
        ?>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <jdoc:include type="head" />
        <?php // Use of Google Font  ?>
        <?php if ($this->params->get('googleFont')) : ?>
            <link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
            <style type="text/css">
                h1,h2,h3,h4,h5,h6,.site-title{
                    font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
                }
            </style>
        <?php endif; ?>
        <?php // Template color ?>
        <?php if ($this->params->get('templateColor')) : ?>
            <style type="text/css">
                body.site
                {
                    border-top: 3px solid <?php echo $this->params->get('templateColor'); ?>;
                    background-color: <?php echo $this->params->get('templateBackgroundColor'); ?>
                }
                a
                {
                    color: <?php echo $this->params->get('templateColor'); ?>;
                }
                .navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
                .btn-primary
                {
                    background: <?php echo $this->params->get('templateColor'); ?>;
                }
                .navbar-inner
                {
                    -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
                    -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
                    box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
                }
            </style>
        <?php endif; ?>
        <!--[if lt IE 9]>
                <script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
        <![endif]-->

        <script>
            /*jQuery(document).ready(function($){
             jQuery( ".dropdownbutton" ).click(function(){
             jQuery(".menu_dropdownmenu" ).slideToggle("fast");
             });
             });	*/
        </script>
    </head>

    <body class="site <?php
    echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
    echo ($this->direction == 'rtl' ? ' rtl' : '');
    ?>">
        <!-- Body -->
        <div class="body">
            <!-- Header -->
            <div class="container header-container">
                <header class="header" role="banner">
                    <div class="header-inner clearfix">

                        <a class="brand pull-left" href="<?php echo $this->baseurl; ?>/">
                            <?php
                            $app = JFactory::getApplication();
                            $menu = $app->getMenu();
                            if ($menu->getActive() == $menu->getDefault()) {
                                echo $logo;
                            } else {
                                echo '<jdoc:include type="modules" name="position-6" style="xhtml" />';
                            }
                            ?>

                            <?php if ($this->params->get('sitedescription')) : ?>
                                <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
                            <?php endif; ?>
                        </a>						
                        <div class="logo-2">

                        </div>
                        <div class="header-search pull-right">

                            <div class="user-dropdown-menu">
                                <?php
                                //$str=substr($user->name, 0, strrpos($user->name, ' '));
                                $item = explode(" ", $user->name);
                                ?>
                                <?php if ($user->id > 0) { ?>
                                    <div class="login-greetings dropdownbutton"><a><?php echo "Hi, " . $item[0]; ?>  <img src="images/dropdown-arrow.png"></a>
                                        <?php
                                        $attrib = 'width=90 height=90 class="img-polaroid avatar_thumb"';
                                        $avatar = JblanceHelper::getLogo($user->id, $attrib);
                                        echo $avatar;
                                        ?>
                                    </div>
                                    <?php if ($this->countModules('position-9')) : ?>
                                        <jdoc:include type="modules" name="position-9" style="none1" />
                                    <?php endif; ?>

                                <?php } else { ?>
                                    <?php if ($this->countModules('position-0')) : ?>

                                        <jdoc:include type="modules" name="position-0" style="none1" />
                                    <?php endif; ?>

                                <?php } ?>
                            </div>


                        </div>
                    </div>
                    <div class="header-search-text">
                        <h1>Work or Workforce? Find the right person for the right job</h1>
                    </div>
                </header>
            </div>
            <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                <?php if ($this->countModules('position-1')) : ?>
                    <!-- <nav class="navigation" role="navigation">
                            <div class="navbar pull-left">
                                    <a class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                    </a>
                            </div>
                            <div class="nav-collapse">
                                    <jdoc:include type="modules" name="position-1" style="none" />
                            </div>
                    </nav> -->
                <?php endif; ?>
                <jdoc:include type="modules" name="banner" style="xhtml" />
                <div class="row-fluid">
                    <div class="container">
                        <?php
                        if (!$user->guest) {
                            ?>
                            <?php if ($this->countModules('position-5')) : ?>
                                <jdoc:include type="modules" name="position-5" style="well" />
                            <?php endif; ?>
                            <?php
                        } else {
                            ?>
                            <?php if ($this->countModules('position-10')) : ?>
                                <jdoc:include type="modules" name="position-10" style="xhtml" />
                            <?php endif; ?>
                            <?php
                        }
                        ?>
                    </div>
                    <?php if ($this->countModules('position-8')) : ?>
                        <!-- Begin Sidebar -->
                        <div id="sidebar" class="span3">
                            <div class="sidebar-nav">
                                <jdoc:include type="modules" name="position-8" style="xhtml" />
                            </div>
                        </div>
                        <!-- End Sidebar -->
                    <?php endif; ?>
<!-- <main id="content" role="main" class="<?php echo $span; ?>"> -->
                    <!-- Begin Content -->
                    <jdoc:include type="modules" name="position-3" style="xhtml" />
                    <jdoc:include type="message" />
                    <jdoc:include type="component" />
                    <jdoc:include type="modules" name="position-2" style="none" />
                    <!-- End Content -->
                    </main>
                    <?php if ($this->countModules('position-7')) : ?>
                        <div id="aside" class="span3">
                            <!-- Begin Right Sidebar -->
                            <jdoc:include type="modules" name="position-7" style="well" />
                            <!-- End Right Sidebar -->
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="col-lg-12 clients-review-section header-container">
            <div class="container clients-review-heading">
                <h1>What our users say ?</h1>
                <?php //if ($this->countModules('position-4')) : ?>
                <div class="com-sm-12">
                    <div class="clients-review">
                        <!-- <jdoc:include type="modules" name="position-4" style="xhtml" /> -->
                        <p class="comma">&bdquo;</p> <p class="feedback">It is long established fact that a rader will be distractred by the readable content of a page when looking at its layout.The point of using Loreum ipsum is that it has a more-or-less normal distribution of lettrs</p> <p class="comma">&bdquo;</p>
                        <p class="feedback-user-name">Posted By: David Makvan</p>
                    </div>
                </div>
                <jdoc:include type="modules" name="position-11" style="xhtml" />
                <?php //endif;  ?>
            </div>
        </div>
        <!-- Footer -->
        <footer class="footer homepage-footer" role="contentinfo">
            <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                <div class="col-sm-12 footer-section">
                    <div class="">
                        <div class="col-sm-4">
                            <div class="row">
                                    <!-- <p class="pull-right">
                                            <a href="#top" id="back-top">
                                <?php //echo JText::_('TRADEMEN_BACKTOTOP');  ?>
                                            </a>
                                    </p> -->
                                <div class="footer-logo">
                                    <a href = "/"> <?php echo $logo; ?> </a>
                                </div>
                                <p>
                                    It is long established fact that a reader will be distracted by the readable content of a page when looking at its layout.The point of
                                </p>
                                <p>
                                    &copy; <?php echo date('Y'); ?> <?php echo $sitename; ?>
                                </p>
                                <div class="col-xs-12 col-sm-12 footer-app-option">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6">
                                            <a href="https://play.google.com" target="blank"><img src="images/android.png"></a>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <a href="https://itunes.apple.com" target="blank"><img src="images/iosfooter.png"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <jdoc:include type="modules" name="footer" style="none" />
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <footer class="footer otherpages-footer" role="contentinfo">
            <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                <div class="col-sm-12 footer-section">
                    <div class="row">
                        <p>
                            &copy; <?php echo date('Y'); ?> <?php echo $sitename; ?>
                        </p>
                        <div class="col-sm-4">	

                        </div>
                        <div class="col-sm-4">	
                            <a href ="#">Terms and Condition</a>
                        </div>
                        <div class="col-sm-4">	

                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script>
            jQuery.noConflict();
            jQuery(document).ready(function ($) {
                jQuery('.header-inner .header-search ul.nav.menu_dropdownmenu').addClass('hide');
                jQuery('header.header .header-inner div.header-search .user-dropdown-menu .login-greetings.dropdownbutton').click(function () {
                    jQuery('.header-inner .header-search ul.nav.menu_dropdownmenu').toggleClass('hide');
                });
            });
        </script>
        <style>

            @media only screen and (min-width: 991px) and (max-width: 1024px){
                .view-user .chzn-container.chzn-container-multi {width: 100% !important;}
            }
            @media only screen and (min-width: 768px) and (max-width: 990px){
                .view-user .chzn-container.chzn-container-multi {width: 100% !important;}
            }

        </style>
        <jdoc:include type="modules" name="debug" style="none" />
    </body>
</html>