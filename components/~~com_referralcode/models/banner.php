<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

/**
 * Banner model for the Joomla Banners component.
 *
 * @since  1.5
 */
class ReferralcodeModelBanner extends JModelLegacy
{
	/**
	 * Cached item object
	 *
	 * @var    object
	 * @since  1.6
	 */
	protected $_item;

	/**
	 * Clicks the URL, incrementing the counter
	 *
	 * @return  void
	 *
	 * @since   1.5
	 */
	public function click()
	{
		$id = $this->getState('banner.id');

		// Update click count
		$db = $this->getDbo();
		$query = $db->getQuery(true)
			->update('#__banners')
			->set('clicks = (clicks + 1)')
			->where('id = ' . (int) $id);

		$db->setQuery($query);

		try
		{
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			JError::raiseError(500, $e->getMessage());
		}

		$item = $this->getItem();

		
	}

	/**
	 * Get the data for a banner.
	 *
	 * @return  object
	 *
	 * @since   1.6
	 */
	public function &getItem()
	{
		if (!isset($this->_item))
		{
			$cache = JFactory::getCache('com_referralcode', '');

			$id = $this->getState('referralcode.id');

			$this->_item = $cache->get($id);

			if ($this->_item === false)
			{
				// Redirect to banner url
				$db = $this->getDbo();
				$query = $db->getQuery(true)
					->select(
							'a.code as code,' .
							'a.title as title'
					)
					->from('#__referralcode as a')
					->where('a.id = ' . (int) $id)

				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (RuntimeException $e)
				{
					JError::raiseError(500, $e->getMessage());
				}

				$this->_item = $db->loadObject();
				$cache->store($this->_item, $id);
			}
		}

		return $this->_item;
	}

	/**
	 * Get the code for a banner
	 *
	 * @return  string
	 *
	 * @since   1.5
	 */
	public function getCode()
	{
		$item = $this->getItem();
		$url = $item->Code;

		return $url;
	}
}
