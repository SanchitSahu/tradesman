<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_referralcode
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

/**
 * referralcode model for the Joomla referralcode component.
 *
 * @since  1.6
 */
class ReferralcodeModelBanners extends JModelList
{
	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'referralcode', $prefix = 'g6t1u_', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.tag_search');
		$id .= ':' . $this->getState('filter.code');
		$id .= ':' . serialize($this->getState('filter.category_id'));
		$id .= ':' . serialize($this->getState('filter.keywords'));

		return parent::getStoreId($id);
	}

	/**
	 * Method to get a JDatabaseQuery object for retrieving the data set from a database.
	 *
	 * @return  JDatabaseQuery   A JDatabaseQuery object to retrieve the data set.
	 *
	 * @since   1.6
	 */
	protected function getListQuery()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$ordering = $this->getState('filter.ordering');
		$tagSearch = $this->getState('filter.tag_search');
		$cid = $this->getState('filter.code');
		$categoryId = $this->getState('filter.title');
		$keywords = $this->getState('filter.keywords');
		$randomise = ($ordering == 'random');
		$nullDate = $db->quote($db->getNullDate());

		$query->select(
			'a.id as id,' .
				'a.title as title,' .
				'a.code as code,' .
				'a.description as description,' .
				'a.company as company,' .
				'a.tradesmen as tradesmen'
		)
			->from('#__referralcode as a');

		

		// Filter by a single or group of categories
		/*if (is_numeric($categoryId))
		{
			$type = $this->getState('filter.category_id.include', true) ? '= ' : '<> ';

			// Add subcategory check
			$includeSubcategories = $this->getState('filter.subcategories', false);
			$categoryEquals = 'a.catid ' . $type . (int) $categoryId;

			if ($includeSubcategories)
			{
				$levels = (int) $this->getState('filter.max_category_levels', '1');

				// Create a subquery for the subcategory list
				$subQuery = $db->getQuery(true);
				$subQuery->select('sub.id')
					->from('#__categories as sub')
					->join('INNER', '#__categories as this ON sub.lft > this.lft AND sub.rgt < this.rgt')
					->where('this.id = ' . (int) $categoryId)
					->where('sub.level <= this.level + ' . $levels);

				// Add the subquery to the main query
				$query->where('(' . $categoryEquals . ' OR a.catid IN (' . $subQuery->__toString() . '))');
			}
			else
			{
				$query->where($categoryEquals);
			}
		}
		elseif ((is_array($categoryId)) && (count($categoryId) > 0))
		{
			JArrayHelper::toInteger($categoryId);
			$categoryId = implode(',', $categoryId);

			if ($categoryId != '0')
			{
				$type = $this->getState('filter.category_id.include', true) ? 'IN' : 'NOT IN';
				$query->where('a.catid ' . $type . ' (' . $categoryId . ')');
			}
		}*/

		if ($tagSearch)
		{
			if (count($keywords) == 0)
			{
				$query->where('0');
			}
			else
			{
				$temp = array();
				$config = JComponentHelper::getParams('com_referralcode');
				$prefix = $config->get('metakey_prefix');

				

				foreach ($keywords as $keyword)
				{
					$keyword = trim($keyword);
					$condition1 = "a.own_prefix=1 "
						. " AND " . ($prefix == substr($keyword, 0, strlen($prefix)) ? '1' : '0');

					$condition2 = "a.metakey REGEXP '[[:<:]]" . $db->escape($keyword) . "[[:>:]]'";

					if ($cid)
					{
						$condition2 .= " OR cl.metakey REGEXP '[[:<:]]" . $db->escape($keyword) . "[[:>:]]'";
					}

					if ($categoryId)
					{
						$condition2 .= " OR cat.metakey REGEXP '[[:<:]]" . $db->escape($keyword) . "[[:>:]]'";
					}

					$temp[] = "($condition1) AND ($condition2)";
				}

				$query->where('(' . implode(' OR ', $temp) . ')');
			}
		}

		$query->order('a.id DESC,');

		return $query;
	}

	/**
	 * Get a list of banners.
	 *
	 * @return  array
	 *
	 * @since   1.6
	 */
	public function getItems()
	{
		if (!isset($this->cache['items']))
		{
			$this->cache['items'] = parent::getItems();

			foreach ($this->cache['items'] as &$item)
			{
				$parameters = new Registry;
				$parameters->loadString($item->params);
				$item->params = $parameters;
			}
		}

		return $this->cache['items'];
	}

	/**
	 * Makes impressions on a list of banners
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function impress()
	{
		$trackDate = JFactory::getDate()->format('Y-m-d H');
		$items = $this->getItems();
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		foreach ($items as $item)
		{
			// Increment impression made
			$id = $item->id;
			$query->clear()
				->update('#__referralcode')
				->set('impmade = (impmade + 1)')
				->where('id = ' . (int) $id);
			$db->setQuery($query);

			try
			{
				$db->execute();
			}
			catch (RuntimeException $e)
			{
				JError::raiseError(500, $e->getMessage());
			}

			// Track impressions
			$trackImpressions = $item->track_impressions;

			if ($trackImpressions < 0 && $item->cid)
			{
				$trackImpressions = $item->client_track_impressions;
			}

			if ($trackImpressions < 0)
			{
				$config = JComponentHelper::getParams('com_banners');
				$trackImpressions = $config->get('track_impressions');
			}

			if ($trackImpressions > 0)
			{
				// Is track already created?
				$query->clear()
					->select($db->quoteName('count'))
					->from('#__banner_tracks')
					->where('track_type=1')
					->where('banner_id=' . (int) $id)
					->where('track_date=' . $db->quote($trackDate));

				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (RuntimeException $e)
				{
					JError::raiseError(500, $e->getMessage());
				}

				$count = $db->loadResult();

				$query->clear();

				if ($count)
				{
					// Update count
					$query->update('#__banner_tracks')
						->set($db->quoteName('count') . ' = (' . $db->quoteName('count') . ' + 1)')
						->where('track_type=1')
						->where('banner_id=' . (int) $id)
						->where('track_date=' . $db->quote($trackDate));
				}
				else
				{
					// Insert new count
					$query->insert('#__banner_tracks')
						->columns(
							array(
								$db->quoteName('count'), $db->quoteName('track_type'),
								$db->quoteName('banner_id'), $db->quoteName('track_date')
							)
						)
						->values('1, 1, ' . (int) $id . ', ' . $db->quote($trackDate));
				}

				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (RuntimeException $e)
				{
					JError::raiseError(500, $e->getMessage());
				}
			}
		}
	}
}
