<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	30 May 2012
 * @file name	:	views/message/tmpl/inbox.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	Inbox of Private Messages (jblance)
 */
defined('_JEXEC') or die('Restricted access');

JHtml::_('jquery.framework');
JHtml::_('behavior.formvalidator');
JHtml::_('bootstrap.tooltip');

$doc = JFactory::getDocument();
$doc->addScript("components/com_jblance/js/utility.js");
$doc->addScript("components/com_jblance/js/upclick-min.js");

$user = JFactory::getUser();

$config = JblanceHelper::getConfig();
$dformat = $config->dateFormat;
$showUsername = $config->showUsername;
$reviewMessages = $config->reviewMessages;
$nameOrUsername = ($showUsername) ? 'username' : 'name';

//echo "<pre>";
//print_r($msgList);
//exit;
if ($user->id == $this->parent->idFrom) {
    $idFrom = $this->parent->idFrom;
    $idTo = $this->parent->idTo;
} else {
    $idFrom = $this->parent->idTo;
    $idTo = $this->parent->idFrom;
}

$link_compose = JRoute::_('index.php?option=com_jblance&view=message&layout=compose');

JblanceHelper::setJoomBriToken();
?>
<form class="message-section" action="<?php echo JRoute::_('index.php'); ?>" method="post" name="userForm">
    <div class="pull-right"><a href="<?php echo $link_compose; ?>" class="btn btn-primary"><span><?php echo "New Message"; ?></span></a></div>
    <div class="jbl_h3title message-heading"><?php echo JText::_('COM_JBLANCE_MESSAGE'); ?></div>
    <?php
    //echo JHtml::_('tabs.start', 'panel-tabs', array('useCookie'=>'0'));
    $newTitle = ($this->newMsg > 0) ? ' (<b>' . JText::sprintf('COM_JBLANCE_COUNT_NEW', $this->newMsg) . '</b>)' : '';
    //echo JHtml::_('tabs.panel', JText::_('COM_JBLANCE_RECEIVED').$newTitle, 'received'); 
    ?>
    <div class="col-xs-12 col-sm-12 messages-body">
            <!-- <thead>
                    <tr>
                            <th><?php echo JText::_('COM_JBLANCE_FROM'); ?></th>
                            <th><?php echo JText::_('COM_JBLANCE_SUBJECT'); ?></th>
                            <th><?php echo JText::_('COM_JBLANCE_DATE'); ?></th>
                            <th><?php echo JText::_('COM_JBLANCE_ACTION'); ?></th>
                    </tr>			
            </thead> -->
        <?php
        if (count($this->msgs) == 0) {  //Called if there are no messages -> Shows a text that spreads over the whole table
            ?>
            <div class="error-message"><p class="text-center"><?php echo JText::_("COM_JBLANCE_NO_MESSAGES"); ?></p></div>
            <?php
        }
        $k = 0;
        for ($i = 0, $x = count($this->msgs); $i < $x; $i++) {
            $msg = $this->msgs[$i];
            $userFrom = JFactory::getUser($msg->idFrom);
            $userTo = JFactory::getUser($msg->idTo);

            //if the current user is different, then show that name
            if ($user->id == $msg->idFrom)
                $userInfo = JFactory::getUser($msg->idTo);
            else
                $userInfo = JFactory::getUser($msg->idFrom);

            $link_read = JRoute::_('index.php?option=com_jblance&view=message&layout=read&id=' . $msg->id);

            $newMsg = JblanceHelper::countUnreadMsg($msg->id);
            //echo "<pre>"; print_r($userInfo);
            ?>

            <div class="col-sm-12 single-message" id="jbl_feed_item_<?php echo $msg->id; ?>">
                <a href="<?php echo $link_read; ?>">
                        <div class="message-user-image col-sm-1"><!--<img src="/tm/images/message3.png">-->
                        <?php
                        $attrib = 'width=80 height=80 class="img-polaroid"';
                        $avatar = JblanceHelper::getLogo($userInfo->id, $attrib);
                        echo!empty($avatar) ? LinkHelper::GetProfileLink($row->id, $avatar, '', '', ' pull-left') : '&nbsp;';
                        ?>
                        <div class="col-sm-12 message-user-name"><?php echo $userInfo->name; ?></div>
                    </div>
                    <div class="col-xs-7 col-sm-8 message-title"><?php echo ($msg->approved == 1) ? $msg->subject : '<small>' . JText::_('COM_JBLANCE_PRIVATE_MESSAGE_WAITING_FOR_MODERATION') . '</small>'; ?> <?php echo ($newMsg > 0) ? '<span class="label label-info">' . JText::sprintf('COM_JBLANCE_COUNT_NEW', $newMsg) . '</span>' : ''; ?>
                    </div>
                    <div class="col-xs-2 col-sm-2 pull-right">
                        <div class="inbox-date" nowrap="nowrap"><?php echo JHtml::_('date', $msg->date_sent, $dformat, true); ?>
                        </div>
                    </div>
                    <div class="col-xs-7 col-sm-10 message-text">
                        <?php
                        //echo "<pre>"; print_r($msg);
                        if ($msg->approved == 1)
                            echo $msg->message;
                        else
                            echo '<small>' . JText::_('COM_JBLANCE_PRIVATE_MESSAGE_WAITING_FOR_MODERATION') . '</small>';
                        ?>
                    </div>
                    <div class="message-delete-button">
                        <span id="feed_hide_<?php echo $msg->id; ?>" class="help-inline">
                            <a class="" onclick="processMessage('<?php echo $msg->id; ?>', 'message.processmessage');" title="<?php echo JText::_('COM_JBLANCE_REMOVE'); ?>" href="javascript:void(0);"><i class="icon-remove"></i> Delete </a>
                        </span>
                    </div>
            </div>
            </a>
            <?php
            $k = 1 - $k;
        }
        ?>
    </div>
    <?php //echo JHtml::_('tabs.end');   ?>
    <?php if ($reviewMessages) : ?>
        <p class="jbbox-warning"><?php echo JText::_('COM_JBLANCE_MESSAGE_WILL_BE_MODERATED_BEFORE_SENT_TO_RECIPIENT'); ?></p>
    <?php endif; ?>

    <input type="hidden" name="option" value="com_jblance" />			
    <input type="hidden" name="task" value="message.sendmessage" />	
    <input type="hidden" name="idFrom" value="<?php echo $idFrom; ?>" />
    <input type="hidden" name="idTo" value="<?php echo $idTo; ?>" />
    <input type="hidden" name="id" value="0" />
    <input type="hidden" name="subject" value="<?php echo $this->parent->subject; ?>" />
    <input type="hidden" name="project_id" value="<?php echo $this->parent->project_id; ?>" />
    <input type="hidden" name="parent" value="<?php echo $this->parent->id; ?>" />
    <input type="hidden" name="type" value="<?php echo $this->parent->type; ?>" />
    <input type="hidden" name="return" value="<?php echo base64_encode(JFactory::getURI()->toString()); ?>" />
    <?php echo JHtml::_('form.token'); ?>
</form>


