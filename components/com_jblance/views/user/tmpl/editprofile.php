<script>
 jQuery(document).ready(function($) {
 	    $("#myTab li a").click(function(){
		    $('.row-2').remove();
		});

		$('.row-3').remove();

		$("#tab444 a").click(function(){
		    $('.form-actions').hide();
		});
		$("#tab333 a").click(function(){
		    $('.form-actions').show();
		});
		$("#tab222 a").click(function(){
		    $('.form-actions').show();
		});
		$("#tab111 a").click(function(){
		    $('.form-actions').show();
		});
		
		
 });
</script>

<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	22 March 2012
 * @file name	:	views/user/tmpl/editprofile.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	Edit profile (jblance)
 */
 defined('_JEXEC') or die('Restricted access');
 
 JHtml::_('jquery.framework');
 JHtml::_('behavior.formvalidator');
 JHtml::_('bootstrap.tooltip');
 
 $db = JFactory::getDbo();
 $doc 	 = JFactory::getDocument();
 $doc->addScript("components/com_jblance/js/utility.js");
 $doc->addScript("components/com_jblance/js/cropit.js"); 

 $user= JFactory::getUser();
 $model = $this->getModel();
 $select = JblanceHelper::get('helper.select');		// create an instance of the class SelectHelper
 
 $jbuser = JblanceHelper::get('helper.user');		// create an instance of the class UserHelper
 $userInfo = $jbuser->getUserGroupInfo($user->id, null);

 $upload_type = (empty($this->userInfo->picture)) ? 'NO_UPLOAD_CROP' : 'CROP_ONLY';

 
 $config 	  = JblanceHelper::getConfig();
 $currencysym = $config->currencySymbol;	
 $currencycod = $config->currencyCode;	
 $maxSkills   = $config->maxSkills;	
 $link_subscr_hist	= JRoute::_('index.php?option=com_jblance&view=membership&layout=planhistory');
 $link_buy_subscr	= JRoute::_('index.php?option=com_jblance&view=membership&layout=planadd');
 
 JHtml::_('formbehavior.chosen', '#id_category', null, array('max_selected_options' => $maxSkills, 'placeholder_text_multiple' => JText::_('COM_JBLANCE_PLEASE_SELECT_SKILLS_FROM_THE_LIST')));
 
 JText::script('COM_JBLANCE_CLOSE');
 
 JblanceHelper::setJoomBriToken();
if(!JBLANCE_FREE_MODE){
	if(!$user->guest){
		$planStatus = JblanceHelper::planStatus($user->id);
		
		if($planStatus == '1'){ ?>
		<div class="jbbox-warning">
			<?php echo JText::sprintf('COM_JBLANCE_USER_SUBSCRIPTION_EXPIRED', $link_buy_subscr); ?>
		</div>
		<style>
		#jbMenu {
			display:none !important;
		}
		.header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201,.header-search .user-dropdown-menu ul.nav.menu li.item-227
		{
			display: none !important;
		}
		</style>
	<?php }
	elseif($planStatus == '2'){ ?>
	<div class="jbbox-info">
			<?php echo JText::sprintf('COM_JBLANCE_USER_DONT_HAVE_ACTIVE_PLAN', $link_subscr_hist); ?>
		</div>
		<style>
		#jbMenu {
			display:none !important;
		}
		.header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201
		{
			display: none !important;
		}
		</style>
	<?php }
	}
} 
?>
<script type="text/javascript">
function check_val(){
   
    if(jQuery.trim(jQuery("#name").val())==''){ jQuery("#name").val(''); }
    if(jQuery.trim(jQuery("#address").val())==''){ jQuery("#address").val(''); }
    if(jQuery.trim(jQuery("#postcode").val())==''){ jQuery("#postcode").val(''); }
    if(jQuery.trim(jQuery("#mobile").val())==''){ jQuery("#mobile").val(''); }
if(!jQuery.isNumeric(jQuery("#mobile").val())) { jQuery("#mobile").val(''); }
if(!jQuery.isNumeric(jQuery("#custom_field_5").val())) { jQuery("#custom_field_5").val(''); }
    //if(jQuery.trim(jQuery("#biz_name").val())==''){ jQuery("#biz_name").val(''); }
    return true;
}

function validateForm(f){


    if(jQuery.trim(jQuery("#name").val())==''){ jQuery("#name").val(''); return false;}
    if(jQuery.trim(jQuery("#address").val())==''){ jQuery("#address").val(''); }
    if(jQuery.trim(jQuery("#postcode").val())==''){ jQuery("#postcode").val(''); }
    if(jQuery.trim(jQuery("#mobile").val())==''){ jQuery("#mobile").val(''); }
if(!jQuery.isNumeric(jQuery("#mobile").val())) { jQuery("#mobile").val(''); }
if(!jQuery.isNumeric(jQuery("#custom_field_5").val())) { jQuery("#custom_field_5").val(''); }
    //if(jQuery.trim(jQuery("#biz_name").val())==''){ jQuery("#biz_name").val(''); return false;}
    

/*	if(jQuery("#id_category").length){
		if(!jQuery("#id_category option:selected").length){
			alert('<?php echo JText::_('COM_JBLANCE_PLEASE_SELECT_SKILLS_FROM_THE_LIST', true); ?>');
			return false;
		}
	}


    if (document.formvalidator.isValid(f)) {
		
    }
    else {
	    var msg = '<?php echo JText::_('COM_JBLANCE_FIEDS_HIGHLIGHTED_RED_COMPULSORY', true); ?>';
	    if(jQuery("#rate").length && jQuery("#rate").hasClass("invalid")){
	    	msg = msg+'\n\n* '+'<?php echo JText::_('COM_JBLANCE_PLEASE_ENTER_AMOUNT_IN_NUMERIC_ONLY', true); ?>';
	    }
		alert(msg);
		return false;
    }*/
	return true;
}
<!--
<?php if($maxSkills > 0){ ?>
jQuery(document).ready(function($){
	if($("#id_category").length){
		$("#id_category").change(updateSkillCount);
		updateSkillCount();
	}
});
<?php } ?>


function updateSkillCount(){
	sel = jQuery("#id_category option:selected").length;
	jQuery("#skill_left_span").html(sel);
}

function editLocation(){
	jQuery("#level1").css("display", "inline-block").addClass("required");
}
//-->

                jQuery('#system-message-container #system-message div h4').hide();
</script>
<style>
@media only screen and (min-width: 320px) and (max-width: 766px){
.edit-profile-section .form-actions {position: initial !important;margin: 10px 0 0 0 !important;width: 100%;}
.edit-profile-section.row-fluid{height: auto !important;}
}
</style>
<?php //include_once(JPATH_COMPONENT.'/views/profilemenu.php'); ?>
	<div class="jbl_h3title profile-main-heading"><?php echo JText::_('COM_JBLANCE_PROFILE'); ?></div>
<div class="edit-profile-menu">
  <ul class="nav nav-tabs" id="myTab">
    <li id="tab111" class="col-xs-3 active"><a href="#1" data-toggle="tab"> User Details </a></li>
    <li id="tab222" class="col-xs-3"><a href="#2" data-toggle="tab"> Contact Details </a></li>
	<li id="tab333" class="col-xs-3"><a href="#3" data-toggle="tab"> Basic Information </a></li>
		<?php 
		$avatars = JblanceHelper::getAvatarIntegration();
		$link_edit_picture = $avatars->getEditURL();
		?>
	<!-- <li id="tab444" class="col-xs-3 <?php //echo ($layout == 'editpicture') ? 'active' : ''; ?>"><a href="<?php //echo $link_edit_picture; ?>" > Profile Picture </a></li> -->
	<li id="tab444" class="col-xs-3"><a href="#4" data-toggle="tab"> Profile Picture </a></li>
  </ul>
</div>
<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="editProfile" id="frmEditProfile" class="form-validate form-horizontal" onsubmit="return validateForm(this);" enctype="multipart/form-data" novalidate>
	<div class="tab-content row-fluid edit-profile-section">
    <div class="tab-pane active" id="1">
	<fieldset>
		<legend class="edit-profile-heading"><?php echo "User Details" ; ?></legend>
		<div class="edit-profile-body-section">
			<div class="control-group username">
				<label class="control-label"><?php echo JText::_('COM_JBLANCE_USERNAME'); ?>:</label>
				<div class="controls">
					<?php echo  $this->userInfo->username; ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="name"><?php echo JText::_('COM_JBLANCE_NAME'); ?></label>
				<div class="controls">
					<input class="input-medium required" type="text" name="name" id="name" onblur='return check_val();' value="<?php echo $this->userInfo->name; ?>" />
				</div>
			</div>
			<!-- Company Name should be visible only to users who can post job -->
			<?php if($userInfo->allowPostProjects) : ?>
			<div class="control-group">
				<label class="control-label" for="biz_name"><?php echo JText::_('COM_JBLANCE_BUSINESS_NAME'); ?></label>
				<div class="controls">
					<input class="input-medium required" type="text" name="biz_name" id="biz_name" onblur='return check_val();' value="<?php echo $this->userInfo->biz_name; ?>" />
				</div>
			</div>
			<?php endif; ?>
			<!-- Skills and hourly rate should be visible only to users who can work/bid -->
			<?php if($userInfo->allowBidProjects) : ?>
			<div class="control-group">
				<label class="control-label" for="rate"><?php echo "Rates" ; ?></label>
				<div class="controls">
					<div class="input-prepend input-append">
						<span class="add-on"><?php echo $currencysym; ?></span>
						<input class="input-mini validate-numeric" type="text" name="rate" id="rate" value="<?php echo $this->userInfo->rate; ?>" />
						<span class="add-on"><?php echo $currencycod.' / '.JText::_('COM_JBLANCE_HOUR'); ?></span>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="id_category"><?php echo "Skill Set" ; ?></label>
				<div class="controls">
					<?php if($maxSkills > 0){ ?>
					<div class="bid_project_left pull-left">
			    		<div><span id="skill_left_span" class="font26"><?php echo count(explode(',', $this->userInfo->id_category))?></span>/<span><?php echo $maxSkills; ?></span></div>
			    		<div><?php echo JText::_('COM_JBLANCE_SKILLS'); ?></div>
					</div>
					<div class="clearfix"></div>
					<?php } ?>
					<?php 
					//$attribs = 'class="input-medium required" size="20" multiple ';
					//$categtree = $select->getSelectCategoryTree('id_category[]', explode(',', $this->userInfo->id_category), 'COM_JBLANCE_PLEASE_SELECT', $attribs, '', true);
					//echo $categtree; 
					//$attribs = '';
					//$select->getCheckCategoryTree('id_category[]', explode(',', $this->userInfo->id_category), $attribs); ?>
					<?php
					$attribs = "class='input-xxlarge required' multiple";
					echo $select->getSelectCategoryTree('id_category[]', explode(',', $this->userInfo->id_category), '', $attribs, '', true);
					
					


					?>
				</div>
			</div>

			<?php $gval = $db->getQuery(true);
	 		$gval->select($db->quoteName(array('value')));
	 		$gval->from($db->quoteName('#__jblance_custom_field_value'));
	 		$gval->where( ($db->quoteName('fieldid') . ' = '. $db->quote('2')).'AND'. ($db->quoteName('userid') . ' = '. $db->quote($this->userInfo->user_id)) )  ;

	 		$db->setQuery($gval);
	 		$gender_val = $db->loadObjectList(); ?>

			<div class="control-group custom_gender">

				<label for="custom_field_2" class="control-label">Gender<span class="redfont"></span>:</label>
				<div class="controls controls-row">
					<label class="radio inline">
						<input type="radio" <?php if($gender_val[0]->value=='Male') { ?> checked="checked" <?php } ?> value="Male" name="custom_field_2"/><p>Male</p>
					</label>
					<label class="radio inline">
						<input type="radio" value="Female" <?php if($gender_val[0]->value=='Female') { ?> checked="checked" <?php } ?> name="custom_field_2"/><p>Female</p>
					</label>					
						<?php /* <span class="pull-right">
							<select class="input-medium" name="field_access_2" id="field_access_2">
								<option selected="selected" value="0">Public</option>
								<option value="10">Site Members</option>
								<option value="20">Only Me</option>
							</select>
						</span>	*/?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="availability">Availability<span class="redfont"></span>:
					<?php $availability = $jbuser->isAvailability($this->userInfo->user_id); ?>
						<input class="radio inline" type="checkbox" <?php if($availability==true) { ?> checked="checked" <?php } ?> value="1" name="availability" style="width: 20px;"/>
				</label>
			</div>
		
		</div>
		<?php endif; ?>
	</fieldset>
	</div>
    <div class="tab-pane contact-details" id="2">
	<fieldset>
		<legend class="edit-profile-heading"><?php echo "Contact Details" ; ?></legend>
		<div class="edit-profile-body-section">
		<div class="control-group">
			<label class="control-label" for="address"><?php echo "Address" ; ?></label>
			<div class="controls">
				<textarea name="address" id="address" onblur='return check_val();' rows="5" class="input-xlarge"><?php echo $this->userInfo->address; ?></textarea>
			</div>
		</div>
		<div class="control-group location-field">
			<label class="control-label" for="level1"><?php echo JText::_('COM_JBLANCE_LOCATION'); ?></label>
			<?php 
			if($this->userInfo->id_location > 0){ ?>
				<div class="controls">
					<?php echo JblanceHelper::getLocationNames($this->userInfo->id_location); ?>
					<button type="button" class="" onclick="editLocation();"><img src="images/transparent_edit.png"><?php echo JText::_('COM_JBLANCE_EDIT'); ?></button>
				</div>
			<?php 	
			}
			?>
			<div class="controls controls-row choose-country" id="location_info">
				<?php 
				$attribs = array('class' => 'input-medium', 'data-level-id' => '1', 'onchange' => 'getLocation(this, \'project.getlocationajax\');');
				
				if($this->userInfo->id_location == 0){
					$attribs['class'] = 'input-medium';
					$attribs['style'] = 'display: inline-block;';
				}
				else {
					$attribs['style'] = 'display: none;';
				}
				echo $select->getSelectLocationCascade('location_level[]', '', 'COM_JBLANCE_PLEASE_SELECT', $attribs, 'level1');
				?>
				<input type="hidden" name="id_location" id="id_location" value="<?php echo $this->userInfo->id_location; ?>" />
				
				<div id="ajax-container" class="dis-inl-blk"></div>	


			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="postcode"><?php echo "Post Code" ; ?></label>
			<div class="controls">
				<input class="input-small" type="text" name="postcode" id="postcode" maxlength="10" onblur='return check_val();' value="<?php echo $this->userInfo->postcode; ?>" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="mobile"><?php echo "Mobile No" ; ?> :</label>
			<div class="controls">
				<input class="input-large" type="text" pattern="[0-9]{8,15}" name="mobile" id="mobile"  maxlength="15" onblur='return check_val();' value="<?php echo $this->userInfo->mobile; ?>" />
			</div>
		</div>
		<?php 

		$cval = $db->getQuery(true);
 		$cval->select($db->quoteName(array('value')));
 		$cval->from($db->quoteName('#__jblance_custom_field_value'));
 		$cval->where( ($db->quoteName('fieldid') . ' = '. $db->quote('8')).'AND'. ($db->quoteName('userid') . ' = '. $db->quote($this->userInfo->user_id)) )  ;

 		$db->setQuery($cval);
 		$res_city = $db->loadObjectList();

        ?>
		<div class="control-group location-field-city">
				<label for="custom_field_8" class="control-label">Home No<span class="redfont"></span>:</label>
				<div class="controls controls-row">
					<!-- <input type="text"  pattern="[0-9]{8,15}" title="" value="<?php echo $res_city[0]->value;?>" maxlength="15" id="custom_field_5" name="custom_field_8" class="hasTooltip " placeholder="" data-original-title="Home"> -->
<input class="input-large" type="text" pattern="[0-9]{8,15}" name="custom_field_8" id="custom_field_5"  maxlength="15" onblur='return check_val();' value="<?php echo $res_city[0]->value;?>" />
				</div>
		</div>

		</div>
	</fieldset>
	</div>
	<!-- Show the following profile fields only for JoomBri Profile -->
	<?php 
	$joombriProfile = false;
	$profileInteg = JblanceHelper::getProfile();
	$profileUrl = $profileInteg->getEditURL();
	if($profileInteg instanceof JoombriProfileJoombri){
		$joombriProfile = true;
	}
	
	if($joombriProfile){
		if(empty($this->fields)){
			echo '<p class="jbbox-warning">'.JText::_('COM_JBLANCE_NO_PROFILE_FIELD_ASSIGNED_FOR_USERGROUP').'</p>';
		}
		$fields = JblanceHelper::get('helper.fields');		// create an instance of the class fieldsHelper
		
		$parents = $children = array();
		$i=1;
		//isolate parent and childr
		foreach($this->fields as $ct){
			if($ct->parent == 0)
				$parents[] = $ct;
			else
				$children[] = $ct;
		}
			
		if(count($parents)){
			foreach($parents as $pt){ ?>
    <div class="tab-pane" id="3">
		<fieldset>
			<legend class="edit-profile-heading"><?php echo JText::_($pt->field_title); ?></legend>
			<div class="edit-profile-body-section">
			<?php
			foreach($children as $ct){
				if($ct->parent == $pt->id){ ?>
			<div class="control-group basic-info row-<?php echo $i;?>">
					<?php
					$labelsuffix = '';
					if($ct->field_type == 'Checkbox') $labelsuffix = '[]'; //added to validate checkbox
					?>
					<label class="control-label" for="custom_field_<?php echo $ct->id.$labelsuffix; ?>"><?php echo JText::_($ct->field_title); ?><span class="redfont"><?php echo ($ct->required)? '*' : ''; ?></span>:</label>
				<div class="controls controls-row">
					<?php $fields->getFieldHTML($ct, $user->id); ?>
				</div>
			</div>
			<?php $i++;
				}
			} ?>
			</div>
		</fieldset>
		</div>
				<?php
			}
		}

	}	//end of $joombriProfile 'if'
	else {
		echo JText::sprintf('COM_JBLANCE_CLICK_HERE_FOR_OTHER_PROFILE', $profileUrl).'<BR>';
	}

	//echo "<pre>"; print_r($this->row);
	?>
	
<script type="text/javascript">
<!--
jQuery(document).ready(function($){
	JoomBri.uploadCropPicture('user.uploadpicture', '<?php echo JblanceHelper::getLogoUrl($this->userInfo->user_id, ""); ?>', '<?php echo JblanceHelper::getLogoUrl($this->userInfo->user_id, "original"); ?>');
});
//-->
</script>
	<div class="tab-pane" id="4">
	<fieldset>
	<div class="row-fluid">
		<div class="edit-profile-heading">
			<h1>Profile Picture</h1>
		</div>
		<div class="edit-profile-body-section">
		<div class="span7">
			<div class="image-heading">
				Your Profile Image
			</div>
			<div class="cropit-image-view">
				<input type="file" name="profile_file" class="cropit-image-input" />
				<div class="cropit-image-preview-container">
					<div class="cropit-image-preview"></div>
				</div>
				
				<div class="slider-wrapper">
					<span class="icon icon-image font14"></span>
					<input type="range" class="cropit-image-zoom-input" min="0" max="1" step="0.01" disabled>
					<span class="icon icon-image font20"></span>
				</div> 
			</div>
		</div>
		<div class="span5" style="margin-top: 13%;">
			<div class="btns">
				<p>You can edit your Profile Images, if you want to change Profile image so please click on Upload Button and set New image</p>
				<div id="upload-message"></div>
				<button type="button" class="select-image-btn"><?php echo "Upload" ; ?></button>
				<!-- <button type="button" class="btn btn-danger remove-picture" data-user-id="<?php //echo $this->row->user_id; ?>" data-remove-task="user.removepicture"><?php //echo JText::_('COM_JBLANCE_REMOVE_PICTURE'); ?></button> -->
			</div>
			<div class="save-cancel-button">
				<button type="button" class="crop-save" style="/*display: none;*/" onclick="setTimeout(function(){ window.location=window.location.href; },5000);"><?php echo JText::_('COM_JBLANCE_SAVE'); ?></button>
				<input type="button" value="<?php echo JText::_('COM_JBLANCE_CANCEL'); ?>" onclick="javascript:history.back();" class="btn btn-primary" />
			</div>
			<!-- <div class="">
				<strong><?php //echo JText::_('COM_JBLANCE_THUMBNAIL'); ?>:</strong><br>
				<div class="current-profile-picture">
					<div class="cropit-image-preview" style="cursor: auto;"></div>
				</div>
			</div> -->
		</div>
		</div>
		</fieldset>
	</div>
	<div class="form-actions">
		<div class="save-cancel-button">
			<input type="submit" value="<?php echo JText::_('COM_JBLANCE_SAVE'); ?>" class="btn btn-primary" />
			<input type="button" value="<?php echo JText::_('COM_JBLANCE_CANCEL'); ?>" onclick="javascript:history.back();" class="btn btn-primary" />
		</div>
	</div>
	
	<input type="hidden" name="option" value="com_jblance">
	<input type="hidden" name="task" value="user.saveprofile">
	<input type="hidden" name="id" value="<?php echo $this->userInfo->id; ?>">
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $this->userInfo->user_id; ?>" />
	<input type="hidden" name="upload_type" id="upload_type" value="<?php echo $upload_type; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>	