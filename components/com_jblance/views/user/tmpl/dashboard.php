<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	21 March 2012
 * @file name	:	views/user/tmpl/dashboard.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	Displays the user Dashboard (jblance)
 */
defined('_JEXEC') or die('Restricted access');

JHtml::_('jquery.framework');

$doc = JFactory::getDocument();
$doc->addScript("components/com_jblance/js/utility.js");
$jbuser = JblanceHelper::get('helper.user');

$model = $this->getModel();
$user = JFactory::getUser();
$config = JblanceHelper::getConfig();
$showFeedsDashboard = $config->showFeedsDashboard;
$enableEscrowPayment = $config->enableEscrowPayment;
$enableWithdrawFund = $config->enableWithdrawFund;
$userInfo = $jbuser->getUserGroupInfo($user->id, null);

JText::script('COM_JBLANCE_CLOSE');

$link_edit_profile = JRoute::_('index.php?option=com_jblance&view=user&layout=editprofile');
$link_portfolio = JRoute::_('index.php?option=com_jblance&view=user&layout=editportfolio');
$link_messages = JRoute::_('index.php?option=com_jblance&view=message&layout=inbox');
$link_post_project = JRoute::_('index.php?option=com_jblance&view=project&layout=editproject');
$link_list_project = JRoute::_('index.php?option=com_jblance&view=project&layout=listproject');
$link_search_proj = JRoute::_('index.php?option=com_jblance&view=project&layout=searchproject');
$link_my_project = JRoute::_('index.php?option=com_jblance&view=project&layout=showmyproject');
$link_my_bid = JRoute::_('index.php?option=com_jblance&view=project&layout=showmybid');
$link_my_services = JRoute::_('index.php?option=com_jblance&view=service&layout=myservice');
$link_service_bght = JRoute::_('index.php?option=com_jblance&view=service&layout=servicebought');
$link_deposit = JRoute::_('index.php?option=com_jblance&view=membership&layout=depositfund');
$link_withdraw = JRoute::_('index.php?option=com_jblance&view=membership&layout=withdrawfund');
$link_escrow = JRoute::_('index.php?option=com_jblance&view=membership&layout=escrow');
$link_transaction = JRoute::_('index.php?option=com_jblance&view=membership&layout=transaction');
$link_managepay = JRoute::_('index.php?option=com_jblance&view=membership&layout=managepay');
$link_subscr_hist = JRoute::_('index.php?option=com_jblance&view=membership&layout=planhistory');
$link_buy_subscr = JRoute::_('index.php?option=com_jblance&view=membership&layout=planadd');
$my = JFactory::getUser();
$userid = JRequest::getInt('userid', $my->id, 'int');

JblanceHelper::setJoomBriToken();

if (!JBLANCE_FREE_MODE) {
    if (!$user->guest) {
        $planStatus = JblanceHelper::planStatus($user->id);

        if ($planStatus == '1') {
            ?>
            <div class="jbbox-warning">
                <?php echo JText::sprintf('COM_JBLANCE_USER_SUBSCRIPTION_EXPIRED', $link_buy_subscr); ?>
            </div>
            <style>
                .menu-panel .col-sm-3:first-child, .menu-panel .col-sm-3:nth-child(3), .menu-panel .col-sm-3:nth-child(5) {
                    display:none !important;
                }
                .header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201, .header-search .user-dropdown-menu ul.nav.menu li.item-227
                {
                    display: none !important;
                }
            </style>
        <?php } elseif ($planStatus == '2') {
            ?>
            <div class="jbbox-info">
                <?php echo JText::sprintf('COM_JBLANCE_USER_DONT_HAVE_ACTIVE_PLAN', $link_subscr_hist); ?>
            </div>
            <style>
                .menu-panel .col-sm-3:first-child, .menu-panel .col-sm-3:nth-child(3), .menu-panel .col-sm-3:nth-child(5) {
                    display:none !important;
                }
                .header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201,.header-search .user-dropdown-menu ul.nav.menu li.item-227
                {
                    display: none !important;
                }
            </style>
            <?php
        }
    }
}
?>
<div class="jbl_h3title Dashboard-heading"><?php echo "Dashboard"; ?></div>
<div class="dashoard-section">
    <div class="row">
        <div class="col-sm-12 menu-panel">
            <div class="col-sm-3">
                <a href="index.php?option=com_jblance&view=message&layout=inbox">
                    <div class="image">
                        <img src="images/mail.png">
                        <div class="message-notifications">
                            <?php $newMsg = JblanceHelper::countUnreadMsg(); ?>
                            <?php echo ($newMsg > 0) ? '<span class="label label-info">' . JText::sprintf('COM_JBLANCE_COUNT_NEW', $newMsg) . '</span>' : ''; ?>
                        </div>
                    </div>
                    <div class="heading">
                        <h1>Message</h1>
                    </div>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="index.php?option=com_jblance&view=user&layout=editprofile">
                    <div class="image">
                        <img src="images/profile.png">
                    </div>
                    <div class="heading">
                        <h1>Profile</h1>
                    </div>
                </a>
            </div>
            <?php if ($userInfo->allowAddPortfolio) : ?>
                <div class="col-sm-3">
                    <a href="index.php?option=com_jblance&view=user&layout=editportfolio">
                        <div class="image">
                            <img src="images/portfolio.png">
                        </div>
                        <div class="heading">
                            <h1>Portfolio</h1>
                        </div>
                    </a>
                </div>
            <?php endif; ?>
            <?php if ($userInfo->allowPostProjects) : ?>
                <div class="col-sm-3">
                    <a href="index.php?option=com_jblance&view=project&layout=showmyproject">
                        <div class="image">
                            <img src="images/jobs.png">
                        </div>
                        <div class="heading">
                            <h1>Jobs</h1>
                        </div>
                    </a>
                </div>
            <?php endif; ?>
            <div class="col-sm-3">
                <a href="index.php?option=com_jblance&view=membership&layout=planhistory">
                    <div class="image">
                        <img src="images/subscription.png">
                    </div>
                    <div class="heading">
                        <h1>Subscription</h1>
                    </div>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="find-tradesmen/user/favourite">
                    <div class="image">
                        <img src="images/favourite.png">
                    </div>
                    <div class="heading">
                        <h1>Favourite</h1>
                    </div>
                </a>
            </div>
            <?php /* 	$link_projects= JRoute::_('index.php?option=com_jblance&view=project&layout=searchproject&Itemid=195'); 
              ?>
              <div class="col-sm-3">
              <a href="<?php echo $link_projects; ?>">
              <div class="image">
              <img src="/tm/images/camera.png">
              </div>
              <div class="heading">
              <h1>Projects</h1>
              </div>
              </a>
              </div> */ ?>
            <?php /* $link_finance= JRoute::_('index.php?option=com_jblance&view=membership&layout=depositfund&Itemid=138'); 
              ?>
              <div class="col-sm-3">
              <a href="<?php echo $link_finance; ?>">
              <div class="image">
              <img src="/tm/images/video.png">
              </div>
              <div class="heading">
              <h1>Finance</h1>
              </div>
              </a>
              </div> */ ?>
        </div>	
    </div>
    <?php /* <div class="row">
      <div class="col-sm-12 menu-panel">
      <!-- <div class="col-sm-3">
      <a href="index.php?option=com_community&view=friends">
      <div class="image">
      <img src="/tm/images/connection.png">
      </div>
      <div class="heading">
      <h1>Connection</h1>
      </div>
      </a>
      </div> -->
      <?php if(!$userInfo->allowPostProjects) : ?>
      <div class="col-sm-3">
      <a href="index.php?option=com_jblance&view=user&layout=editportfolio">
      <div class="image">
      <img src="/tm/images/portfolio.png">
      </div>
      <div class="heading">
      <h1>Portfolio</h1>
      </div>
      </a>
      </div>
      <?php endif; ?>
      <?php if($userInfo->allowPostProjects) : ?>
      <div class="col-sm-3">
      <a href="index.php?option=com_jblance&view=project&layout=showmyproject">
      <div class="image">
      <img src="/tm/images/jobs.png">
      </div>
      <div class="heading">
      <h1>Jobs</h1>
      </div>
      </a>
      </div>
      <?php endif; ?>
      <div class="col-sm-3">
      <a href="index.php?option=com_jblance&view=membership&layout=planhistory">
      <div class="image">
      <img src="/tm/images/subscription.png">
      </div>
      <div class="heading">
      <h1>Subscription</h1>
      </div>
      </a>
      </div>
      </div>
      </div> */ ?>
</div>
