<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	29 November 2012
 * @file name	:	views/user/tmpl/viewportfolio.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	Lets user to view porfolio (jblance)
 */
 defined('_JEXEC') or die('Restricted access');
 
JHtml::_('jquery.framework');

 $row 	= $this->row;
 $model = $this->getModel();
 $user 	= JFactory::getUser();
 $uri 	= JFactory::getURI();
 
 $doc 	 = JFactory::getDocument();
 $doc->addScript("components/com_jblance/js/utility.js");
 JblanceHelper::setJoomBriToken();
 $config 		  = JblanceHelper::getConfig();
 $currencycode 	  = $config->currencyCode;
 $dformat 		  = $config->dateFormat;
 $enableReporting = $config->enableReporting;
 $guestReporting  = $config->enableGuestReporting;
 $enableAddThis   = $config->enableAddThis;
 $addThisPubid	  = $config->addThisPubid;
 $showUsername	  = $config->showUsername;
 $sealProjectBids = $config->sealProjectBids;
 
 $nameOrUsername = ($showUsername) ? 'username' : 'name';
 
 $projHelper 	= JblanceHelper::get('helper.project');		// create an instance of the class ProjectHelper
 $hasJBProfile  = JblanceHelper::hasJBProfile($user->id);
 $publisher 	= JFactory::getUser($row->publisher_userid);
 
 if($hasJBProfile){
 	$jbuser = JblanceHelper::get('helper.user');
 	$userGroup = $jbuser->getUserGroupInfo($user->id, null);
 }
 
 $isMine = ($row->publisher_userid == $user->id);
 $hasJBProfileForViewer  = JblanceHelper::hasJBProfile($user->id);
 $userInfo = $jbuser->getUserGroupInfo($userid, null);

 $link_edit_portfolio = JRoute::_('index.php?option=com_jblance&view=project&layout=editportfolio&id='.$row->id); 
 $link_pick_user	= JRoute::_( 'index.php?option=com_jblance&view=project&layout=pickuser&id='.$row->id);
 JText::script('COM_JBLANCE_CLOSE');
 
 $now 		 = JFactory::getDate();
 $expiredate = JFactory::getDate($row->start_date);
 $expiredate->modify("+$row->expires days");
 //$isExpired = ($now > $expiredate) ? true : false;
 $isExpired = ($row->status == 'COM_JBLANCE_EXPIRED') ? true : false;
?>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.1/jquery.js"></script>

 <script type="text/javascript">
			jQuery(function() {
                var LBC = new carouselLightbox({
                    "selector": ".main"
                });
			})
</script>

<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="userForm">
	<div class="col-sm-12 back">
		<!-- <a href="/index.php?option=com_jblance&view=user&layout=viewprofile&id=<?php echo $row->user_id; ?>&Itemid=191" class=""><img src="images/left_arrow.png"><span>Back</span></a> -->
		<a href="javascript:history.back();" class=""><img src="images/left_arrow.png"><span>Back</span></a>
	</div>
<div class="detail-portfolio">
	<div class="title-contact">
		<div class="jbl_h3title">
			<?php echo $row->title; ?>
		</div>
		<div class="contact">
			<?php if($isMine) : 
				if($row->status == 'COM_JBLANCE_OPEN' || $row->status == 'COM_JBLANCE_EXPIRED'){ ?>
			<div id="edit-project" class="page-action edit-button">
			    <a href="<?php echo $link_edit_portfolio; ?>"><i class="icon-edit"></i> <?php echo JText::_('COM_JBLANCE_EDIT_PROJECT'); ?></a>
			</div>
			<?php } ?>
			<!-- show Pick User if bids>0 and status=open -->
			<?php if(($row->status == 'COM_JBLANCE_OPEN' || $row->status == 'COM_JBLANCE_EXPIRED') && count($this->bids) > 0) :?>
				<div id="pick-user" class="page-action">
				    <a href="<?php echo $link_pick_user; ?>"><i class="icon-map-marker"></i> <?php echo JText::_('COM_JBLANCE_PICK_USER').' ('.count($this->bids).')'; ?></a>
				</div>
			<?php endif; ?>
		<?php else : ?>
			<?php if($enableReporting && ($user->id !=0 || $guestReporting )) : ?>
			<!-- <div id="report-this" class="page-action">
			    <a href="<?php //echo $link_report; ?>"><i class="icon-warning-sign"></i> <?php //echo JText::_('COM_JBLANCE_REPORT_PROJECT'); ?></a>
			</div> -->
			<div id="send-message" class="page-action contact-button">
				<?php $link_sendpm = JRoute::_('index.php?option=com_jblance&view=message&layout=compose&username='.$publisher->username.'&subject='.$row->project_title); ?>
			    <a href="<?php echo $link_sendpm; ?>"> <?php echo "Contact" ; ?></a>
			</div>
			<?php endif; ?>
		<?php endif; ?> <!-- end of ismine if -->
		</div>

                <?php $jbuser = JblanceHelper::get('helper.user');?>
                <?php $protfolio_user_info = $jbuser->getUser($row->user_id); ?>
                <?php if($protfolio_user_info->group=='Company'){ ?>
                <div class="ratings">
                    <div class="social_activity">
                        <span class="like l_<?php echo $row->id?>" <?php if($user->group=='Tradesmen'){ ?> onclick="company_like('<?php echo $row->id?>','Portfolio');" <?php } ?>>
                            <?php $like_status = '';
                            $like_status = $jbuser->isLikeByTradesmen($row->id,'Portfolio',$user->id); 
                            echo ($like_status == 1) ? 'Unlike' : 'Like'; ?>
                        </span>
                        (<span class="like_count l_<?php echo $row->id?>">
                            <?php $like_count = '';
                            $like_count = $jbuser->postLikeCount($row->id,'Portfolio');
                            echo ($like_count>0) ? $like_count : 0;?>
                        </span>)
                    </div>
                    
                </div>
                <?php } ?>
	</div>
	<div class="row-fluid">
		<?php 
		if(!empty($this->row->video_link)){
		$youtubeUrl =  JUri::getInstance($this->row->video_link);
		$videoId = $youtubeUrl->getVar('v'); ?>

		<div class="jb-aligncenter">
			<object width="640" height="390">
				<param name="movie" value="https://www.youtube.com/v/<?php echo $videoId; ?>?version=3"></param>
				<param name="allowScriptAccess" value="always"></param>
				<embed src="https://www.youtube.com/v/<?php echo $videoId; ?>?version=3" type="application/x-shockwave-flash" allowscriptaccess="always" width="640" height="390"></embed>
			</object>
		</div>
		<?php 
		}
		?>
		<div class="clearfix"></div>

		<!-- <h4><?php //echo ; ?>:</h4> -->
		<!-- <div class="location">
		<?php //echo "<pre>"; print_r($user->id); exit; ?>
		<p><?php //echo JblanceHelper::getLocationNames($row->id_location); ?></p>
		</div> -->
		<div class="skills">
		<h4><?php echo "Skills Used"; ?>:</h4>
		<p><?php echo JblanceHelper::getCategoryNames($row->id_category, 'tags-link', 'user'); ?></p>
		</div>
		<div class="span9">
		<div class="description">
		<h4><?php echo "Work Description" ; ?></h4>
		<p><?php echo nl2br($row->description); ?></p>
		</div>
		
		<!-- <h4><?php //echo JText::_('COM_JBLANCE_WEB_ADDRESS'); ?>:</h4> -->
		<!-- <p><?php //echo !empty($row->link) ? $row->link : '<span class="redfont">'.JText::_('COM_JBLANCE_NOT_MENTIONED').'</span>'; ?></p> -->
		
		<!-- <h4><?php //echo JText::_('COM_JBLANCE_DURATION'); ?>:</h4> -->
		<!-- <p> -->
			<?php
			//if( ($row->start_date != "0000-00-00 00:00:00" ) && ($row->finish_date!= "0000-00-00 00:00:00") ){
			?>
				<?php //echo JHtml::_('date', $this->row->start_date, $dformat).' &harr; '.JHtml::_('date', $this->row->finish_date, $dformat); ?>
			<?php 
			//}
			//else
				//echo '<span class="redfont">'.JText::_('COM_JBLANCE_NOT_MENTIONED').'</span>';
			?>
		<!-- </p> -->
		</div>
		<div class="span3">
			<!-- <h1>Posted By</h1> -->
			<?php
			if($this->row->picture){
				$attachment = explode(";", $this->row->picture);
				$showName = $attachment[0];
				$fileName = $attachment[1];
				$imgLoc = JBPORTFOLIO_URL.$fileName;
			?>
			<p class="jb-aligncenter"><img src='<?php echo $imgLoc; ?>' class="img-polaroid" style="max-width: 450px; width: 95%" /></p>
			<?php 
			} ?>
			<!-- <h2><?php //echo $user->name; ?></h2> -->
		</div>
		<div class="span12 display-none">
			<h4 class="work-portfolio"><?php echo "Work Portfolio" ; ?></h4>
			<?php 
			$count = 0;
			for($i=1; $i<=5; $i++){
				$attachmentColumnNum = 'attachment'.$i;
				if($row->$attachmentColumnNum){ 
					$count++;
				?>
			<p><i class="icon-download"></i> <?php echo LinkHelper::getPortfolioDownloadLink('portfolio', $this->row->id, 'user.download', $attachmentColumnNum); ?></p>
			<?php 
				}
			} 
			if($count == 0)
				echo '<p class="no-more-portfolio">' . "No more portfolio" . '</p>' ;
			?>
		</div>
		<div class="span12 additional-images">
			<h4 class="work-portfolio"><?php echo "Work Portfolio" ; ?></h4>

<div class="main" 
style="display: block;width: 1000px;height: 402px;margin: 0px;">			
				<?php

					if($this->row->attachment1){
						$attachment1 = explode(";", $this->row->attachment1);
						$fileName1 = $attachment1[1];
						$img1 = JBPORTFOLIO_URL.$fileName1;
				?>


					<div class="carousel-main">
						<img title="" src="<?php echo $img1; ?>" height="125px" width="200px" data-index="0">
					</div>
			

				<?php 
				} 
				?>
				<?php
					if($this->row->attachment2){
						$attachment2 = explode(";", $this->row->attachment2);
						$fileName2 = $attachment2[1];
						$img2 = JBPORTFOLIO_URL.$fileName2;
				?>
				
					
						<div class="carousel-main">
							<img title="" src="<?php echo $img2; ?>" height="125px" width="200px" data-index="0">
						</div>
					
			

				<?php 
				} 
				?>

				<?php
					if($this->row->attachment3){
						$attachment3 = explode(";", $this->row->attachment3);
						$fileName3 = $attachment3[1];
						$img3 = JBPORTFOLIO_URL.$fileName3;
				?>

				
					<div class="carousel-main">
						<img title="" src="<?php echo $img3; ?>" height="125px" width="200px" data-index="0">
					</div>
				

				<?php 
				}
				?>

				<?php
					if($this->row->attachment4){
						$attachment4 = explode(";", $this->row->attachment4);
						$fileName4 = $attachment4[1];
						$img4 = JBPORTFOLIO_URL.$fileName4;
				?>

				
					<div class="carousel-main">
						<img title="" src="<?php echo $img4; ?>" height="125px" width="200px" data-index="0">
					</div>
				

				<?php 
				} 
				?>

				<?php
					if($this->row->attachment5){
						$attachment5 = explode(";", $this->row->attachment5);
						$fileName5 = $attachment5[1];
						$img5 = JBPORTFOLIO_URL.$fileName5;
				?>
				
					<div class="carousel-main">
						<img title="" src="<?php echo $img5; ?>" height="125px" width="200px" data-index="0">
					</div>
				

				<?php 
				} 
				?>
				</div>	
		</div>
		
	</div>
</div>
</form>