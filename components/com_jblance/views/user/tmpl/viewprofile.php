<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	22 March 2012
 * @file name	:	views/user/tmpl/viewprofile.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	View user profile (jblance)
 */
defined('_JEXEC') or die('Restricted access');

JHtml::_('jquery.framework');

JHtml::_('behavior.modal', 'a.jb-modal');
JHtml::_('behavior.tabstate');

$db = JFactory::getDbo();
$doc = JFactory::getDocument();
$session = JFactory::getSession();
$doc->addScript("components/com_jblance/js/utility.js");
$doc->addScript("components/com_jblance/js/jquery.timeago.js");


$app = JFactory::getApplication();


$model = $this->getModel();
$user = JFactory::getUser();
$userid = $app->input->get('id', 0, 'int');
if (empty($userid)) {  // get the current userid if not passed
    $userid = $user->id;
}

$isMine = ($user->id == $userid);
$hasJBProfileForViewer = JblanceHelper::hasJBProfile($user->id); //check if the profile viewer has JB profile
$jbuser = JblanceHelper::get('helper.user');  // create an instance of the class UserHelper
$userInfo = $jbuser->getUserGroupInfo($userid, null);   // this holds the info of profile owner
if ($hasJBProfileForViewer)
    $viewerInfo = $jbuser->getUserGroupInfo($user->id, null);  // this holds the info of profile viewer

$config = JblanceHelper::getConfig();
$enableReporting = $config->enableReporting;
$enableAddThis = $config->enableAddThis;
$addThisPubid = $config->addThisPubid;
$showUsername = $config->showUsername;
$showBizName = $config->showBizName;

$nameOrUsername = ($showUsername) ? 'username' : 'name';

$uri = JFactory::getURI();
$uid = $this->userInfo->user_id;

$link_sendpm = JRoute::_('index.php?option=com_jblance&view=message&layout=compose&username=' . $this->userInfo->username);
$link_report = JRoute::_('index.php?option=com_jblance&view=message&layout=report&id=' . $userid . '&report=profile&link=' . base64_encode($uri)/* .'&tmpl=component' */);
$link_edit_profile = JRoute::_('index.php?option=com_jblance&view=user&layout=editprofile');
$link_add_manages = JRoute::_('index.php?option=com_jblance&view=user&layout=addmanager');
$link_edit_picture = JRoute::_('index.php?option=com_jblance&view=user&layout=editpicture');
$link_user_list = JRoute::_('index.php?option=com_jblance&view=user&layout=userlist');
$link_subscr_hist = JRoute::_('index.php?option=com_jblance&view=membership&layout=planhistory');
$link_buy_subscr = JRoute::_('index.php?option=com_jblance&view=membership&layout=planadd');


JblanceHelper::setJoomBriToken();
if (!JBLANCE_FREE_MODE) {
    if (!$user->guest) {
        $planStatus = JblanceHelper::planStatus($user->id);

        if ($planStatus == '1') {
            ?>
            <div class="jbbox-warning">
                <?php echo JText::sprintf('COM_JBLANCE_USER_SUBSCRIPTION_EXPIRED', $link_buy_subscr); ?>
            </div>
            <style>
                #jbMenu {
                    display:none !important;
                }
                .text-background, .contact-us {
                    display: none !important;
                }
                .header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201, .header-search .user-dropdown-menu ul.nav.menu li.item-227
                {
                    display: none !important;
                }
            </style>
        <?php } elseif ($planStatus == '2') {
            ?>
            <div class="jbbox-info">
                <?php echo JText::sprintf('COM_JBLANCE_USER_DONT_HAVE_ACTIVE_PLAN', $link_subscr_hist); ?>
            </div>
            <style>
                #jbMenu {
                    display:none !important;
                }
                .text-background, .contact-us {
                    display: none !important;
                }
                .header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201, .header-search .user-dropdown-menu ul.nav.menu li.item-227
                {
                    display: none !important;
                }
            </style>
            <?php
        }
    }
}
?>
<script type="text/javascript">

    jQuery(document).ready(function () {
        jQuery("time.timeago").timeago();
    });
</script>
<?php
$cval = $db->getQuery(true);
$cval->select($db->quoteName(array('value')));
$cval->from($db->quoteName('#__jblance_custom_field_value'));
$cval->where(($db->quoteName('fieldid') . ' = ' . $db->quote('8')) . 'AND' . ($db->quoteName('userid') . ' = ' . $db->quote($this->userInfo->user_id)));

$db->setQuery($cval);
$res_city = $db->loadObjectList();

$home_no = $res_city[0]->value;
?>
<?php
//echo "<pre>"; print_r($session);

$user_add = $db->getQuery(true);

$user_add->select($db->quoteName(array('address')));
$user_add->from($db->quoteName('#__jblance_user'));
$user_add->where($db->quoteName('user_id') . ' = ' . $db->quote($uid));

$db->setQuery($user_add);

$addr = $db->loadObjectList();

foreach ($addr as $key_add) {

    $u_add = $key_add->address;
    $user_address = urlencode($u_add);

    if ($user_address) {
        $user_address = urlencode('united kingdom');
    }
    $response = '';
    if (!empty($user_address) && !empty($key_add->address)) {
        $url = "http://maps.google.com/maps/api/geocode/json?address=" . (($user_address) ? $user_address : urlencode('United Kingdom')) . "&sensor=false&region=UK";
        $response = file_get_contents($url);
        $response = json_decode($response, true);
    }
//print_r($response);

    $lat = @$response['results'][0]['geometry']['location']['lat'];
    $long = @$response['results'][0]['geometry']['location']['lng'];

//echo "latitude: " . $lat . " longitude: " . $long;

    $u_addone = explode(",", $u_add);
    ?>

    <script src="http://maps.googleapis.com/maps/api/js"></script>
<?php if($lat!='' && $long!=''){ ?>
    <script>

    var myCenter = new google.maps.LatLng(<?php echo $lat; ?>,<?php echo $long; ?>);

    function initialize()
    {
        var mapProp = {
            center: myCenter,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);


        var marker = new google.maps.Marker({
            position: myCenter,
        });

        marker.setMap(map);

        var infowindow = new google.maps.InfoWindow({
            content: "<?php echo $u_add; ?>"
        });


        infowindow.open(map, marker);

    }

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<?php } ?>

    <?php
}

$qrating = $db->getQuery(true);

$qrating->select($db->quoteName(array('actor', 'comments', 'quality_clarity', 'communicate', 'expertise_payment', 'professional', 'hire_work_again')));
$qrating->from($db->quoteName('#__jblance_rating'));
$qrating->where($db->quoteName('actor') . ' = ' . $db->quote($this->userInfo->user_id));

$db->setQuery($qrating);
$rating = $db->loadObjectList();


?>

<style type="text/css">
    .page-actions-box .page-action{margin-bottom: 5px;display: table;width: 100%;}
    .page-actions-box .page-action a{width: 100%;display: block;text-align: center;}
</style>

<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="viewProfile" class="form-horizontal">

    <div class="Back-link">
        <div class="pull-left"><a onclick="javascript:history.back();" class=""><img src="images/left_arrow.png"><span><?php echo "Back"; ?></span></a></div>
    </div>
    <!-- <div class="jbl_h3title"><?php //echo JText::_('COM_JBLANCE_PROFILE').' - '.$this->userInfo->name;          ?></div> -->

    <!-- Do not show send message & edit link to the profile owner -->
    <div class="searchtradedmen-detail">
        <?php if ($this->userInfo->address != '' && $u_addone[1] != "") { ?>
            <div class="map" id="googleMap"></div>
        <?php } ?>
        <div class="page-actions page-actions-box">
            <?php if ($isMine) : ?>
                <div id="edit-profile" class="page-action">
                    <a href="<?php echo $link_edit_profile; ?>"><?php echo JText::_('COM_JBLANCE_EDIT_PROFILE'); ?></a>
                </div>
            <?php else : ?>
                <?php //if($enableReporting) : ?>
                <!-- <div id="report-this" class="page-action">
                    <a href="<?php //echo $link_report;          ?>"><i class="icon-warning-sign"></i> <?php //echo JText::_('COM_JBLANCE_REPORT_USER');          ?></a>
                </div> -->
                <?php //endif;  ?>
                <div id="send-message" class="page-action">
                    <a href="<?php echo $link_sendpm; ?>"><?php echo "Contact"; ?></a>

                </div>
            <?php endif; ?>
            <?php if ($userInfo->allowPostProjects && ($user->parent_company=='' || $user->parent_company==0)) : ?>
                <div id="managers" class="page-action">
                    <a href="<?php echo $link_add_manages; ?>"><?php echo JText::_('COM_JBLANCE_MANAGERS'); ?></a>
                </div>
            <?php endif; ?>
        </div>

        <fieldset>
                <!-- <legend><?php //echo JText::_('COM_JBLANCE_USER_INFORMATION');          ?></legend> -->
            <div class="row-fluid">

                <div class="span3">
                    <?php
                    $att = "class='thumbnail'";
                    $avatar = JblanceHelper::getLogo($userid, $att);
                    echo $avatar;
                    ?><br>
                    <?php if ($isMine) : ?>
                                                            <!-- <a href="<?php //echo $link_edit_picture;         ?>"><i class="icon-picture"></i> <?php //echo JText::_('COM_JBLANCE_EDIT_PICTURE');         ?></a> -->
                    <?php endif; ?>


                    <?php //if(($userInfo->id)=='2'){?>
                    <?php if (!$isMine && $userInfo->allowBidProjects && $hasJBProfileForViewer && $viewerInfo->allowPostProjects) : ?><!-- show invite to project to non-profile-owner, if profile owner can bid and profile viewer can post project -->
                        <div class="row-fluid display-none">
                            <div class="span6">
                                <?php $isFavourite = JblanceHelper::checkFavorite($userid, 'profile'); // check if profile owner is favoured by viewer  ?>
                                <span id="fav-msg-<?php echo $userid; ?>">
                                    <?php if ($isFavourite > 0) : ?>
                                        <a onclick="favourite('<?php echo $userid; ?>', -1, 'profile');" href="javascript:void(0);" class="btn btn-danger btn-block"><span class="icon-minus-sign icon-white"></span> <?php echo JText::_('COM_JBLANCE_REMOVE_FAVOURITE') ?></a>
                                    <?php else : ?>
                                        <a onclick="favourite('<?php echo $userid; ?>', 1, 'profile');" href="javascript:void(0);" class="btn btn-block"><span class="icon-plus-sign"></span> <?php echo JText::_('COM_JBLANCE_ADD_FAVOURITE') ?></a>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <div class="span6">
                                <a class="btn btn-block" href="<?php echo $link_sendpm; ?>"><i class="icon-comment"></i> <?php echo JText::_('COM_JBLANCE_SEND_MESSAGE'); ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php //} else {  ?>
                    <?php if (!$isMine) : ?>
                        <!-- && $userInfo->allowBidProjects && $hasJBProfileForViewer && $viewerInfo->allowPostProjects  -->

                        <!-- show invite to project to non-profile-owner, if profile owner can bid and profile viewer can post project -->
                        <div class="row-fluid invite-project-button display-none">
                            <div class="span12">
                                <?php $link_invite = JRoute::_('index.php?option=com_jblance&view=project&layout=invitetoproject&id=' . $userid . '&tmpl=component'); ?>
                                <a href="<?php echo $link_invite; ?>" class="jb-modal" rel="{handler: 'iframe', size: {x: 650, y: 400}}"><i class="icon-envelope icon-white"></i> <?php echo JText::_('COM_JBLANCE_INVITE_TO_PROJECT'); ?></a>
                            </div>
                        </div>
                        <div class="row-fluid add-favourite-button">
                            <div class="span12">
                                <?php $isFavourite = JblanceHelper::checkFavorite($userid, 'profile'); // check if profile owner is favoured by viewer ?>
                                <span class="view-profile" id="fav-msg-<?php echo $userid; ?>">
                                    <?php if ($isFavourite > 0) : ?>
                                        <a onclick="favourite('<?php echo $userid; ?>', -1, 'profile');" href="javascript:void(0);" class="remove-favourite"><span class="icon-minus-sign icon-white"></span> <?php echo JText::_('COM_JBLANCE_REMOVE_FAVOURITE') ?></a>
                                    <?php else : ?>
                                        <a onclick="favourite('<?php echo $userid; ?>', 1, 'profile');" href="javascript:void(0);" class=""><span class="glyphicon glyphicon-plus"></span> <?php echo JText::_('COM_JBLANCE_ADD_FAVOURITE') ?></a>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <div class="span6 display-none">
                                <a class="btn btn-block" href="<?php echo $link_sendpm; ?>"><i class="icon-comment"></i> <?php echo JText::_('COM_JBLANCE_SEND_MESSAGE'); ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php //} ?>

                </div>
                <div class="span9">
                    <h2><?php echo $this->userInfo->name; ?> <small><?php //echo  $this->userInfo->username;          ?></small></h2>

                    <!-- Company Name should be visible only to users who can post project -->
                    <?php // if ($userInfo->allowPostProjects && $showBizName) : ?>
                    <!--<div class="control-group">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_BUSINESS_NAME'); ?>: </label>
                        <div class="controls">
                    <?php // echo $this->userInfo->biz_name; ?>
                        </div>
                    </div>-->
                    <?php // endif; ?>
                    <?php //if($isMine) :  ?>
                    <div class="control-group">
                            <!-- <label class="control-label nopadding"><?php //echo JText::_('COM_JBLANCE_LOCATION');          ?>: </label> -->
                        <div class="controls">
                            <?php echo JblanceHelper::getLocationNames($this->userInfo->id_location); ?>
                        </div>
                    </div>
                    <?php //endif; ?>
                    <div class="custom-field-gender display-none">
                        <?php
                        $fields = JblanceHelper::get('helper.fields');  // create an instance of the class FieldsHelper
                        $parents = array();
                        $children = array();
//isolate parent and childr
                        foreach ($this->fields as $ct) {
                            if ($ct->parent == 0)
                                $parents[] = $ct;
                            else
                                $children[] = $ct;
                        }
                        $i = 0;

                        if (count($parents)) {
                            foreach ($parents as $pt) {
                                ?>
                                <fieldset>
                                        <!-- <legend><?php //echo JText::_($pt->field_title);         ?></legend> -->
                                    <?php
                                    foreach ($children as $ct) {
                                        if ($ct->parent == $pt->id) {
                                            ?>
                                            <?php
                                            $labelsuffix = '';
                                            if ($ct->field_type == 'Checkbox')
                                                $labelsuffix = '[]'; //added to validate checkbox
                                            ?>
                                            <div class="control-group row-<?php echo $i; ?>">
                                                <label class="control-label nopadding" for="custom_field_<?php echo $ct->id . $labelsuffix; ?>"><?php echo JText::_($ct->field_title); ?>:</label>
                                                <div class="controls">
                                                    <?php $fields->getFieldHTMLValues($ct, $userid, 'profile'); ?>
                                                </div>
                                            </div>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </fieldset>
                                <?php
                            }
                        }
                        ?>
                    </div>
                            <?php if($this->userInfo->group=='Company' && $user->group=='Tradesmen'){ ?>
                    <div class="control-group user-follow">
                            <div class="social_activity">
                                <span class="follow co_<?php echo $this->userInfo->user_id;?>" onclick="company_follow('<?php echo $this->userInfo->user_id?>');"><?php if($jbuser->isFollowCompanyByTradesmen($this->userInfo->user_id,$user->id)==1){ ?>UnFollow<?php }else{ ?>Follow<?php } ?></span>
                            </div>
                    </div>
                            <?php } ?>
                    <div class="control-group user-status">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_STATUS'); ?>: </label>
                        <div class="controls">
                            <?php
                            //get user online status
                            $status = $jbuser->isOnline($this->userInfo->user_id);
                            //print_r($this->userInfo);
                            ?>
                            <?php if ($status) : ?>
                                <span class="label label-success"><?php echo JText::_('COM_JBLANCE_ONLINE'); ?></span>
                            <?php else : ?>
                                <span class="label"><?php echo JText::_('COM_JBLANCE_OFFLINE'); ?></span>
                            <?php endif; ?>

                        </div>  
                            <br/>
                            <?php  //$u_group = $user->group; 
                            if($this->userInfo->group!='Company'){ ?>
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_AVAILABILITY'); ?>: </label>
                        <div class="controls">
                        <?php 
                            $availability = $jbuser->isAvailability($this->userInfo->user_id); 
                        ?>
                            <?php if (@$availability) : ?>
                                <span class="label label-success"><?php echo JText::_('COM_JBLANCE_ON'); ?></span>
                            <?php else : ?>
                                <span class="label"><?php echo JText::_('COM_JBLANCE_OFF'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div> 
                    <?php } ?>
                    <!-- Skills and hourly rate should be visible only to users who can work/bid -->
                    <?php if ($userInfo->allowBidProjects) : ?>
                        <?php if ($isMine) : ?>
                            <div class="control-group view-skills">
                                <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_SKILLS'); ?>: </label>
                                <div class="controls">
                                    <?php echo JblanceHelper::getCategoryNames($this->userInfo->id_category, 'tags-link'); ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="control-group view-skills">
                                <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_SKILLS'); ?>: </label>
                                <div class="controls">
                                    <?php echo JblanceHelper::getCategoryNames($this->userInfo->id_category, 'tags-link', 'user'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div class="control-group">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_AVERAGE_RATING'); ?>: </label>
                        <div class="controls">

                            <?php $rate = JblanceHelper::getAvarageRate($this->userInfo->user_id, true); ?>
                        </div>
                    </div> 

                    <?php if ($enableAddThis & !$isMine) : ?>
                        <div id="social-bookmark" class="page-action social-icons">
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_button_google_plusone" g:plusone:size="medium"></a> 
                                <a class="addthis_counter addthis_pill_style"></a>
                            </div>
                            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=<?php echo $addThisPubid; ?>"></script>
                            <!-- AddThis Button END -->
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </fieldset>
        <div class="search-tradesmen-listing-menu display-none">
            <ul class="nav nav-tabs" id="myTab">
                <li id="tab111" class="active"><a href="#1" data-toggle="tab"> About </a></li>
                <?php if ($userInfo->allowAddPortfolio) : ?>
                    <li id="tab222" class=""><a href="#2" data-toggle="tab"> Portfolio </a></li>
                <?php endif; 
                    if ($userInfo->allowPostProjects) : ?>
                    <li id="tab222" class=""><a href="#2" data-toggle="tab"> Projects </a></li>
                <?php endif; ?>
                <?php /* <li id="tab333" class=""><a href="#3" data-toggle="tab"> Projects History </a></li> */ ?>
                <li id="tab444" class=""><a href="#4" data-toggle="tab"> Rating </a></li>
            </ul>
        </div>
        <div class="row-fluid view-section">
            <div class="parts">
                <div class="parts-heading">
                    <h2>About</h2>
                </div>
                <div class="custom-field-info">
                    <?php
                    $fields = JblanceHelper::get('helper.fields');  // create an instance of the class FieldsHelper
                    $parents = array();
                    $children = array();
                    //isolate parent and childr
                    foreach ($this->fields as $ct) {
                        if ($ct->parent == 0)
                            $parents[] = $ct;
                        else
                            $children[] = $ct;
                    }

                    if (count($parents)) {
                        foreach ($parents as $pt) {
                            ?>
                            <fieldset>
                                    <!-- <legend><?php //echo JText::_($pt->field_title);         ?></legend> -->
                                <?php
                                foreach ($children as $ct) {
                                    if ($ct->parent == $pt->id) {
                                        ?>
                                        <?php
                                        $labelsuffix = '';
                                        if ($ct->field_type == 'Checkbox')
                                            $labelsuffix = '[]'; //added to validate checkbox
                                        ?>
                                        <?php if (!empty($fields->getFieldValue(3, $userid, 'profile'))) { ?>
                                            <div class="control-group">
                                                <label class="control-label nopadding" for="custom_field_<?php echo $ct->id . $labelsuffix; ?>"><?php echo "About" . " - " . $this->userInfo->name; ?></label>
                                                <div class="controls">
                                                    <?php $fields->getFieldHTMLValues($ct, $userid, 'profile'); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php
                                    }
                                }
                                ?>
                            </fieldset>
                            <?php
                        }
                    }
                    ?>
                </div>
                <?php //if($isMine) :  ?>
                <fieldset class="trademen-info">
                    <?php if (!empty($this->userInfo->address) || !empty($this->userInfo->postcode) || !empty($this->userInfo->mobile) || !empty($home_no)) { ?>
                        <?php if ($userInfo->allowBidProjects) : ?>
                            <legend><?php echo "Information about Tradesmen"; ?></legend>
                        <?php elseif ($userInfo->allowPostProjects) : ?>
                            <legend><?php echo "Information about Company"; ?></legend>
                        <?php endif; ?>
                    <?php } ?>
                    <?php 
                    if(@$user->parent_company!=''){
                    $query = "SELECT p.name FROM #__users p ".
                             "WHERE p.id=".$user->parent_company." AND p.block=0";
                    $db->setQuery($query);
                    $parent_company = $db->loadObject();
                    if(isset($parent_company->name) && !empty($parent_company->name) && $userInfo->allowPostProjects){ ?>
                        <div class="control-group">
                            <label class="control-label nopadding"><?php echo "Company"; ?>: </label>
                            <div class="controls">
                                <?php echo $parent_company->name; ?>
                            </div>
                        </div>
                    <?php } 
                    } ?>

                    <?php if (!empty($this->userInfo->address)) { ?>
                        <div class="control-group">
                            <?php if ($userInfo->allowBidProjects) : ?>
                                <label class="control-label nopadding"><?php echo "Contact Address"; ?>: </label>
                            <?php elseif ($userInfo->allowPostProjects) : ?>
                                <label class="control-label nopadding"><?php echo "Company Address"; ?>: </label>
                            <?php endif; ?>	
                            <div class="controls">
                                <?php echo nl2br($this->userInfo->address); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($userInfo->allowBidProjects) : ?>
                        <div class="control-group">
                            <label class="control-label nopadding"><?php echo "Rates"; ?>: </label>
                            <div class="controls">
                                <?php echo JblanceHelper::formatCurrency($this->userInfo->rate, true, true) . ' / ' . JText::_('COM_JBLANCE_HOUR'); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($this->userInfo->postcode)) { ?>
                        <div class="control-group">
                            <label class="control-label nopadding"><?php echo "Zip Code"; ?>: </label>
                            <div class="controls">
                                <?php echo $this->userInfo->postcode; ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if (!empty($this->userInfo->mobile)) { ?>
                        <div class="control-group">

                            <?php if ($userInfo->allowBidProjects) : ?>
                                <label class="control-label nopadding"><?php echo "Mobile Number"; ?>: </label>
                            <?php elseif ($userInfo->allowPostProjects) : ?>
                                <label class="control-label nopadding"><?php echo "Company Mobile"; ?>: </label>
                            <?php endif; ?>

                            <div class="controls">
                                <?php echo $this->userInfo->mobile; ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($home_no)) { ?>
                        <div class="control-group">
                            <div class="controls">
                                <label class="control-label nopadding">
                                    <?php echo "Home Number"; ?>: 
                                </label>

                                <?php echo $home_no; ?>
                            </div>
                        </div>
                    <?php } ?>
                </fieldset>
                <?php //endif;  ?>

            </div>
            <div class="parts">
                <div class="parts-heading">
                    <?php if ($userInfo->allowBidProjects) : ?>
                        <h2>Portfolio</h2>
                    <?php elseif ($userInfo->allowPostProjects) : ?>
                        <h2>Portfolio</h2>
                    <?php endif; ?>
                </div>
                <?php if ($userInfo->allowAddPortfolio) : ?>
                                                    <!-- <div class="jbl_h3title"><?php //echo JText::_('COM_JBLANCE_PORTFOLIO')          ?></div> -->
                    <?php
                    if (count($this->portfolios)) :
                        ?>
                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <?php
                                for ($i = 0, $n = count($this->portfolios); $i < $n; $i++) {
                                    $portfolio = $this->portfolios[$i];
                                    $link_view_portfolio = JRoute::_('index.php?option=com_jblance&view=user&layout=viewportfolio&id=' . $portfolio->id);

                                    //get the portfolio image info
                                    if ($portfolio->picture) {
                                        $attachment = explode(";", $portfolio->picture);
                                        $showName = $attachment[0];
                                        $fileName = $attachment[1];
                                        $imgLoc = JBPORTFOLIO_URL . $fileName;
                                    } else
                                        $imgLoc = 'components/com_jblance/images/no_portfolio.png';
                                    ?>
                                    <li class="span12">
                                        <a href="<?php echo $link_view_portfolio; ?>" class=""><?php //echo JText::_('COM_JBLANCE_PORTFOLIO_DETAILS');          ?>
                                            <div class="thumbnail">
                                                <img style="width: 93px; height: 80px;" src="<?php echo $imgLoc; ?>" alt="" title="">
                                            </div>
                                            <div class="caption">
                                                <h3><?php echo $portfolio->title; ?></h3>
                                                <p><?php echo $portfolio->description; ?></p>
                                            </div>  
                                        </a>
                                        <?php if($this->userInfo->group=='Company'){ ?>
                                        <div class="ratings">
                                            <?php $jbuser = JblanceHelper::get('helper.user');?>
                                            <div class="social_activity">
                                                <span class="like l_<?php echo $portfolio->id?>" <?php if($user->group=='Tradesmen'){ ?> onclick="company_like('<?php echo $portfolio->id?>','Portfolio');" <?php } ?>>
                                                    <?php $like_status = '';
                                                    $like_status = $jbuser->isLikeByTradesmen($portfolio->id,'Portfolio',$user->id); 
                                                    echo ($like_status == 1) ? 'Unlike' : 'Like'; ?>
                                                </span>
                                                (<span class="like_count l_<?php echo $portfolio->id?>">
                                                    <?php $like_count = '';
                                                    $like_count = $jbuser->postLikeCount($portfolio->id,'Portfolio');
                                                    echo ($like_count>0) ? $like_count : 0;?>
                                                </span>)
                                            </div>
                                            
                                        </div>
                                        <?php } ?>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>

                        <?php
                    else :
                        echo '<p class="no-items">' . JText::_('COM_JBLANCE_NO_PORTFOLIO_FOUND') . '</p>';
                    endif;
                    ?>
                    <!-- end of portfolio count -->
                    <!-- <div class="lineseparator"></div> -->
                <?php elseif ($userInfo->allowPostProjects) : ?>
                    <?php
                    $projlisting = $db->getQuery(true);

                    $projlisting->select(array('id', 'project_title', 'description'));
                    $projlisting->from($db->quoteName('#__jblance_project'));
                    $projlisting->where(($db->quoteName('publisher_userid') . ' = ' . $db->quote($this->userInfo->user_id)));

                    //$projlisting->select(array('a.*', 'b.comments','b.actor'));
                    // $projlisting->from($db->quoteName('#__jblance_project', 'a'));
                    // $projlisting->join('INNER', $db->quoteName('#__jblance_rating', 'b') . ' ON (' . $db->quoteName('a.publisher_userid') . ' = ' . $db->quoteName('b.actor') . ')');
                    //$projlisting->where(($db->quoteName('a.publisher_userid') . ' = '. $db->quote($this->userInfo->user_id)).'AND'.($db->quoteName('a.status') . ' = '. $db->quote('COM_JBLANCE_CLOSED')));

                    $db->setQuery($projlisting);
                    $projhist = $db->loadObjectList();
                    //echo "<pre>"; print_r($projhist); exit;

                    if (count($projhist)) :
                        ?>

                        <div class="row-fluid">
                            <ul class="thumbnails">

                                <?php foreach ($projhist as $projlist) { ?>
                                    <li class="span12">				
                                        <div class="thumbnail">
                                            <?php
                                            $att = "class=''";
                                            $avatar = JblanceHelper::getLogo($userid, $att);
                                            echo $avatar;
                                            ?>
                                        </div>
                                        <div class="caption">
                                            <h3><?php echo LinkHelper::getProjectLink($projlist->id, $projlist->project_title); ?></h3>
                                            <p><?php echo $projlist->description; ?></p>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php
                    else :
                        echo JText::_('COM_JBLANCE_NO_PROJECTS_FOUND');
                    endif;
                    ?>
                <?php endif; ?>
            </div>
            <?php /* <div class="tab-pane" id="3">
              <div class="jbl_h3title"><?php echo JText::_('COM_JBLANCE_PROJECTS_HISTORY'); ?></div>
              <?php

              $projlisting= $db->getQuery(true);

              $projlisting->select(array('a.*', 'b.comments','b.actor'));
              $projlisting->from($db->quoteName('#__jblance_project', 'a'));
              $projlisting->join('INNER', $db->quoteName('#__jblance_rating', 'b') . ' ON (' . $db->quoteName('a.publisher_userid') . ' = ' . $db->quoteName('b.actor') . ')');
              $projlisting->where(($db->quoteName('a.publisher_userid') . ' = '. $db->quote($this->userInfo->user_id)).'AND'.($db->quoteName('a.status') . ' = '. $db->quote('COM_JBLANCE_CLOSED')));

              $db->setQuery($projlisting);
              $projhist = $db->loadObjectList();
              //echo "<pre>"; print_r($projhist);

              if(count($projhist)) :
              ?>
              <div id="no-more-tables">
              <table class="table table-bordered table-hover table-striped">
              <thead>
              <tr>
              <th>#</th>
              <th><?php echo JText::_('COM_JBLANCE_PROJECT_NAME'); ?></th>
              <th><?php echo JText::_('COM_JBLANCE_RATED_BY'); ?></th>
              <th><?php echo JText::_('COM_JBLANCE_RATING_FROM_PUBLISHER'); ?></th>
              <th><?php echo JText::_('COM_JBLANCE_COMMENTS'); ?></th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($projhist as $projlist) {
              $k = 0;
              for($i=0, $n=count($projlist); $i < $n; $i++){
              $fproject = $this->fprojects[$i];
              $buyer 	  = JFactory::getUser($projlist->publisher_userid);
              ?>
              <tr>
              <td data-title="#">
              <?php echo $i+1;?>
              </td>
              <td data-title="<?php echo JText::_('COM_JBLANCE_PROJECT_NAME'); ?>">
              <?php echo LinkHelper::getProjectLink($projlist->id, $projlist->project_title); ?>
              </td>
              <td data-title="<?php echo JText::_('COM_JBLANCE_RATED_BY'); ?>">
              <?php echo LinkHelper::GetProfileLink($projlist->publisher_userid, $buyer->$nameOrUsername); ?>
              </td>
              <td data-title="<?php echo JText::_('COM_JBLANCE_RATING_FROM_PUBLISHER'); ?>">
              <?php
              $rate = JblanceHelper::getUserRateProject($projlist->assigned_userid, $projlist->id);
              JblanceHelper::getRatingHTML($rate);
              ?>
              </td>
              <td data-title="<?php echo JText::_('COM_JBLANCE_COMMENTS'); ?>">
              <?php
              if($projlist->comments!='')
              echo $projlist->comments;
              else
              echo '<i>'.JText::_('COM_JBLANCE_NOT_YET_RATED').'</i>';
              ?>
              </td>
              </tr>
              <?php
              $k = 1 - $k;
              } } ?>
              </tbody>
              </table>
              </div>


              <?php
              else :
              echo JText::_('COM_JBLANCE_NO_PROJECTS_FOUND');
              endif;
              ?>

              <div class="sp20">&nbsp;</div>
              </div> */ ?>
            <div class="parts rating-tab">
                <div class="parts-heading">
                    <h2>Rating</h2>
                </div>
                <div class="jbl_h3title"><?php echo JText::_('COM_JBLANCE_RATING'); ?></div>
                <?php //echo "<pre>"; print_r($user); exit; ?>
                <?php //if ($userInfo->allowBidProjects) : ?>
                <?php
                $u_group = $user->group;

                if ($u_group == 'Tradesmen') {
                    $logug_id = '1';
                } elseif ($u_group == 'Company') {
                    $logug_id = '2';
                }

                $act_group = $this->userInfo->ug_id;

                if ($act_group == '1') {
                    $viewug_id = $act_group;
                } elseif ($act_group == '2') {
                    $viewug_id = $act_group;
                }

                //if($logug_id==$viewug_id) { 
                ?>

                <?php //} else { ?>
                <div class="rating-button">
                    <?php
                    $q1 = $db->getQuery(true);

                    $q1->select($db->quoteName(array('logged_in_user', 'actor')));
                    $q1->from($db->quoteName('#__jblance_rating'));
                    $q1->where(($db->quoteName('actor') . ' = ' . $db->quote($this->userInfo->user_id)) . 'AND' . ($db->quoteName('logged_in_user') . ' = ' . $db->quote($user->id)));


                    $db->setQuery($q1);

                    $res = $db->loadObjectList();


                    $act_id = $this->userInfo->user_id;
                    $aid = $res[0]->actor;

                    $u_id = $user->id;
                    $luser = $res[0]->logged_in_user;


                    //echo $act_id.'='.$aid;
                    //echo $u_id.'='.$luser;
                    if ($logug_id == $viewug_id) {
                        
                    } else {

                        if ($act_id == $aid && $luser == $u_id) {
                            ?>

                            <?php
                        } else {
                            $link_rate = JRoute::_('index.php?option=com_jblance&view=project&layout=rateuser?userid=' . $act_id);
                            ?>
                            <a href="<?php echo $link_rate; ?>" class="">
                                <?php echo JText::_('COM_JBLANCE_RATE_COMPANY'); ?>
                            </a>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?php //endif ?>
                <?php
                $qclarity = "SELECT AVG(quality_clarity) AS QualityAverage FROM #__jblance_rating WHERE actor='$act_id'";

                $db->setQuery($qclarity);
                $quality_clarity = $db->loadResult();


                $comm = "SELECT AVG(communicate) AS CommAverage FROM #__jblance_rating WHERE actor='$act_id'";
                $db->setQuery($comm);
                $communicate = $db->loadResult();

                $expr = "SELECT AVG(expertise_payment) AS ExpAverage FROM #__jblance_rating WHERE actor='$act_id'";
                $db->setQuery($expr);
                $expertise_payment = $db->loadResult();

                $prof = "SELECT AVG(professional) AS ProfAverage FROM #__jblance_rating WHERE actor='$act_id'";
                $db->setQuery($prof);
                $professional = $db->loadResult();

                $work = "SELECT AVG(hire_work_again) AS WorkAverage FROM #__jblance_rating WHERE actor='$act_id'";
                $db->setQuery($work);
                $hire_work_again = $db->loadResult();



                if (!empty($rating)) {
                    ?>
                    <div class="control-group">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_QUALITY_OF_WORK'); ?>: </label>
                        <div class="controls">
                            <?php JblanceHelper::getRatingHTML($quality_clarity); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_COMMUNICATION'); ?>: </label>
                        <div class="controls">
                            <?php JblanceHelper::getRatingHTML($communicate); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_EXPERTISE'); ?>: </label>
                        <div class="controls">
                            <?php JblanceHelper::getRatingHTML($expertise_payment); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_PROFESSIONALISM'); ?>: </label>
                        <div class="controls">
                            <?php JblanceHelper::getRatingHTML($professional); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label nopadding"><?php echo JText::_('COM_JBLANCE_HIRE_AGAIN'); ?>: </label>
                        <div class="controls">
                            <?php JblanceHelper::getRatingHTML($hire_work_again); ?>
                        </div>
                    </div>
                    <?php
                    $q2 = $db->getQuery(true);

                    $q2->select($db->quoteName(array('logged_in_username', 'comments')));
                    $q2->from($db->quoteName('#__jblance_rating'));
                    $q2->where(($db->quoteName('actor') . ' = ' . $db->quote($this->userInfo->user_id)));

                    $db->setQuery($q2);

                    $results = $db->loadObjectList();
                    //echo"<pre>"; print_r($results); exit;
                    ?>
                    <div class="user-reviews">
                        <div class="jbl_h3title reviews">Reviews</div>
                        <?php for ($k = 0; $k < count($results); $k++) { ?>
                            <div class="single-review">
                                <label class="user-name">
                                    <?php echo $results[$k]->logged_in_username . ":"; ?>
                                </label>
                                <p class="user-comments">
                                    <?php echo $results[$k]->comments; ?>
                                </p>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                } else {
                    echo '<p class="no-rating">' . JText::_('COM_JBLANCE_RATING_NOT_FOUND') . '</p>';
                }
                echo JHtml::_('bootstrap.endTab');
                ?>

            </div>

            <?php //echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'freelancer'));  ?>

            <?php //echo JHtml::_('bootstrap.addTab', 'myTab', 'freelancer', JText::_('COM_JBLANCE_FREELANCER', true));   ?>
            <!-- project history -->
            <!-- <div class="jbl_h3title"><?php //echo JText::_('COM_JBLANCE_PROJECTS_HISTORY');         ?></div> -->


            <input type="hidden" name="option" value="com_jblance">
            <input type="hidden" name="task" value="">
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </div>
</form>	

