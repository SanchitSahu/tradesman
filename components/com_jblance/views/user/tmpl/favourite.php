<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	11 June 2012
 * @file name	:	views/user/tmpl/favourite.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	User list page (jblance)
 */
 defined('_JEXEC') or die('Restricted access');
 
 JHtml::_('jquery.framework');
 JHtml::_('formbehavior.chosen', '.advancedSelect');
 
 $doc = JFactory::getDocument();
 $db = JFactory::getDbo();
 $user = JFactory::getUser();
 //$currentdate =& JFactory::getDate();
 $currentdate = JFactory::getDate();

 	$usergroup = JblanceHelper::getUserroll($user->id);

	$getcountry = $db->getQuery(true);
	$getcountry->select($db->quoteName(array('id_location')));
	$getcountry->from($db->quoteName('#__jblance_user'));
	$getcountry->where($db->quoteName('user_id') . ' = '. $db->quote($user->id));
	$db->setQuery($getcountry);

	$usercountry = $db->loadObjectList();
	$country = $usercountry[0]->id_location;

	$checkparent = $db->getQuery(true);
	$checkparent->select($db->quoteName(array('parent_id')));
	$checkparent->from($db->quoteName('#__jblance_location'));
	$checkparent->where($db->quoteName('id') . ' = '. $db->quote($country));
	$db->setQuery($checkparent);

	$parentdata = $db->loadObjectList();
	$parent_id = $parentdata[0]->parent_id;

	if($parent_id>'1')
	{
		$set_country = $parent_id;
		$set_city = $country;

		$rootpath = JURI::base();

		$selecttop = $db->getQuery(true);
		$selecttop->select($db->quoteName(array('name','params','publish_down')));
		$selecttop->from($db->quoteName('#__banners'));
		$selecttop->where($db->quoteName('catid') . ' = '.$db->quote('9'),'AND');
		$selecttop->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selecttop->where($db->quoteName('city').'='.$db->quote($set_city),'AND');
		$selecttop->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selecttop->where($db->quoteName('state').'=1');


		$selecttop->order('RAND()');
		$selecttop->setLimit('1');

		$db->setQuery($selecttop);

		$topbanners = $db->loadObjectList();
		
		$topexp = $topbanners[0]->publish_down;
		$topbndata = json_decode($topbanners[0]->params, TRUE);

		if($currentdate<$topexp)
		{
			$topbanimg = $rootpath.$topbndata['imageurl'];
		}	

		$selectside = $db->getQuery(true);
		$selectside->select($db->quoteName(array('name','params','publish_down')));
		$selectside->from($db->quoteName('#__banners'));
		$selectside->where($db->quoteName('catid').'='.$db->quote('8'),'AND');
		$selectside->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selectside->where($db->quoteName('city').'='.$db->quote($set_city),'AND');
		$selectside->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selectside->where($db->quoteName('state').'=1');
		$selectside->order('RAND()');
		$selectside->setLimit('1');
		$db->setQuery($selectside);
	
		$sidebanners = $db->loadObjectList();
		$sideexp = $sidebanners[0]->publish_down;
		$sidebndata =	json_decode($sidebanners[0]->params, TRUE);

		if($currentdate<$sideexp)
		{
			$sidebanimg = $rootpath.$sidebndata['imageurl'];
		}
	}
	else
	{
		$set_country = $country;

		$rootpath = JURI::base();

		$selecttop = $db->getQuery(true);
		$selecttop->select($db->quoteName(array('name','params','publish_down')));
		$selecttop->from($db->quoteName('#__banners'));
		$selecttop->where($db->quoteName('catid') . ' = '.$db->quote('9'),'AND');
		$selecttop->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selecttop->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selecttop->where($db->quoteName('state').'=1');
		$selecttop->order('RAND()');
		$selecttop->setLimit('1');
		$db->setQuery($selecttop);

		$topbanners = $db->loadObjectList();
		$topexp = $topbanners[0]->publish_down;
		$topbndata = json_decode($topbanners[0]->params,TRUE);


		if($currentdate<$topexp)
		{
			$topbanimg = $rootpath.$topbndata['imageurl'];
		}

		
		$selectside = $db->getQuery(true);
		$selectside->select($db->quoteName(array('name','params','publish_down')));
		$selectside->from($db->quoteName('#__banners'));
		$selectside->where($db->quoteName('catid').'='.$db->quote('8'),'AND');
		$selectside->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selectside->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selectside->where($db->quoteName('state').'=1');
		$selectside->order('RAND()');
		$selectside->setLimit('1');
		$db->setQuery($selectside);
	
		$sidebanners = $db->loadObjectList();
		$sideexp = $sidebanners[0]->publish_down;
		$sidebndata =	json_decode($sidebanners[0]->params, TRUE);

		if($currentdate<$sideexp)
		{
			$sidebanimg = $rootpath.$sidebndata['imageurl'];
		}
	} 

 $doc->addScript("components/com_jblance/js/utility.js");
 $doc->addScript("components/com_jblance/js/barrating.js");
 $doc->addScript("components/com_jblance/js/bootstrap-slider.js");
 $doc->addStyleSheet("components/com_jblance/css/barrating.css");
 $doc->addStyleSheet("components/com_jblance/css/slider.css");
 
 $app  = JFactory::getApplication();
 $jbuser = JblanceHelper::get('helper.user');		// create an instance of the class UserHelper
 $select = JblanceHelper::get('helper.select');		// create an instance of the class SelectHelper
 $user = JFactory::getUser();
 $model = $this->getModel();
 
 $letter = $app->input->get('letter', '', 'string');
 $actionLetter = (!empty($letter)) ? '&letter='.$letter : '';
 
 $config 		  = JblanceHelper::getConfig();
 $showUsername 	  = $config->showUsername;
 $showBizName 	  = $config->showBizName;
 $nameOrUsername = ($showUsername) ? 'username' : 'name';
 
 $keyword	  = $app->input->get('keyword', '', 'string');
 $id_category = $app->input->get('id_category', array(), 'array');
 $id_location = $app->input->get('id_location', array(), 'array');
 $hourly_rate = $app->input->get('hourly_rate', '', 'string');
 $rating	  = $app->input->get('rating', 0, 'int');
 $ordering	  = $app->input->get('ordering', 'u.name asc', 'string');
 $city		= $app->input->get('city', '', 'string');

 JArrayHelper::toInteger($id_category);
 JArrayHelper::toInteger($id_location);
 
 // Load the parameters.
 $params = $app->getParams();
 $show_search = $params->get('show_search', false);
 
 $action	= JRoute::_('index.php?option=com_jblance&view=user&layout=favourite'.$actionLetter);
 $actionAll	= JRoute::_('index.php?option=com_jblance&view=user&layout=favourite');
 
 JblanceHelper::setJoomBriToken();
 ?>
<script type="text/javascript">
<!--
jQuery(document).ready(function($) {
    $("#rating").barrating("show", {
        showSelectedRating:false
    });

    $("#hourly_rate").sliderz({});
});
//-->
</script>


<form action="<?php echo $action; ?>" method="post" name="userFormJob" class="form-inline" enctype="multipart/form-data">
<!-- show search fields if enabled -->
<div class="search-job-section">
  	<div class="row-fluid search-trademen-heading">
		<h1>Favourite</h1>
	</div>

<div class="col-sm-3 serch-bar">
	<div class="row-fluid search-bar-heading">
		<h2>Advance Search</h2>
	</div>
	<?php if($show_search) : ?>
<div class="searchbar-body">
	<div class="row-fluid top10">
		<div class="span12">
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<input type="text" name="keyword" id="keyword" value="<?php echo $keyword; ?>" class="input-xlarge hasTooltip" placeholder="Keywords" />
				</div>
				<div class="btn-group pull-left display-none">
					<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('COM_JBLANCE_SEARCH'); ?>"><i class="icon-search"></i></button>
					<a href="<?php echo $action; ?>" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid top10 display-none">	
	<?php /* ?>	<div class="span12 country-select">
			<?php 
			$attribs = "class='input-xlarge advancedSelect' size='1'";
			echo $select->getSelectLocationTree('id_location[]', $id_location, '', 'Country', $attribs, ''); ?>
		</div> <?php */ ?>
	</div>
	<?php ?> <div class="control-group location-info search-location">
			<label class="control-label" for="level1"><?php echo JText::_('COM_JBLANCE_LOCATION'); ?> :</label>
			<?php 
			if($this->row->id_location > 0){ ?>
				<div class="controls loacation-field">
					<?php echo JblanceHelper::getLocationNames($this->row->id_location); ?>
					<button type="button" class="" onclick="editLocation();"><?php echo JText::_('COM_JBLANCE_EDIT'); ?></button>
				</div>
			<?php 	
			}
			?>
			<div class="controls controls-row" id="location_info">
				<?php 
				$attribs = array('class' => 'input-medium', 'data-level-id' => '1', 'onchange' => 'getLocation(this, \'project.getlocationajax\');');
				
				//if($this->row->id_location == 0){
					$attribs['class'] = 'input-medium required';
					$attribs['style'] = 'display: inline-block;';
				//}
				//else {
				//	$attribs['style'] = 'display: none;';
				//}
				echo $select->getSelectLocationCascade('location_level[]', '', 'Country', $attribs, 'level1');
				if ( isset($_SESSION['country_location']) ) {
			        $_SESSION['country_location'] = $userdata;
			     }
			     //unset($_SESSION['country_location']);
				?>
				<input type="hidden" name="id_location" id="id_location" value="<?php echo $this->row->id_location; ?>" />
				<div id="ajax-container" class="dis-inl-blk"></div>	
			</div>
	</div> <?php  ?>
	<div class="row-fluid top10 display-none">
		<div class="span12 city-select">
			<?php 

				$queryc = $db->getQuery(true);
				 
				$queryc->select($db->quoteName(array('value')));
				$queryc->from($db->quoteName('#__jblance_custom_field_value'));
				$queryc->where(($db->quoteName('fieldid') . ' = '. $db->quote('5')));
				$queryc->group($db->quoteName('value'));
				 
				$db->setQuery($queryc);
				 
				$resultsc = $db->loadObjectList();
				//print_r($resultsc);?>


				<select name="city" id="city">
					<option selected="selected" value=""><?php echo "City";?></option>
						<?php for($i=0;$i<count($resultsc);$i++) { ?>
								<option value="<?php echo $resultsc[$i]->value;?>">
									<?php echo $resultsc[$i]->value;?>
								</option>
						<?php } ?>
	            </select>
		</div>
	</div>
	<div class="row-fluid top10">
		<div class="span12">
      		<div class="control-group">
				<label class="control-label rating-label" for="rating"><?php echo JText::_('COM_JBLANCE_RATING'); ?></label>
				<div class="controls brating" title="<?php echo JText::_('COM_JBLANCE_RATING_ABOVE'); ?>">
					<?php echo $select->getSelectRating('rating', $rating); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid top10 display-none">
		<div class="hour-select">
		<div class="span12">
      		<div class="control-group">
				<label class="control-label" for="project_type"><?php echo JText::_('COM_JBLANCE_HOURLY_RATE'); ?></label>
				<div class="controls">
					<label class="radio">
						<?php $limit = $model->getMaxMinHourlyLimit(); 
						$sliderValue = (empty($hourly_rate)) ? $limit->minlimit.','.$limit->maxlimit : $hourly_rate;
						?>
						<b style="margin-right: 15px;"><?php echo JblanceHelper::formatCurrency($limit->minlimit, true, false, 0); ?></b>
						<input type="text" name="hourly_rate" id="hourly_rate" class="input-xlarge" value="<?php echo $hourly_rate; ?>" data-slider-min="<?php echo $limit->minlimit; ?>" data-slider-max="<?php echo $limit->maxlimit; ?>" data-slider-step="10" data-slider-value="[<?php echo $sliderValue; ?>]" style="display: none; margin-top: 20px;" />
		 				<b style="margin-left: 15px;"><?php echo JblanceHelper::formatCurrency($limit->maxlimit, true, false, 0); ?></b>
					</label>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="select-skills">
		<div class="span12">
			<?php 
			$attribs = 'class="input-xlarge advancedSelect" size="1"';
			$categtree = $select->getSelectCategoryTree('id_category[]', $id_category, 'COM_JBLANCE_ALL_SKILLS', $attribs, '', true);
			echo $categtree; ?>
		</div>
	</div>
	<div class="sort-by">
		<div class="ordering-select pull-right">
			<!-- <span class="boldfont"><?php //echo JText::_('COM_JBLANCE_SORT_BY'); ?>:</span> -->
			<?php 
			$attribs = "class='input-medium advancedSelect' size='1'";
			echo $model->getSelectUserlistOrdering('ordering', $ordering, $attribs);
			?>		
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<button type="submit" class="search-button" title=""><i class="icon-search"></i> <?php echo JHtml::tooltipText('COM_JBLANCE_SEARCH') . " " . "Now"; ?></button>
		</div>
	</div>
</div>
	<?php endif; ?>
	<?php if ($sidebanimg !='') { ?>
	<div class="search-bar-banner">
		<a href="#"><img src="<?php echo $sidebanimg; ?>"></a>
	</div>
	<?php } ?>
</div>

<div class="col-sm-9 project-body-section">
	<!-- hide alpha index if search form is enabled -->
	<?php if(!$show_search) : ?>
	<div class="btn-group display-none">
	<?php
	echo JHtml::_('link', $actionAll, '#', array('title'=>JText::_('COM_JBLANCE_ALL'), 'class'=>'btn btn-mini')); 
	foreach (range('A', 'Z') as $i) :
		$link_comp_index = JRoute::_('index.php?option=com_jblance&view=user&layout=favourite&letter='.strtolower($i), false);
		if(strcasecmp($letter, $i) == 0)
			echo JHtml::_('link', $link_comp_index, $i, array('title'=>$i, 'class'=>'btn btn-mini active'));
		else
			echo JHtml::_('link', $link_comp_index, $i, array('title'=>$i, 'class'=>'btn btn-mini'));
	endforeach; ?>	
    </div>
	<div class="sp10">&nbsp;</div>
	<?php endif; ?>

<div class="container-fluid">
	<?php if ($topbanimg !='') { ?>
		<div class="row-fluid search-body-banner">
			<a href="#"><img src="<?php echo $topbanimg;?>"></a>
		</div>
	<?php } ?>
	
	<div class="row-fluid search-body">
		<div class="span12">
			<?php
				for ($i=0, $x=count($this->rows); $i < $x; $i++){
				$row 		 = $this->rows[$i];
				$status 	 = $jbuser->isOnline($row->user_id);		//get user online status
				
				$availability= $jbuser->isAvailability($row->user_id);	//check user availability
				if($availability==false){
					continue;
				}
				$viewerInfo  = $jbuser->getUserGroupInfo($user->id, null); 	// this holds the info of profile viewer
				$isFavourite = JblanceHelper::checkFavorite($row->user_id, 'profile');
				$isMine		 = ($user->id == $row->user_id);
				$link_sendpm = JRoute::_('index.php?option=com_jblance&view=message&layout=compose&username='.$row->username);



				$query = $db->getQuery(true);
 
				$query->select($db->quoteName(array('value')));
				$query->from($db->quoteName('#__jblance_custom_field_value'));
				$query->where($db->quoteName('fieldid') . ' = '. $db->quote('3') . ' AND ' . $db->quoteName('userid') . ' = '. $row->user_id);
				 
				$db->setQuery($query);
				 
				$results = $db->loadObjectList();
				//echo "<pre>"; print_r($results);

			?>
	<?php if($isFavourite > 0) : ?>
	<div class="row-fluid project-single-section">
		<div class="span12">
			<div class="span2">
				<?php
				$attrib = 'width=90 height=90 class="img-polaroid"';
				$avatar = JblanceHelper::getLogo($row->user_id, $attrib);
				echo !empty($avatar) ? LinkHelper::GetProfileLink($row->user_id, $avatar, '', '', ' pull-left') : '&nbsp;' ?>
			<div class="username">
				<?php echo LinkHelper::GetProfileLink($row->user_id, $row->$nameOrUsername); ?> 
			</div>
        	</div>
            <div class="span7">
				<div class="media-body">
					<h3 class="media-heading">
						<?php $stats = ($status) ? 'online' : 'offline'; ?> 
						<span class="online-status <?php echo $stats; ?>" title="<?php echo JText::_('COM_JBLANCE_'.strtoupper($stats)); ?>"></span>
						<div class="name">
						<?php echo LinkHelper::GetProfileLink($row->user_id, $row->name); ?>
						</div>
						
						<!-- show Add to Favorite button to others and registered users and who can post project -->
						<?php //if(!$user->guest && !$isMine && $viewerInfo->allowPostProjects){ ?>
						<span  class="pull-right">
						<div class="hourly-rate">
							<?php if($row->rate > 0){ ?>
							<span class="font14" style="margin-left: 10px;"><?php echo JblanceHelper::formatCurrency($row->rate, true, true, 0).'/'.JText::_('COM_JBLANCE_HR'); ?></span>
							<?php } ?>
						</div>
						<span class="text-background" id="fav-msg-<?php echo $row->user_id; ?>">
							<?php if($isFavourite > 0) : ?>
							<a onclick="favourite('<?php echo $row->user_id; ?>', -1,'profile');" href="javascript:void(0);" class="remove-favourite"><!-- <span class="icon-minus-sign icon-white"></span> --> <?php echo JText::_('COM_JBLANCE_REMOVE_FAVOURITE')?></a>
							<?php else : ?>
							<a onclick="favourite('<?php echo $row->user_id; ?>', 1,'profile');" href="javascript:void(0);" class="add-to-favourite"><!-- <span class="icon-plus-sign"></span> --> <?php echo JText::_('COM_JBLANCE_ADD_FAVOURITE')?></a>
							<?php endif; ?>
						</span>				
						<a class="contact-us" href="<?php echo $link_sendpm; ?>"><!-- <i class="icon-comment"></i> --> <?php echo JText::_('COM_JBLANCE_SEND_MESSAGE'); ?></a>
						<?php //} ?>
						</span>
					</h3>

					<?php /*if(!empty($row->biz_name) && $showBizName) : ?>
					<span class="boldfont font12"><?php echo JText::_('COM_JBLANCE_BUSINESS_NAME'); ?>: </span><?php echo $row->biz_name; ?>
					<?php endif; */?>

					<?php /*<strong><?php echo JText::_('COM_JBLANCE_USERGROUP'); ?> : </strong><?php echo $row->grpname; ?> | 
					<?php if($status) : ?>
						<span class="label label-success"><?php echo JText::_('COM_JBLANCE_ONLINE'); ?></span>
					<?php else : ?>
						<span class="label"><?php echo JText::_('COM_JBLANCE_OFFLINE'); ?></span>
					<?php endif; */?> 

					</div>
			</div>
			<div class="span3 loacation">
				<?php echo JblanceHelper::getLocationNames($row->id_location); ?>
			</div>
			<div class="span10 pull-right rating-panel">
				<?php if(!empty($results[0]->value)){ ?>
				<div class="font14 description">
					<p><?php echo $results[0]->value;?></p>
				</div>
				<?php } ?>

				<?php if(!empty($row->id_category)){ ?>
						<div class="font14 skills">
							<strong><?php echo JText::_('COM_JBLANCE_SKILLS'); ?></strong>: <?php echo JblanceHelper::getCategoryNames($row->id_category, 'tags-link', 'favourite'); ?>
						</div>
				<?php } ?>

				<div class="ratings">
					<?php $rate = JblanceHelper::getAvarageRate($row->user_id, true); ?>
				</div>
			</div>

		</div>

		<style>
			.no-results {
				display: none;
			}
		</style>

	</div>
<?php endif; ?>
	<div class="lineseparator display-none"></div>
	<?php } ?>
	<p class="no-results">There are no any favourites added.</p>
</div>
</div>
</div>

	<?php if(!count($this->rows)){ ?>
	<div class="alert alert-info">
		<?php echo JText::_('COM_JBLANCE_NO_USER_OR_MATCHING_YOUR_QUERY'); ?>
	</div>
	<?php } ?>
	<div class="pagination pagination-centered clearfix display-none">
		<div class="display-limit pull-right">
			<?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>&#160;
			<?php //echo $this->pageNav->getLimitBox(); ?>
		</div>
		<?php //echo $this->pageNav->getPagesLinks(); ?>
	</div>
</div>
 </div>
</form> 