<script>
 jQuery(document).ready(function($) {
 	    $("#myTab li a").click(function(){
		    $('.row-2').remove();
		});
		$('.row-3').remove();

		$("#tab222 a").click(function(){
		    $('.form-actions').show();
		});
		$("#tab111 a").click(function(){
		    $('.form-actions').show();
		});
 });
</script>
<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	22 March 2012
 * @file name	:	views/user/tmpl/addmanager.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	Add Manager (jblance)
 */
 defined('_JEXEC') or die('Restricted access');
 
 JHtml::_('jquery.framework');
 JHtml::_('behavior.formvalidator');
 JHtml::_('bootstrap.tooltip');
 
 $db = JFactory::getDbo();
 $doc 	 = JFactory::getDocument();
 $doc->addScript("components/com_jblance/js/utility.js");
 $doc->addScript("components/com_jblance/js/cropit.js"); 

 $user= JFactory::getUser();
 $model = $this->getModel();
 $select = JblanceHelper::get('helper.select');		// create an instance of the class SelectHelper
 
 $jbuser = JblanceHelper::get('helper.user');		// create an instance of the class UserHelper
 $userInfo = $jbuser->getUserGroupInfo($user->id, null);

 $upload_type = (empty($this->userInfo->picture)) ? 'NO_UPLOAD_CROP' : 'CROP_ONLY';

 
 $config 	  = JblanceHelper::getConfig();
 $currencysym = $config->currencySymbol;	
 $currencycod = $config->currencyCode;	
 $maxSkills   = $config->maxSkills;	
 $link_subscr_hist	= JRoute::_('index.php?option=com_jblance&view=membership&layout=planhistory');
 $link_buy_subscr	= JRoute::_('index.php?option=com_jblance&view=membership&layout=planadd');
 
 JHtml::_('formbehavior.chosen', '#id_category', null, array('max_selected_options' => $maxSkills, 'placeholder_text_multiple' => JText::_('COM_JBLANCE_PLEASE_SELECT_SKILLS_FROM_THE_LIST')));
 
 JText::script('COM_JBLANCE_CLOSE');
 
 JblanceHelper::setJoomBriToken();
if(!JBLANCE_FREE_MODE){
	if(!$user->guest){
		$planStatus = JblanceHelper::planStatus($user->id);
		
		if($planStatus == '1'){ ?>
		<div class="jbbox-warning">
			<?php echo JText::sprintf('COM_JBLANCE_USER_SUBSCRIPTION_EXPIRED', $link_buy_subscr); ?>
		</div>
		<style>
		#jbMenu {
			display:none !important;
		}
		.header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201,.header-search .user-dropdown-menu ul.nav.menu li.item-227
		{
			display: none !important;
		}
		</style>
	<?php }
	elseif($planStatus == '2'){ ?>
	<div class="jbbox-info">
			<?php echo JText::sprintf('COM_JBLANCE_USER_DONT_HAVE_ACTIVE_PLAN', $link_subscr_hist); ?>
		</div>
		<style>
		#jbMenu {
			display:none !important;
		}
		.header-search .user-dropdown-menu ul.nav.menu li.item-200, .header-search .user-dropdown-menu ul.nav.menu li.item-201
		{
			display: none !important;
		}
		</style>
	<?php }
	}
} 
?>
<?php  
if($user->id){
		$query = "SELECT p.* FROM #__users p ".
				 "WHERE p.parent_company=".$user->id." AND p.block=0 ". 
				 "GROUP BY p.id";
 		$db->setQuery($query);
 		$managers = $db->loadObjectList();
 	}
//echo '<pre>';print_r($managers);die;
 		?>
<script type="text/javascript">
function check_val(){
   
    if(jQuery.trim(jQuery("#name").val())==''){ jQuery("#name").val(''); }
    if(jQuery.trim(jQuery("#email").val())==''){ jQuery("#email").val(''); }
    if(jQuery.trim(jQuery("#username").val())==''){ jQuery("#username").val(''); }
    //if(jQuery.trim(jQuery("#biz_name").val())==''){ jQuery("#biz_name").val(''); }
    return true;
}

function validateForm(f){


    if(jQuery.trim(jQuery("#name").val())==''){ jQuery("#name").val(''); return false;}
    if(jQuery.trim(jQuery("#email").val())==''){ jQuery("#email").val(''); }
    if(jQuery.trim(jQuery("#username").val())==''){ jQuery("#username").val(''); }
    //if(jQuery.trim(jQuery("#biz_name").val())==''){ jQuery("#biz_name").val(''); return false;}
	return true;
}
<!--
<?php if($maxSkills > 0){ ?>
jQuery(document).ready(function($){
	if($("#id_category").length){
		$("#id_category").change(updateSkillCount);
		updateSkillCount();
	}
});
<?php } ?>
function updateSkillCount(){
	sel = jQuery("#id_category option:selected").length;
	jQuery("#skill_left_span").html(sel);
}

function editLocation(){
	jQuery("#level1").css("display", "inline-block").addClass("required");
}
//-->
jQuery('#system-message-container #system-message div h4').hide();
</script>
<style>
.profile-table-box > table{margin-top: 10px }
.profile-table-box > table > thead{font-weight: bold;background-color: #ccc;border: 1px solid #ccc;}
.profile-table-box > table > tbody{padding: 10px;border: 1px solid #ccc}
.add-manager-form-box .form-control{height: 43px;width: 100%;border: 2px solid #ccc;padding: 5px 17px;}
.add-manager-form-box .control-label{padding-top: 12px}
.add-manager-form-box .form-group{margin: 0px; margin-bottom: 15px;}

@media only screen and (min-width: 320px) and (max-width: 766px){
.edit-profile-section .form-actions {position: initial !important;margin: 10px 0 0 0 !important;width: 100%;}
.edit-profile-section.row-fluid{height: auto !important;}
}
</style>
<?php //include_once(JPATH_COMPONENT.'/views/profilemenu.php'); ?>

	<div class="jbl_h3title profile-main-heading"><?php echo JText::_('COM_JBLANCE_PROFILE'); ?></div>
<div class="edit-profile-menu">
  <ul class="nav nav-tabs" id="myTab">
    <li id="tab111" class="col-xs-3 active"><a href="#1" data-toggle="tab"> Manager </a></li>
    <li id="tab222" class="col-xs-3"><a href="#2" data-toggle="tab"> Add Manager </a></li>
  </ul>
</div>
	<div class="tab-content row-fluid edit-profile-section">
    <div class="tab-pane active" id="1">
    <span class="msg_success hide">Manager deleted successfully</span>
	<fieldset>
		<legend class="edit-profile-heading"><?php echo "Manager" ; ?></legend>
		<div class="profile-table-box">
			<table class="table table-striped">
				<thead>

					<th>Name</th>
					<th>Email</th>
					<th>Action</th>
				</thead>
				<tbody>
				<?php foreach($managers as $manager) { ?>
					<tr class="manager_<?php echo $manager->id;?>">
						<td>
							<h4>
								<?php echo LinkHelper::GetProfileLink($manager->id, $manager->name); ?>
							</h4>
						</td>
						<td><h4><?php echo $manager->email?></h4></td>
						<td><a for="<?php echo $manager->id;?>" onclick="DeleteManager(this);">Delete</a></td>
					</tr>
				<?php } ?>
						
				</tbody>
			</table>
		</div>
	</fieldset>
	</div>

    <div class="tab-pane contact-details" id="2">
    <?php  if(count($managers) >= 4){ ?>
    	<h3>You have use max add limit of managers.</h3><br/><h4>If you want to add any other manager then you remove first any existing manager.</h4>
    <?php }else{ ?>
		<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="addmanager" id="frmAddManager" class="form-validate form-horizontal" onsubmit="/*jQuery('.save-cancel-button .submit').attr('disable','disable');*/ return validateForm(this);" enctype="multipart/form-data" novalidate>
			<fieldset>
				<legend class="edit-profile-heading"><?php echo "Add Manager" ; ?></legend>
				<div class="edit-profile-body-section add-manager-form-box">

					<div class="form-group">
						<label class="control-label">Company Name</label>
						<div class="controls">
							<input class="form-control" type="text" name="name" id="name" onblur='return check_val();' value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="name">Company Userame</label>
						<div class="controls">
							<input class="form-control input-large hasTooltip required validate-username" type="text" name="username" id="username" onchange="checkAvailable(this);" onblur='return check_val();' value="" />
							<span id="status_username"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="name">Email Address</label>
						<div class="controls">
							<input class="form-control input-large hasTooltip required validate-email" type="text" name="manager_email" id="manager_email" onchange="checkAvailable(this);" onblur='return check_val();' value="" />
							<span id="status_email"></span>
						</div>
					</div>
					<div class="form-group hide">
						<label class="control-label" for="name">Password</label>
						<div class="controls">
							<input class="form-control" type="password" name="password" id="password" onblur='return check_val();' value="" />
						</div>
					</div>
						<div class="form-actions">
							<div class="save-cancel-button">
								<input type="submit" value="<?php echo JText::_('COM_JBLANCE_SAVE'); ?>" class="btn btn-primary submit" />
								<input type="button" value="<?php echo JText::_('COM_JBLANCE_CANCEL'); ?>" onclick="javascript:history.back();" class="btn btn-primary" />
							</div>
						</div>
				</div>
			</fieldset>
			<input type="hidden" name="option" value="com_jblance">
			<input type="hidden" name="type" value="Company">
            <input type="hidden" name="task" value="user.saveManager">
			<input type="hidden" name="id" value="<?php echo $this->userInfo->id; ?>">
			<input type="hidden" name="user_id" id="user_id" value="<?php echo $this->userInfo->user_id; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</form>	
		<?php } ?>
	</div>
	
	</div>