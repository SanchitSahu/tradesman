<?php
/**
 * @company		:	BriTech Solutions
 * @created by	:	JoomBri Team
 * @contact		:	www.joombri.in, support@joombri.in
 * @created on	:	28 March 2012
 * @file name	:	views/project/tmpl/searchproject.php
 * @copyright   :	Copyright (C) 2012 - 2015 BriTech Solutions. All rights reserved.
 * @license     :	GNU General Public License version 2 or later
 * @author      :	Faisel
 * @description	: 	Search projects (jblance)
 */
 defined('_JEXEC') or die('Restricted access');
 
 JHtml::_('jquery.framework');
 JHtml::_('bootstrap.tooltip');
 JHtml::_('formbehavior.chosen', '#id_categ', null, array('placeholder_text_multiple'=>JText::_('COM_JBLANCE_FILTER_PROJECT_BY_SKILLS')));
 //JHtml::_('formbehavior.chosen', '#id_location');
 
 $doc 	 = JFactory::getDocument();
 $db = JFactory::getDbo();
 $user = JFactory::getUser();
 //$currentdate =& JFactory::getDate();
 $currentdate = JFactory::getDate();

 	$usergroup = JblanceHelper::getUserroll($user->id);

	$getcountry = $db->getQuery(true);
	$getcountry->select($db->quoteName(array('id_location')));
	$getcountry->from($db->quoteName('#__jblance_user'));
	$getcountry->where($db->quoteName('user_id') . ' = '. $db->quote($user->id));
	$db->setQuery($getcountry);
	
	$usercountry = $db->loadObjectList();
	$country = $usercountry[0]->id_location;

	$checkparent = $db->getQuery(true);
	$checkparent->select($db->quoteName(array('parent_id')));
	$checkparent->from($db->quoteName('#__jblance_location'));
	$checkparent->where($db->quoteName('id') . ' = '. $db->quote($country));
	$db->setQuery($checkparent);

	$parentdata = $db->loadObjectList();
	$parent_id = $parentdata[0]->parent_id;

	if($parent_id>'1')
	{
		$set_country = $parent_id;
		$set_city = $country;

		$rootpath = JURI::base();

		$selecttop = $db->getQuery(true);
		$selecttop->select($db->quoteName(array('name','params','publish_down')));
		$selecttop->from($db->quoteName('#__banners'));
		$selecttop->where($db->quoteName('catid') . ' = '.$db->quote('9'),'AND');
		$selecttop->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selecttop->where($db->quoteName('city').'='.$db->quote($set_city),'AND');
		$selecttop->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selecttop->where($db->quoteName('state').'=1');


		$selecttop->order('RAND()');
		$selecttop->setLimit('1');

		$db->setQuery($selecttop);

		$topbanners = $db->loadObjectList();
		
		$topexp = $topbanners[0]->publish_down;
		$topbndata = json_decode($topbanners[0]->params, TRUE);

		if($currentdate<$topexp)
		{
			$topbanimg = $rootpath.$topbndata['imageurl'];
		}	

		$selectside = $db->getQuery(true);
		$selectside->select($db->quoteName(array('name','params','publish_down')));
		$selectside->from($db->quoteName('#__banners'));
		$selectside->where($db->quoteName('catid').'='.$db->quote('8'),'AND');
		$selectside->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selectside->where($db->quoteName('city').'='.$db->quote($set_city),'AND');
		$selectside->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selectside->where($db->quoteName('state').'=1');
		$selectside->order('RAND()');
		$selectside->setLimit('1');
		$db->setQuery($selectside);
	
		$sidebanners = $db->loadObjectList();
		$sideexp = $sidebanners[0]->publish_down;
		$sidebndata =	json_decode($sidebanners[0]->params, TRUE);

		if($currentdate<$sideexp)
		{
			$sidebanimg = $rootpath.$sidebndata['imageurl'];
		}
	}
	else
	{
		$set_country = $country;

		$rootpath = JURI::base();

		$selecttop = $db->getQuery(true);
		$selecttop->select($db->quoteName(array('name','params','publish_down')));
		$selecttop->from($db->quoteName('#__banners'));
		$selecttop->where($db->quoteName('catid') . ' = '.$db->quote('9'),'AND');
		$selecttop->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selecttop->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selecttop->where($db->quoteName('state').'=1');
		$selecttop->order('RAND()');
		$selecttop->setLimit('1');
		$db->setQuery($selecttop);

		$topbanners = $db->loadObjectList();
		$topexp = $topbanners[0]->publish_down;
		$topbndata = json_decode($topbanners[0]->params,TRUE);


		if($currentdate<$topexp)
		{
			$topbanimg = $rootpath.$topbndata['imageurl'];
		}

		
		$selectside = $db->getQuery(true);
		$selectside->select($db->quoteName(array('name','params','publish_down')));
		$selectside->from($db->quoteName('#__banners'));
		$selectside->where($db->quoteName('catid').'='.$db->quote('8'),'AND');
		$selectside->where($db->quoteName('country').'='.$db->quote($set_country),'AND');
		$selectside->where($db->quoteName('roll').'='.$db->quote($usergroup),'AND');
		$selectside->where($db->quoteName('state').'=1');
		$selectside->order('RAND()');
		$selectside->setLimit('1');
		$db->setQuery($selectside);

		$sidebanners = $db->loadObjectList();
		$sideexp = $sidebanners[0]->publish_down;
		$sidebndata =	json_decode($sidebanners[0]->params, TRUE);

		if($currentdate<$sideexp)
		{
			$sidebanimg = $rootpath.$sidebndata['imageurl'];
		}
	} 

 $doc->addScript("components/com_jblance/js/utility.js");
 $doc->addScript("components/com_jblance/js/btngroup.js");
 $doc->addScript("components/com_jblance/js/bootstrap-slider.js");
 $doc->addStyleSheet("components/com_jblance/css/slider.css");
 
 $app  		 = JFactory::getApplication();

 $model 	 = $this->getModel();

 $now 		 = JFactory::getDate();
 $projHelper = JblanceHelper::get('helper.project');		// create an instance of the class ProjectHelper
 $select 	 = JblanceHelper::get('helper.select');		// create an instance of the class SelectHelper
 $userHelper = JblanceHelper::get('helper.user');		// create an instance of the class UserHelper
 
 $keyword	  = $app->input->get('keyword', '', 'string');
 $phrase	  = $app->input->get('phrase', 'any', 'string');
 $id_categ	  = $app->input->get('id_categ', array(), 'array');
 $id_location = $app->input->get('id_location', array(), 'array');
 $proj_type	  = $app->input->get('project_type', array('fixed' => 'COM_JBLANCE_FIXED', 'hourly' => 'COM_JBLANCE_HOURLY'), 'array');
 $budget 	  = $app->input->get('budget', '', 'string');
 $status	  = $app->input->get('status', 'COM_JBLANCE_OPEN', 'string');
 $city		= $app->input->get('city', '', 'string');

 
 JArrayHelper::toInteger($id_categ);
 JArrayHelper::toInteger($id_location);
 
 $config 		  = JblanceHelper::getConfig();
 $currencysym 	  = $config->currencySymbol;
 $currencycode 	  = $config->currencyCode;
 $dformat 		  = $config->dateFormat;
 $sealProjectBids = $config->sealProjectBids;
 $showUsername 	  = $config->showUsername;
 
 $nameOrUsername = ($showUsername) ? 'username' : 'name';
 
 $action = JRoute::_('index.php?option=com_jblance&view=project&layout=searchproject');

  JblanceHelper::setJoomBriToken();
?>
<script type="text/javascript">
<!--
jQuery(document).ready(function($){
	$("#budget").sliderz({});
	
});
//-->
</script>


<form action="<?php echo $action; ?>" method="get" name="userFormJob" enctype="multipart/form-data">
<div class="search-job-section">
	<div class="row-fluid search-trademen-heading">
		<h1>Search Job</h1>
	</div>
	<div class="col-sm-3 serch-bar">
	<div class="row-fluid search-bar-heading">
		<h2>Advance Search</h2>
	</div>
	<div class="searchbar-body">
	<div class="row-fluid top10">
		<div class="span12">
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<input type="text" name="keyword" id="keyword" value="<?php echo $keyword; ?>" class="input-xlarge hasTooltip" placeholder="Keywords" />
				</div>
				<div class="btn-group pull-left display-none">
					<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('COM_JBLANCE_SEARCH'); ?>"><i class="icon-search"></i></button>
					<a href="<?php echo $action; ?>" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></a>
				</div>
			</div>
		</div>
		<div class="span5 display-none">
			<?php $list_phrase = $select->getRadioSearchPhrase('phrase', $phrase);	   					   		
			echo $list_phrase; ?>
		</div>
	</div>
	<div class="row-fluid top10 select-skills jobs">
		<div class="span12">
			<?php 
			$attribs = "class='input-block-level required' size='5' MULTIPLE";
			echo $select->getSelectCategoryTree('id_categ[]', $id_categ, '', $attribs, '', true); ?>
		</div>
	</div>
	<div class="row-fluid top10 display-none">
	<?php /* ?>	<div class="span12 country-select">
			<?php 
			$attribs = "class='input-large' size='1'";
			echo $select->getSelectLocationTree('id_location', $id_location, '', 'Country', $attribs, ''); ?>
		</div> <?php */ ?>
	</div>
	<?php ?> <div class="control-group location-info search-location">
			<label class="control-label" for="level1"><?php echo JText::_('COM_JBLANCE_LOCATION'); ?> :</label>
			<?php 
			if($this->row->id_location > 0){ ?>
				<div class="controls loacation-field">
					<?php echo JblanceHelper::getLocationNames($this->row->id_location); ?>
					<button type="button" class="" onclick="editLocation();"><?php echo JText::_('COM_JBLANCE_EDIT'); ?></button>
				</div>
			<?php 	
			}
			?>
			<div class="controls controls-row" id="location_info">
				<?php 
				$attribs = array('class' => 'input-medium', 'data-level-id' => '1', 'onchange' => 'getLocation(this, \'project.getlocationajax\');');
				
				//if($this->row->id_location == 0){
					$attribs['class'] = 'input-medium required';
					$attribs['style'] = 'display: inline-block;';
				//}
				//else {
				//	$attribs['style'] = 'display: none;';
				//}
				echo $select->getSelectLocationCascade('location_level[]', '', 'Country', $attribs, 'level1');
				if ( isset($_SESSION['country_location']) ) {
			        $_SESSION['country_location'] = $search_location_id;
			     }
			     //unset($_SESSION['country_location']);
				?>
				<input type="hidden" name="id_location" id="id_location" value="<?php echo $this->row->id_location; ?>" />
				<div id="ajax-container" class="dis-inl-blk"></div>	
			</div>
	</div> <?php  ?>
	<div class="row-fluid top10 display-none">
		<div class="span12 city-select">
			<?php 
						$cities = $db->getQuery(true);
				 
						$cities->select($db->quoteName(array('value')));
						$cities->from($db->quoteName('#__jblance_custom_field_value'));
						$cities->where(($db->quoteName('fieldid') . ' = '. $db->quote('7')));
						$cities->group($db->quoteName('value'));
				 
						$db->setQuery($cities);
				 
						$clists = $db->loadObjectList();

					    ?>
						<select name="city" id="city">
								<option selected="selected" value=""><?php echo "Please select";?></option>
							<?php for($i=0;$i<count($clists);$i++) { ?>
								<option value="<?php echo $clists[$i]->value;?>">
									<?php echo $clists[$i]->value;?>
								</option>
							<?php } ?>
	                	</select>
		</div>
	</div>
	<div class="row-fluid top10 form-horizontal">
		<div class="span6 display-none">
      		<div class="control-group">
				<label class="control-label" for="status"><?php echo JText::_('COM_JBLANCE_PROJECT_STATUS'); ?></label>
				<div class="controls">
					<?php 
					$attribs = "class='input-small' size='1'";
					$list_status = $select->getSelectProjectStatus('status', $status, 'COM_JBLANCE_ANY', $attribs, '');	   					   		
					echo $list_status; ?>
				</div>
			</div>			
		</div>
		<div class="span12 project-type-search">
      		<div class="control-group">
				<label class="control-label title" for="project_type"><?php echo JText::_('COM_JBLANCE_PROJECT_TYPE'); ?></label>
				<div class="">
					<label class="checkbox inline" for="">
						<input type="checkbox" id="fixed" name="project_type[fixed]" value="COM_JBLANCE_FIXED" <?php if(isset($proj_type['fixed'])) {echo 'checked'; } ?> ><?php echo JText::_('COM_JBLANCE_FIXED'); ?>
					</label> 
					<label class="checkbox inline">
						<!-- <input type="hidden" name="project_type['hourly']" value="0" /> -->
						<input type="checkbox" id="hourly" name="project_type[hourly]" value="COM_JBLANCE_HOURLY" <?php if(isset($proj_type['hourly'])) {echo 'checked';} ?> ><?php echo JText::_('COM_JBLANCE_HOURLY'); ?>
					</label> 
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid top10 form-horizontal display-none">
		<div class="span12">
      		<div class="control-group">
				<label class="control-label" for="project_type"><?php echo JText::_('COM_JBLANCE_BUDGET'); ?></label>
				<div class="controls">
					<label class="radio">
						<?php $limit = $model->getMaxMinBudgetLimit('COM_JBLANCE_FIXED'); 
						$sliderValue = (empty($budget)) ? $limit->minlimit.','.$limit->maxlimit : $budget;
						?>
						<b style="margin-right: 15px;"><?php echo JblanceHelper::formatCurrency($limit->minlimit, true, false, 0); ?></b>
						<input type="text" name="budget" id="budget" class="input-xlarge" value="<?php echo $budget; ?>" data-slider-min="<?php echo $limit->minlimit; ?>" data-slider-max="<?php echo $limit->maxlimit; ?>" data-slider-step="50" data-slider-value="[<?php echo $sliderValue; ?>]" style="display: none; margin-top: 20px;" />
		 				<b style="margin-left: 15px;"><?php echo JblanceHelper::formatCurrency($limit->maxlimit, true, false, 0); ?></b>		
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<button type="submit" class="search-button" title=""><i class="icon-search"></i> <?php echo JHtml::tooltipText('COM_JBLANCE_SEARCH') . " " . "Now"; ?></button>
		</div>
	</div>
	</div>
	<?php if ($sidebanimg !='') { ?>
	<div class="search-bar-banner">
		<a href="#"><img src="<?php echo $sidebanimg; ?>"></a>
	</div>
	<?php } ?>
	</div>
	
	<!-- <div class="lineseparator"></div> -->
	
	<div class="col-sm-9 project-body-section">
	<div class="container-fluid">
	<?php if ($topbanimg !='') { ?>
		<div class="row-fluid search-body-banner">
			<a href="#"><img src="<?php echo $topbanimg;?>"></a>
		</div>
	<?php } ?>
		<div class="row-fluid search-body">
			<div class="span12">
      		<!--Body content-->
      			<div class="jbl_h3title display-none"><?php echo JText::_('COM_JBLANCE_SEARCH_RESULTS'); ?></div>
      			<?php
				for ($i=0, $x=count($this->rows); $i < $x; $i++){
					$row = $this->rows[$i];
					$buyer = $userHelper->getUser($row->publisher_userid);
					$daydiff = $row->daydiff;
					
					if($daydiff == -1){
						$startdate = JText::_('COM_JBLANCE_YESTERDAY');
					}
					elseif($daydiff == 0){
						$startdate = JText::_('COM_JBLANCE_TODAY');
					}
					else {
						$startdate =  JHtml::_('date', $row->start_date, $dformat, true);
					}
					
					// calculate expire date and check if expired
					$expiredate = JFactory::getDate($row->start_date);
					$expiredate->modify("+$row->expires days");
					$isExpired = ($now > $expiredate) ? true : false;
					
					/* if($isExpired)
						$statusLabel = 'label';
					else */if($row->status == 'COM_JBLANCE_OPEN')
						$statusLabel = 'label label-success';
					elseif($row->status == 'COM_JBLANCE_FROZEN')
						$statusLabel = 'label label-warning';
					elseif($row->status == 'COM_JBLANCE_CLOSED')
						$statusLabel = 'label label-important';
					elseif($row->status == 'COM_JBLANCE_EXPIRED')
						$statusLabel = 'label';
					
					$bidsCount = $model->countBids($row->id);
					
					//calculate average bid
					$avg = $projHelper->averageBidAmt($row->id);
					$avg = round($avg, 0);
					
					// 'private invite' project shall be visible only to invitees and project owner
					$isMine = ($row->publisher_userid == $user->id);
					if($row->is_private_invite){
						$invite_ids = explode(',', $row->invite_user_id);
						if(!in_array($user->id, $invite_ids) && !$isMine)
							continue;
					}
				?>
				<div class="row-fluid project-single-section">
					<div class="span2">

						<ul class="promotions">
							<?php if($row->is_featured) : ?>
							<li data-promotion="featured"><?php echo JText::_('COM_JBLANCE_FEATURED'); ?></li>
							<?php endif; ?>
							<?php if($row->is_private) : ?>
				  			<li class="display-none" data-promotion="private"><?php echo JText::_('COM_JBLANCE_PRIVATE'); ?></li>
				  			<?php endif; ?>
							<?php if($row->is_urgent) : ?>
				  			<li class="display-none" data-promotion="urgent"><?php echo JText::_('COM_JBLANCE_URGENT'); ?></li>
				  			<?php endif; ?>
				  			<?php if($sealProjectBids || $row->is_sealed) : ?>
							<li class="display-none" data-promotion="sealed"><?php echo JText::_('COM_JBLANCE_SEALED'); ?></li>
							<?php endif; ?>
							<?php if($row->is_nda) : ?>
							<li class="display-none" data-promotion="nda"><?php echo JText::_('COM_JBLANCE_NDA'); ?></li>
							<?php endif; ?>
						</ul>
						<?php
						$attrib = 'width=90 height=90 class="img-polaroid"';
						$avatar = JblanceHelper::getLogo($row->publisher_userid, $attrib);
						echo !empty($avatar) ? LinkHelper::GetProfileLink($row->publisher_userid, $avatar) : '&nbsp;' ?>
						<div class="font14">
							<!-- <strong><?php //echo JText::_('COM_JBLANCE_POSTED_BY'); ?></strong>: --> <?php echo LinkHelper::GetProfileLink($row->publisher_userid, $buyer->biz_name); ?>
						</div>
					</div>
					<div class="span10">
						<div class="span6">
							<h3 class="media-heading">
								<?php echo LinkHelper::getProjectLink($row->id, $row->project_title); ?>
							</h3>
						</div>
						<div class="span3 text-right">
							<div class="display-none">
								<i class="icon-tags"></i> <?php echo JText::_('COM_JBLANCE_BIDS'); ?> : 
								<?php if($sealProjectBids || $row->is_sealed) : ?>
					        		<span class="label label-info"><?php echo JText::_('COM_JBLANCE_SEALED'); ?></span>
					  			<?php else : ?>
					  			<span class="badge badge-info"><?php echo $bidsCount; ?></span>
					  			<?php endif; ?>
							</div>
							<div>
								<!-- <i class="icon-flag"></i> --> <?php echo JText::_('COM_JBLANCE_STATUS'); ?> : 
								<span class="<?php echo $statusLabel; ?>"><?php echo JText::_($row->status); ?></span>
							</div>
						</div>
						<div class="span3 loacation text-center">
							<div class="font14 pull-right">
								<strong class="display-none"><?php echo JText::_('COM_JBLANCE_LOCATION'); ?></strong> <span class=""><?php echo JblanceHelper::getLocationNames($row->id_location); ?></span>
							</div>
						</div>
					</div>
					<div class="span10">
						<div class="font14 description">
							<p><?php
								echo strlen($row->description) >= 250 ? 
								substr($row->description, 0, 238) . LinkHelper::getProjectLink($row->id, ' [Read more]') : 
								$row->description;
								//echo $row->description;
								?></p>
						</div>
						<div class="font14 skills">
							<strong><?php echo JText::_('COM_JBLANCE_SKILLS_REQUIRED'); ?></strong>: <?php echo JblanceHelper::getCategoryNames($row->id_category, 'tags-link', 'project'); ?>
						</div>
						<div class="ratings">
							<?php $rate = JblanceHelper::getAvarageRate($row->publisher_userid, true); ?>
						</div>
						<div class="ratings">
							 <?php $jbuser = JblanceHelper::get('helper.user');?>
                            <div class="social_activity">
                            	<span class="like l_<?php echo $row->id?>" <?php if($user->group=='Tradesmen'){ ?> onclick="company_like('<?php echo $row->id?>','Job');" <?php } ?>>
	                            	<?php $like_status = '';
	                            	$like_status = $jbuser->isLikeByTradesmen($row->id,'Job',$user->id); 
	                            	echo ($like_status == 1) ? 'Unlike' : 'Like'; ?>
                            	</span>
                            	(<span class="like_count l_<?php echo $row->id?>">
                            		<?php $like_count = '';
                            		$like_count = $jbuser->postLikeCount($row->id,'Job');
                            		echo ($like_count>0) ? $like_count : 0;?>
                            	</span>)
                            </div>
                            
						</div>
					</div>
					
					<div class="span3 display-none">
						<div class="display-none">
							<i class="icon-tags"></i> <?php echo JText::_('COM_JBLANCE_BIDS'); ?> : 
							<?php if($sealProjectBids || $row->is_sealed) : ?>
				        		<span class="label label-info"><?php echo JText::_('COM_JBLANCE_SEALED'); ?></span>
				  			<?php else : ?>
				  			<span class="badge badge-info"><?php echo $bidsCount; ?></span>
				  			<?php endif; ?>
						</div>
						<div>
							<i class="icon-flag"></i> <?php echo JText::_('COM_JBLANCE_STATUS'); ?> : 
							<span class="<?php echo $statusLabel; ?>"><?php echo JText::_($row->status); ?></span>
						</div>
					</div>
					<div class="span2 display-none">
						<div class="bid_project_left text-center">
							<div><?php echo JText::_('COM_JBLANCE_AVG_BID'); ?></div>
							<?php if($sealProjectBids || $row->is_sealed) : ?>
				        	<span class="label label-info"><?php echo JText::_('COM_JBLANCE_SEALED'); ?></span>
				  			<?php else : ?>
				  			<span class="font16 boldfont"><?php echo JblanceHelper::formatCurrency($avg, true, false, 0); ?></span><?php echo ($row->project_type == 'COM_JBLANCE_HOURLY') ? ' / '.JText::_('COM_JBLANCE_HR') : ''; ?>
				  			<?php endif; ?>
			  			</div>
					</div>
				</div>
				<div class="lineseparator"></div>
				<?php 
				}
				?>
				<?php if(!count($this->rows)){ ?>
				<div class="alert alert-info">
				<?php echo JText::_('COM_JBLANCE_NO_MATCHING_RESULTS_FOUND'); ?>
				</div>
				<?php } ?>
				<div class="pagination pagination-centered clearfix">
					<div class="display-limit pull-right">
						<?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>&#160;
						<?php echo $this->pageNav->getLimitBox(); ?>
					</div>
					<?php echo $this->pageNav->getPagesLinks(); ?>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	<input type="hidden" name="option" value="com_jblance" />
	<input type="hidden" name="view" value="project" />
	<input type="hidden" name="layout" value="searchproject" />
	<input type="hidden" name="task" value="" />
</div>
</form>