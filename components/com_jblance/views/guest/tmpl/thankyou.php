<div class="container thank-you-section">
	
		<div class="col-sm-12">
			<div class="success-image">
				<img src="/images/thankyou.png">
			</div>
			<div class="greetings-text">
				<h1>Thank You</h1>
			</div>
			<div class="success-text">
				<p>You have been succesfully registered.You will be redirected in <span id="counter">5</span> second to login page</p>
			</div>
		</div>
	
</div>
<?php
	header( "refresh:5;url=/index.php?option=com_users&view=login&Itemid=211" );
?>

<script type="text/javascript">
function countdown() {
    var i = document.getElementById('counter');
    if (parseInt(i.innerHTML)<=0) {
        location.href = '/index.php?option=com_users&view=login&Itemid=211';
    }
    i.innerHTML = parseInt(i.innerHTML)-1;
}
setInterval(function(){ countdown(); },1000);
</script>