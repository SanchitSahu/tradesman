<?php

include('config.php');

$userid = $_REQUEST['user_id'];
$title = $_REQUEST['title'];
$description = $_REQUEST['description'];
$skills = $_REQUEST['skills'];
$final_result = array();

if ($userid == "") {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Required field missing user_id.';
} elseif ($title == "") {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Required field missing title.';
} elseif ($description == "") {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Required field missing description.';
} elseif ($skills == "") {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Required field missing skills.';
} else {

    $total = count($_FILES['picture']['name']);
    //$image_name = $_FILES['picture']['name'];
    for ($i = 0; $i < $total; $i++) {
        $image_name = $_FILES['picture']['name'][$i];
        $tmpFilePath = $_FILES['picture']['tmp_name'][$i];

        $rand = rand(0000000000, 9999999999);
        $image = "pic_" . $rand . "_" . $image_name;
        $images = "$image_name;" . "pic_" . $rand . "_" . $image_name;
        $path = "../images/jblance/portfolio/" . $image;

        //Upload the file into the temp dir
        if (move_uploaded_file($tmpFilePath, $path)) {

            $imagearray[] = $images;
        }
    }

    $image_name1 = $_FILES['portfolio_image']['name'];
    $tmpFilePath1 = $_FILES['portfolio_image']['tmp_name'];

    $rand = rand(0000000000, 9999999999);
    $image1 = "pic_" . $rand . "_" . $image_name1;
    $images1 = "$image_name1;" . "pic_" . $rand . "_" . $image_name1;
    $path1 = "../images/jblance/portfolio/" . $image1;

    //Upload the file into the temp dir
    if (move_uploaded_file($tmpFilePath1, $path1)) {

        $portfolio_image = $images1;
    }

    $insert = "insert into g6t1u_jblance_portfolio (`title`,`id_category`,`user_id`,`description`,`picture`,`attachment1`,`attachment2`,`attachment3`,`attachment4`,`attachment5`,`published`) "
            . "values ('" . mysql_real_escape_string($title) . "','" . $skills . "','" . $userid . "','" . mysql_real_escape_string($description) . "','" . $portfolio_image . "','" . $imagearray[0] . "','" . $imagearray[1] . "','" . $imagearray[2] . "','" . $imagearray[3] . "','" . $imagearray[4] . "','1')";
    $insertquery = mysql_query($insert);
    $last_id = mysql_insert_id();

    if ($last_id != "") {
        $select_job = "SELECT * FROM g6t1u_jblance_portfolio WHERE id='" . $last_id . "' ";
        $query_job = mysql_query($select_job);
        $fetch_job = mysql_fetch_assoc($query_job);

        $array_temp['success'] = "true";
        $array_temp['message'] = "Portfolio added successfully.";
        $array_temp['result'] = $fetch_job;
    } else {
        $array_temp['Success'] = 'true';
        $array_temp['Message'] = 'Portfolio not added.';
    }
}
$final_result = $array_temp;
echo json_encode($final_result);
?>