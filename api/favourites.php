<?php
	include('config.php');

	$user_id 	= $_REQUEST['user_id'];
	$page_size 		= $_REQUEST['page_size'];
	$current_page 	= $_REQUEST['current_page'];
	$imagepath = 'http://'.$_SERVER['SERVER_NAME'].'/images/jblance/profile/';
	
	if(@$page_size=='' || @$page_size==''){
		$page_size=10;
	}

	if(@$current_page=='' || @$current_page==''){
		$current_page=0;
	}else{
		$current_page=($current_page*$page_size)-$page_size;
	}

	if($user_id == "")
	{
		$array_temp['Success'] = 'false';
		$array_temp['Message'] = 'Required field missing user_id.';		
	}
	else
	{
		// SELECT Favourite users from table

		$select_fav = "SELECT target FROM g6t1u_jblance_favourite WHERE actor= '".$user_id."' ORDER BY id DESC LIMIT ".$current_page.", ".$page_size;
		$query_fav = mysql_query($select_fav);
		
		while($fetch_fav = mysql_fetch_assoc($query_fav))
		{
			// SELECT id,name,username,email,picture and id_location(start) //
			
			$select_user = "SELECT a.id,a.name,a.username,a.email,b.picture,b.ug_id,b.id_location FROM g6t1u_users AS a INNER JOIN g6t1u_jblance_user AS b ON a.id = b.user_id WHERE a.id='".$fetch_fav['target']."' ";

			$query_user = mysql_query($select_user);
			$fetch_user = mysql_fetch_assoc($query_user);

			$id['id'] = $fetch_user['id'];
			if($id['id'] == ""){
				$id['id'] = "";
			}
			$name['name'] = $fetch_user['name'];
			if($name['name'] == ""){
				$name['name'] = "";
			}
			$username['username'] = $fetch_user['username'];
			if($username['username'] == ""){
				$username['username'] = "";
			}
			$email['email'] = $fetch_user['email'];
			if($email['email'] == ""){
				$email['email'] = "";
			}
			$ug_id['usergroup'] = $fetch_user['ug_id'];
			if($ug_id['usergroup'] == ""){
				$ug_id['usergroup'] = "";
			}
			if($ug_id['usergroup'] == 1){
				$ug_id['usergroup'] = "Tradesmen";
			}
			if($ug_id['usergroup'] == 2){
				$ug_id['usergroup'] = "Company";
			}

			$picture['picture'] = ($fetch_user['picture']) ? $imagepath.$fetch_user['picture']: '';
			//$id_location['id_location'] = $fetch_user['id_location'];

			// SELECT id,name,username,email,picture and id_location(end) //

			// SELECT location name from g6t1u_jblance_location table(start) //

			$select_loc = "SELECT title FROM g6t1u_jblance_location WHERE id='".$fetch_user['id_location']."' ";
			$query_loc = mysql_query($select_loc);
			$fetch_loc = mysql_fetch_assoc($query_loc);
			$id_location['id_location'] = $fetch_loc['title'];
			if($id_location['id_location'] == ""){
				$id_location['id_location'] = "";
			}

			// SELECT location name from g6t1u_jblance_location table(end) //


			// SELECT total ratings from g6t1u_jblance_rating (start) //

			$select_t_ratings = "SELECT *,count(*) AS  total_ratings FROM g6t1u_jblance_rating WHERE actor = '".$fetch_user['id']."' ";
			$query_t_ratings = mysql_query($select_t_ratings);
			$fetch_t_ratings = mysql_fetch_assoc($query_t_ratings);

			$total_ratings['total_ratings'] = $fetch_t_ratings['total_ratings'];


			$select_avg_ratings = "SELECT AVG(quality_clarity) AS quality_clarity,AVG(communicate) AS communicate,AVG(expertise_payment) AS expertise_payment,AVG(professional) AS professional,AVG(hire_work_again) AS hire_work_again FROM g6t1u_jblance_rating WHERE actor = '".$fetch_user['id']."'";

			$query_avg_ratings = mysql_query($select_avg_ratings);
			$fetch_avg_ratings = mysql_fetch_assoc($query_avg_ratings);

			$avg1 = ($fetch_avg_ratings['quality_clarity']+
					$fetch_avg_ratings['communicate']+
					$fetch_avg_ratings['expertise_payment']+
					$fetch_avg_ratings['professional']+
					$fetch_avg_ratings['hire_work_again'])/5;

			$avg_rating['avg_ratings'] = "".round($avg1,1)."";

			// SELECT total ratings from g6t1u_jblance_rating(end) //

			// SELECT online status from g6t1u_session (start) //

			$select_online_user = "SELECT * FROM g6t1u_session WHERE userid = '".$fetch_user['id']."' ";
			$query_online_user = mysql_query($select_online_user);
			$num_online_user = mysql_num_rows($query_online_user);

			if($num_online_user > 0)
			{
				$ol_status['online_status'] = 'online';
			}
			else
			{
				$ol_status['online_status'] = 'offline';
			}

			// SELECT online status from g6t1u_session (end) //

		$array_temp['Success'] = 'true';
		$array_temp['Message'] = 'Favourite Listing';	
		$array_temp['result'][] = array_merge($id,$name,$username,$email,$picture,$ug_id,$id_location,$total_ratings,$avg_rating,$ol_status);
		}
		if ($id['id'] == "") {
			$array_temp['Success'] = 'false';
			$array_temp['Message'] = 'No user in your favourite';
		}
		
	}


$final_result = $array_temp;

echo json_encode($final_result);
?>