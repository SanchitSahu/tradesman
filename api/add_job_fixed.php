<?php

include('config.php');

$date = date("Y-m-d H:i:s");

$user_id = $_REQUEST['user_id'];
$project_title = $_REQUEST['project_title'];
$description = $_REQUEST['description'];
$category_id = $_REQUEST['category_id'];
$publish_date = $_REQUEST['start_date'];
$expires = $_REQUEST['expires'];
$status = 'COM_JBLANCE_OPEN';
$budgetmin = $_REQUEST['budgetmin'];
$budgetmax = $_REQUEST['budgetmax'];
//$is_featured = $_REQUEST['is_featured'];
$project_type = 'COM_JBLANCE_FIXED';
$commitment = '{"period":"","interval":"COM_JBLANCE_DAY","undefined":"notsure"}';
$id_location = $_REQUEST['id_location'];
$select_days = $_REQUEST['select_days'];
$select_featured = $_REQUEST['select_featured'];


if ($user_id == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field user_id.";
} else if ($project_title == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field project_title.";
} else if ($description == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field description.";
} else if ($category_id == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field category_id.";
} else if ($publish_date == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field start_date.";
} else if ($expires == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field expires.";
} else if ($budgetmin == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field budgetmin.";
} else if ($budgetmax == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field budgetmax.";
} else if ($project_type == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field project_type.";
} else if ($id_location == "") {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field id_location.";
} else {
    $select_fund = "SELECT SUM(fund_plus) AS fund_plus,SUM(fund_minus) AS fund_minus FROM g6t1u_jblance_transaction WHERE user_id='" . $user_id . "' ";
    $query_fund = mysql_query($select_fund);
    while ($fetch_fund = mysql_fetch_assoc($query_fund)) {
        $plus_fund = $fetch_fund['fund_plus'];
        $minus_fund = $fetch_fund['fund_minus'];
        $total_fund = $plus_fund - $minus_fund;
    }


    if ($total_fund > $select_days) {

        if ($select_featured != "") {

            $featured = 1;
            $project_promotion = "Project promotion fee for - " . $project_title;
            $transaction = "insert into g6t1u_jblance_transaction (`date_trans`,`transaction`,`fund_plus`,`fund_minus`,`user_id`) values ('" . $date . "','" . $project_promotion . "','0','" . $select_days . "','" . $user_id . "')";
            $transactionquery = mysql_query($transaction);
        } else {
            $featured = 0;
        }

        $insert_job = "INSERT INTO g6t1u_jblance_project 
		(
			project_title,
			id_category,
			start_date,
			create_date,
			expires,
			publisher_userid,
			status,
			budgetmin,
			budgetmax,
			description,
			is_featured,
			paid_amt,
			approved,
			metakey,
			metadesc,
			project_type,
			commitment,
			params,
			id_location
		) 
		VALUES 
		(
			'" . mysql_real_escape_string($project_title) . "',
			'$category_id',
			'$publish_date',
			'$date',
			'$expires',
			'$user_id',
			'$status',
			'$budgetmin',
			'$budgetmax',
			'" . mysql_real_escape_string($description) . "',
			'$featured',
			'$select_days',
			'1',
			' ',
			' ',
			'$project_type',
			'$commitment',
			'{}',
			'$id_location'
		)";

        $query_job = mysql_query($insert_job);
        $last_id = mysql_insert_id();
    }

    if ($total_fund < $select_days) {
        $array_temp['success'] = "false";
        $array_temp['message'] = "You dont have enough fund.";
        $array_temp['total_fund'] = $total_fund;
    } elseif ($last_id != "") {



        $total = count($_FILES['picture']['name']);
        //$image_name = $_FILES['picture']['name'];
        for ($i = 0; $i < $total; $i++) {
            $image_name = $_FILES['picture']['name'][$i];
            $tmpFilePath = $_FILES['picture']['tmp_name'][$i];

            $rand = rand(0000000000, 9999999999);
            $image = "proj_" . $last_id . "_" . $rand . "_" . $image_name;
            $images = "$image_name;" . "pic_" . $rand . "_" . $image_name;
            $path = "../images/jblance/project/" . $image;
            //Upload the file into the temp dir
            if (move_uploaded_file($tmpFilePath, $path)) {

                $insert = "insert into g6t1u_jblance_project_file (`project_id`,`file_name`,`show_name`,`hash`) values ('" . $last_id . "','" . $image . "','" . $image_name . "','')";
                $insertquery = mysql_query($insert);
                //$last_id = mysql_insert_id();

                if ($last_id != "") {
                    $select_file = "SELECT * FROM g6t1u_jblance_project_file WHERE project_id='" . $last_id . "' ";
                    $query_file = mysql_query($select_file);
                    $fetch_file = mysql_fetch_assoc($query_file);

                    $array_temp['success'] = "true";
                    $array_temp['message'] = "Job added successfully.";
                    $array_temp['result'] = $fetch_file;
                }
            }
        }



        $select_job = "SELECT * FROM g6t1u_jblance_project WHERE id='" . $last_id . "' ";
        $query_job = mysql_query($select_job);
        $fetch_job = mysql_fetch_assoc($query_job);

        $array_temp['success'] = "true";
        $array_temp['message'] = "added new job.";
        $array_temp['result'] = $fetch_job;
    } else {
        $array_temp['success'] = "false";
        $array_temp['message'] = "job not added.";
    }
}

$final_result = $array_temp;
echo json_encode($final_result);
?>