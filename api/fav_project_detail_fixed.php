<?php
	include('config.php');
	//date_default_timezone_set('Asia/Calcutta');
	
	$project_id = $_REQUEST['project_id'];
	$image_path = 'http://'.$_SERVER['SERVER_NAME'].'/images/jblance/profile/';
	$file_path = 'http://'.$_SERVER['SERVER_NAME'].'/images/jblance/project/';

	if($project_id=="")
	{
		$array_temp['success'] = 'false';
		$array_temp['message'] = 'Required field missing project_id.';	
	}
	else
	{
		$select_project = "SELECT a.*,b.file_name,b.show_name FROM g6t1u_jblance_project AS a INNER JOIN g6t1u_jblance_project_file AS b ON a.id = b.project_id WHERE a.id = '".$project_id."' AND a.status = 'COM_JBLANCE_OPEN' ";
		$query_project = mysql_query($select_project);
		$num_project = mysql_num_rows($query_project);

		if($num_project==0)
		{
			$array_temp['success'] = 'false';
			$array_temp['message'] = 'no record found.';
		}
		while($fetch_project = mysql_fetch_assoc($query_project))
		{
			$select_pic = "SELECT picture FROM g6t1u_jblance_user WHERE user_id = '".$fetch_project['publisher_userid']."' ";
			$query_pic = mysql_query($select_pic);
			$fetch_pic = mysql_fetch_assoc($query_pic);
			$pic['picture'] = $image_path.$fetch_pic['picture'];

			// project idate(format)
			$id['project_id'] = $fetch_project['id'];

			//project title
			$project_title['project_title'] = $fetch_project['project_title'];

			$id_location = $fetch_project['id_location'];
			$select_location = "SELECT path FROM g6t1u_jblance_location WHERE id = '".$id_location."' ";
			$query_location = mysql_query($select_location);
			$fetch_location = mysql_fetch_assoc($query_location);

			//location
			$location['location'] = $fetch_location['path'];

			//category
			$category['category'] = $fetch_project['id_category'];

			//image file name
			$file_name['show_name'] = $fetch_project['show_name'];
			$file_name['file_name'] = $file_path.$fetch_project['file_name'];
			$file['file'][] = array_merge($file_name);

			//start date
			$publish_date['publish_date'] = $fetch_project['start_date'];

			//min and max budget
			$min_budget['min_budget'] = $fetch_project['budgetmin'];
			$max_budget['max_budget'] = $fetch_project['budgetmax'];

			$exp_days = $fetch_project['expires'];
			//description
			$description['description'] = $fetch_project['description'];

			$array_temp['success'] = 'true';
			$array_temp['message'] = 'project details';

			//$from_time = strtotime($fetch_project['start_date']);
			$fro_date = $fetch_project['start_date'];
		}

			$days = '+'.$exp_days.' day';
			$date = date_create($fro_date);
			$date = date_modify($date, $days);
			$from_time = date_format($date, 'Y-m-d H:i:s');
			$from_time = strtotime($from_time);

			// time difference //
			$to_time = time();
			//$from_time = strtotime($fetch_project['start_date']);
			$hours = round(($from_time - $to_time)/3600);
			$day = round($hours/24);
			$remain_hours = round($hours%24);
			$exp_date =  $day." days ".$remain_hours." hours";
			

			$expires_in['expires_in'] = $exp_date;
			//Merge Results
			$array_temp['result'][] = array_merge($id,$project_title,$description,$location,$category,$publish_date,$min_budget,$max_budget,$pic,$file,$expires_in);
	}

	$final_result = $array_temp;
	echo json_encode($final_result);
?>