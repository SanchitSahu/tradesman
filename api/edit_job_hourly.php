<?php

include('config.php');

$project_id = $_REQUEST['project_id'];
$project_title = $_REQUEST['project_title'];
$description = $_REQUEST['description'];
$category_id = $_REQUEST['category_id'];
$date = date("Y-m-d H:i:s");
$publish_date = $_REQUEST['start_date'];
$project_duration = $_REQUEST['project_duration'];
$expire = $_REQUEST['expires'];
$status = 'COM_JBLANCE_OPEN';
$budgetmin = $_REQUEST['budgetmin'];
$budgetmax = $_REQUEST['budgetmax'];
$is_featured = $_REQUEST['is_featured'];
$project_type = 'COM_JBLANCE_HOURLY';
$hours_of_work_required = $_REQUEST['hours_of_work_required'];
$work_hours = $_REQUEST['work_hours'];
$work_hours_type = $_REQUEST['work_hours_type'];
$id_location = $_REQUEST['id_location'];
$select_days = $_REQUEST['select_days'];
$select_featured = $_REQUEST['select_featured'];
$file1_name = $_FILES['file1']['name'];
$file_tmp1 = $_FILES['file1']['tmp_name'];
$file2_name = $_FILES['file2']['name'];
$file_tmp2 = $_FILES['file2']['tmp_name'];
$file3_name = $_FILES['file3']['name'];
$file_tmp3 = $_FILES['file3']['tmp_name'];
$file4_name = $_FILES['file4']['name'];
$file_tmp4 = $_FILES['file4']['tmp_name'];
$file5_name = $_FILES['file5']['name'];
$file_tmp5 = $_FILES['file5']['tmp_name'];


if ($project_id != "") {

    $select_fund = "SELECT SUM(fund_plus) AS fund_plus,SUM(fund_minus) AS fund_minus FROM g6t1u_jblance_transaction WHERE user_id='" . $user_id . "' ";
    $query_fund = mysql_query($select_fund);
    while ($fetch_fund = mysql_fetch_assoc($query_fund)) {
        $plus_fund = $fetch_fund['fund_plus'];
        $minus_fund = $fetch_fund['fund_minus'];
        $total_fund = $plus_fund - $minus_fund;
    }

    $check_featured = "SELECT is_featured FROM g6t1u_jblance_project WHERE id = '" . $project_id . "' ";
    $query_check_featured = mysql_query($check_featured);
    $fetch_check_featured = mysql_fetch_assoc($query_check_featured);
    //echo $fetch_check_featured['is_featured']; exit;
    if ($select_featured != "") {
        if ($fetch_check_featured['is_featured'] == 1) {
            $featured = 1;
        } else {
            $featured = 1;
            $project_promotion = "Project promotion fee for - " . mysql_real_escape_string($project_title);
            $transaction = "insert into g6t1u_jblance_transaction (`date_trans`,`transaction`,`fund_plus`,`fund_minus`,`user_id`) values ('" . $date . "','" . $project_promotion . "','0','" . $select_days . "','" . $user_id . "')";
            $transactionquery = mysql_query($transaction);

            if ($select_days != "") {
                $update_featured_days = "UPDATE g6t1u_jblance_project SET paid_amt = '$select_days' WHERE id = '" . $project_id . "' ";
                $query_featured_days = mysql_query($update_featured_days);
            }
        }
    } else {
        $featured = 0;
    }

    $select_project = "SELECT * FROM g6t1u_jblance_project WHERE id = '" . $project_id . "' AND project_type='COM_JBLANCE_HOURLY' ";
    $query_project = mysql_query($select_project);
    $fetch_project = mysql_fetch_assoc($query_project);
    $num_project = mysql_num_rows($query_project);

    if ($num_project == 1) {

        if ($project_title != "") {
            $update_project_title = "UPDATE g6t1u_jblance_project SET project_title = '" . mysql_real_escape_string($project_title) . "' WHERE id = '" . $project_id . "' ";
            $query_project_titile = mysql_query($update_project_title);
        }

        if ($category_id != "") {
            $update_category_id = "UPDATE g6t1u_jblance_project SET id_category = '$category_id' WHERE id = '" . $project_id . "' ";
            $query_category_id = mysql_query($update_category_id);
        }

        if ($publish_date != "") {
            $update_publish_date = "UPDATE g6t1u_jblance_project SET start_date = '$publish_date' WHERE id = '" . $project_id . "' ";
            $query_publish_date = mysql_query($update_publish_date);
        }


        if ($expire != "") {
            $update_expires = "UPDATE g6t1u_jblance_project SET expires = '$expire' WHERE id = '" . $project_id . "' ";
            $query_expires = mysql_query($update_expires);
        }

        if ($description != "") {
            $update_description = "UPDATE g6t1u_jblance_project SET description = '" . mysql_real_escape_string($description) . "' WHERE id = '" . $project_id . "' ";
            $query_description = mysql_query($update_description);
        }

        if ($project_duration != "") {
            $update_project_duration = "UPDATE g6t1u_jblance_project SET project_duration = '$project_duration' WHERE id = '" . $project_id . "' ";
            $query_project_duration = mysql_query($update_project_duration);
        }

        if ($id_location != "") {
            $update_id_location = "UPDATE g6t1u_jblance_project SET id_location = '$id_location' WHERE id = '" . $project_id . "' ";
            $query_id_location = mysql_query($update_id_location);
        }

        if ($budgetmin != "") {
            $update_budgetmin = " UPDATE g6t1u_jblance_project SET budgetmin = '$budgetmin' WHERE id = '" . $project_id . "' ";
            $query_budgetmin = mysql_query($update_budgetmin);
        }

        if ($budgetmax != "") {
            $update_budgetmax = " UPDATE g6t1u_jblance_project SET budgetmax = '$budgetmax' WHERE id = '" . $project_id . "' ";
            $query_budgetmax = mysql_query($update_budgetmax);
        }

        if ($work_hours_type != "") {
            if ($work_hours_type == "DAY") {
                $commitment = '{"undefined":"sure","period":' . '"' . $work_hours . '"' . ',"interval":"COM_JBLANCE_DAY"}';
            } else if ($work_hours_type == "WEEK") {
                $commitment = '{"undefined":"sure","period":' . '"' . $work_hours . '"' . ',"interval":"COM_JBLANCE_WEEK"}';
            } else if ($work_hours_type == "MONTH") {
                $commitment = '{"undefined":"sure","period":' . '"' . $work_hours . '"' . ',"interval":"COM_JBLANCE_MONTH"}';
            } else {
                $commitment = '{"undefined":"sure","period":' . '"' . $work_hours . '"' . ',"interval":""}';
            }
        }

        if ($hours_of_work_required != "") {
            $update_hours_status = " UPDATE g6t1u_jblance_project SET commitment = '$commitment' WHERE id = '" . $project_id . "' ";
            $query_hours_status = mysql_query($update_hours_status);
        }

        if ($select_featured != "") {
            if ($select_featured == 1 || $select_featured == 0) {
                $update_is_featured = " UPDATE g6t1u_jblance_project SET is_featured = '$select_featured' WHERE id = '" . $project_id . "' ";
                $query_is_featured = mysql_query($update_is_featured);
            } else {
                $array_temp['success'] = "false";
                $array_temp['message'] = "Please enter 1 or 0 for featured project.";
            }
        }
    }

    $select_update_project = "SELECT * FROM g6t1u_jblance_project WHERE id = '" . $project_id . "' AND project_type='COM_JBLANCE_HOURLY' ";
    $query_update_project = mysql_query($select_update_project);
    $fetch_update_project = mysql_fetch_assoc($query_update_project);

    $project_id = $fetch_update_project['id'];
    $project_title = $fetch_update_project['project_title'];
    $description = $fetch_update_project['description'];
    $category_id = $fetch_update_project['id_category'];
    $publishing_date = $fetch_update_project['start_date'];
    $expires = $fetch_update_project['expires'];
    $is_featured = $fetch_update_project['is_featured'];
    $featured_days = $fetch_update_project['paid_amt'];
    $budgetmin = $fetch_update_project['budgetmin'];
    $budgetmax = $fetch_update_project['budgetmax'];
    $location = $fetch_update_project['id_location'];
    $project_duration = $fetch_update_project['project_duration'];


    $check_file = "SELECT * FROM g6t1u_jblance_project_file WHERE project_id='" . $project_id . "' ";
    $query_check_file = mysql_query($check_file);
    $fetch_check_file = mysql_fetch_assoc($query_check_file);

    $check_file_id = "SELECT id FROM g6t1u_jblance_project_file WHERE project_id='" . $project_id . "' ";
    $query_check_file_id = mysql_query($check_file_id);

    while ($fetch_check_file_id = mysql_fetch_assoc($query_check_file_id)) {
        $file_id[] = $fetch_check_file_id['id'];
    }

    if ($fetch_check_file['id'] == "") {
        $total = count($_FILES['picture']['name']);

        for ($i = 0; $i < $total; $i++) {
            $image_name = $_FILES['picture']['name'][$i];
            $tmpFilePath = $_FILES['picture']['tmp_name'][$i];

            $rand = rand(0000000000, 9999999999);
            $image = "proj_" . $project_id . "_" . $rand . "_" . $image_name;
            $images = "$image_name;" . "pic_" . $rand . "_" . $image_name;
            $path = "../images/jblance/project/" . $image;


            if (move_uploaded_file($tmpFilePath, $path)) {

                $insert = "insert into g6t1u_jblance_project_file (`project_id`,`file_name`,`show_name`,`hash`) values ('" . $project_id . "','" . $image . "','" . $image_name . "','')";
                $insertquery = mysql_query($insert);
            }


            if ($project_id != "") {
                $select_file = "SELECT * FROM g6t1u_jblance_project_file WHERE project_id='" . $project_id . "' ";
                $query_file = mysql_query($select_file);
                $fetch_file = mysql_fetch_assoc($query_file);
            }
        }
    } else {
        $rand = rand(0000000000, 9999999999);
        $attachment1 = "proj_" . $project_id . "_" . $rand . "_" . $file1_name;
        $attachment1s = "$file1_name;" . "pic_" . $rand . "_" . $file1_name;
        $path1 = "../images/jblance/project/" . $attachment1;

        if (move_uploaded_file($file_tmp1, $path1)) {
            $updateatt1 = "update g6t1u_jblance_project_file SET file_name = '$attachment1', show_name = '$file1_name' WHERE project_id = '" . $project_id . "' AND id = '" . $file_id[0] . "'";
            $updateatt1query = mysql_query($updateatt1);
        }

        $attachment2 = "proj_" . $project_id . "_" . $rand . "_" . $file2_name;
        $attachment2s = "$file2_name;" . "pic_" . $rand . "_" . $file2_name;
        $path2 = "../images/jblance/project/" . $attachment2;

        if (move_uploaded_file($file_tmp2, $path2)) {
            $updateatt2 = "update g6t1u_jblance_project_file SET file_name = '$attachment2', show_name = '$file2_name' WHERE project_id = '" . $project_id . "' AND id = '" . $file_id[1] . "'";
            $updateatt2query = mysql_query($updateatt2);
        }

        if (move_uploaded_file($file_tmp3, $path3)) {
            $updateatt3 = "update g6t1u_jblance_project_file SET file_name = '$attachment3', show_name = '$file3_name' WHERE project_id = '" . $project_id . "' AND id = '" . $file_id[2] . "'";
            $updateatt3query = mysql_query($updateatt3);
        }

        $attachment4 = "proj_" . $project_id . "_" . $rand . "_" . $file4_name;
        $attachment4s = "$file4_name;" . "pic_" . $rand . "_" . $file4_name;
        $path4 = "../images/jblance/project/" . $attachment4;

        if (move_uploaded_file($file_tmp4, $path4)) {
            $updateatt4 = "update g6t1u_jblance_project_file SET file_name = '$attachment4', show_name = '$file4_name' WHERE project_id = '" . $project_id . "' AND id = '" . $file_id[3] . "'";
            $updateatt4query = mysql_query($updateatt4);
        }

        $attachment5 = "proj_" . $project_id . "_" . $rand . "_" . $file5_name;
        $attachment5s = "$file5_name;" . "pic_" . $rand . "_" . $file5_name;
        $path5 = "../images/jblance/project/" . $attachment5;

        if (move_uploaded_file($file_tmp5, $path5)) {
            $updateatt5 = "update g6t1u_jblance_project_file SET file_name = '$attachment5', show_name = '$file5_name' WHERE project_id = '" . $project_id . "' AND id = '" . $file_id[4] . "'";
            $updateatt5query = mysql_query($updateatt5);
        }
    }

    $array_temp['success'] = "true";
    $array_temp['message'] = "edit project details.";
    $array_temp['result']['project_id'] = $project_id;
    $array_temp['result']['project_title'] = $project_title;
    $array_temp['result']['description'] = $description;
    $array_temp['result']['skills'] = $category_id;
    $array_temp['result']['expires'] = $expires;
    $array_temp['result']['project_duration'] = $project_duration;
    $array_temp['result']['project_type'] = $project_type;
    $array_temp['result']['is_featured'] = $is_featured;
    $array_temp['result']['budgetmin'] = $budgetmin;
    $array_temp['result']['budgetmax'] = $budgetmax;
    $array_temp['result']['location'] = $location;
} else {
    $array_temp['success'] = "false";
    $array_temp['message'] = "missing required field project_id.";
}


$final_result = $array_temp;
echo json_encode($final_result);
?>