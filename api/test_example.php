<?
class Model
{
    public $text;
    public function __construct() 
    {
        $this->text = 'this is  a test example...';
    }        
}

class View 
{
    private $model;
    public function __construct(Model $model) 
    {
        $this->model = $model;
    }

    

    public function output()
    {
        return '<h5>' . $this->model->text .'</h5>';
    }

}

class Controller 
{
    private $model;
    public function __construct(Model $model) {

        $this->model = $model;
    }
}

//initiate the triad

$model = new Model();

//It is important that the controller and the view share the model

$controller = new Controller($model);

$view = new View($model);

echo $view->output();
?>