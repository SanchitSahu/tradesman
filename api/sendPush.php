<?php

function sendIOSNotification($device_id, $message, $data = array(), $mode = 'sandbox') {
    if ($mode == 'production') {
        $gateway = 'ssl://gateway.push.apple.com:2195';
    } else {
        $gateway = 'ssl://gateway.sandbox.push.apple.com:2195';
    }

    $ctx = stream_context_create();
    $passphrase = '1234';

    stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', 1234);

    $fp = stream_socket_client($gateway, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
    $body['aps'] = array(
        'badge' => 1,
        'alert' => $message,
        'sound' => 'default',
        'data' => $data
    );

    $payload = json_encode($body);

    $regId = explode(',', $device_id);

    for ($i = 0; $i < count($regId); $i++) {
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $regId[$i]) . pack('n', strlen($payload)) . $payload;
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
    }
    return true;
}

function send_android_push($device_id, $message, $data = array()) {
    return true;
}
