<?php

	include 'config.php';

	$final_result 	= array();
	$imagepath 		= 'http://'.$_SERVER['SERVER_NAME'].'/images/jblance/profile/';

	$user_id 		= $_REQUEST['user_id'];
	$keyword 		= $_REQUEST['keyword'];
	$name 			= $_REQUEST['name'];
	$user_name 		= $_REQUEST['user_name'];
	$country 		= $_REQUEST['country_id'];
	$city 			= $_REQUEST['city'];
	$rate 			= $_REQUEST['rate'];
	$ratings 		= $_REQUEST['ratings'];
	$page_size 		= $_REQUEST['page_size'];
	$current_page 	= $_REQUEST['current_page'];
	$skills 		= rtrim($_REQUEST['skills'],',');
	$skillsexplode 	= explode(',',$skills);
	$cat_num 		= count($skillsexplode);
	
	if(@$page_size=='' || @$page_size==''){
		$page_size=10;
	}
	if(@$current_page=='' || @$current_page==''){
		$current_page=0;
	}else{
		$current_page=($current_page*$page_size)-$page_size;
	}

	if($user_id == "")
	{
		$array_temp['Success'] = 'false';
		$array_temp['Message'] = 'Required field missing user_id.';		
	}
	else
	{

		if($keyword!="")
		{
			$and = " AND b.name LIKE '%".$keyword."%' ";
		}

		/*if($country!="")
		{
			$and.= " AND a.id_location = '".$country."' ";
		}*/

		if($city!="")
		{
			$and.= " AND a.id_location = '".$city."' ";
		}

		if($user_name!="")
		{
			$and.= " AND b.username LIKE '%".$user_name."%' ";
		}

		if($rate!="")
		{
			$and.= " AND a.rate LIKE '%".round($rate*10)."%' ";
		}


		if($skills!="")
		{
			$and_cat1 = '';
			//for($i=0;$i<$cat_num;$i++)
			//	{
					// $and_cat[$i] = " AND a.id_category like '%".$skillsexplode[$i]."%' ";
					// if($i>0)
					// {

					// 	$and_cat[$i] = " AND a.id_category like '%".$skillsexplode[$i]."%' ";
					// }
					
					// if(isset($_REQUEST['skills']))
					// {
					// 	$and.= " ".$and_cat[$i]."";
					// }
			//	}

			foreach ($skillsexplode as $key=>$value) {

				if($and_cat1==''){
					if($value){
						$and_cat[$key] = " FIND_IN_SET(".$value.", a.id_category) ";
						$and_cat1 = " FIND_IN_SET(".$value.", a.id_category) ";
					}
				}else{
					if($value){
						$and_cat[$key] = " OR FIND_IN_SET(".$value.", a.id_category) ";
						$and_cat1.= " OR FIND_IN_SET(".$value.", a.id_category) ";
					}
				}
			}
					
			if(isset($_REQUEST['skills']) && $and_cat1!='')
			{
				$and.= " AND (".$and_cat1.") ";
			}
		}

		if($name!="")
		{
			
			if($name=="name_asc")
			{
				$and.= " ORDER BY b.name ASC ";
			}

			if($name=="name_desc")
			{
				$and.= " ORDER BY b.name DESC ";
			}
			
			if($name=="username_asc")
			{
				$and.= " ORDER BY b.username ASC ";
			}

			if($name=="username_desc")
			{
				$and.= " ORDER BY b.username DESC ";
			}
			
			if($name=="rate_asc")
			{
				$and.= " ORDER BY a.rate ASC ";
			}

			if($name=="rate_desc")
			{
				$and.= " ORDER BY a.rate DESC ";
			}

			if($name=="rating_asc")
			{
				
			}

			if($name=="rating_desc")
			{
				
			}
			
		}

		/*if($ratings!="")
		{
			$and.= " AND a.rate = ".round($ratings*10)." ";
		}*/

		$select1 = '';
		$where1 = '';
		if($ratings!="")
		{

			//$and.= "rating = '".$rating."'";

			//$select1 = "IFNULL((SELECT AVG((quality_clarity+communicate+expertise_payment+professional+hire_work_again)/5) FROM g6t1u_jblance_rating WHERE actor = a.user_id),'') AS avg_user_rating";
			//echo "(SELECT AVG((quality_clarity+communicate+expertise_payment+professional+hire_work_again)/5) FROM g6t1u_jblance_rating WHERE $and) AS rating";
			//echo "<pre>"; print_r($select);


			$where1 = "  AND round((SELECT AVG((quality_clarity+communicate+expertise_payment+professional+hire_work_again)/5) FROM g6t1u_jblance_rating WHERE actor = a.user_id)) = round(IFNULL(".$ratings.",0)) ";
		}

		$select = "SELECT a.user_id, a.picture, a.ug_id, a.id_category, a.address, a.postcode, a.mobile, a.home, b.username, b.name, c.value AS basic_info, d.title As location, a.rate As hourly_rate, IFNULL(round((SELECT AVG((quality_clarity+communicate+expertise_payment+professional+hire_work_again)/5) FROM g6t1u_jblance_rating WHERE actor = a.user_id),1),'0') AS avg_user_rating FROM g6t1u_jblance_user AS a INNER JOIN g6t1u_users AS b ON a.user_id=b.id INNER JOIN g6t1u_jblance_custom_field_value AS c ON a.user_id=c.userid LEFT JOIN g6t1u_jblance_location AS d ON a.id_location=d.id WHERE a.ug_id=1 AND b.availability=1 AND c.fieldid=3  ". $where1 . $and. " LIMIT ".$current_page.", ".$page_size;

		$selectquery = mysql_query($select);

		
			while($filterdata = mysql_fetch_assoc($selectquery))
			{
		
				$filter = $filterdata;
				$user_id_tm = $filter['user_id'];
				$location_tm = $filter['location'];
				if(!isset($filter['location'])){
					$filter['location']="";
				}
				if ($filter['hourly_rate'] == "") 
				{
					$filter['hourly_rate'] = "";
				}
				
				$location = "SELECT title, path FROM g6t1u_jblance_location WHERE title='".$location_tm."' ";
				$location_query = mysql_query($location);
				$location_fetch = mysql_fetch_assoc($location_query);
				$location_name['location_name'] = $location_fetch['title'];
				$location_parent['parent_id'] = $location_fetch['parent_id'];
				$location_path['path'] = $location_fetch['path'];
				$country_name = explode('/',$location_path['path']);
				$filter['parent_location'] = $country_name[0];

				$select_online_user = mysql_query("SELECT * FROM g6t1u_session 
													WHERE userid='".$user_id_tm."'
												  ");
				$num_ol_users = mysql_num_rows($select_online_user);


				if($num_ol_users>=1)
				{
					$filter['online_status'] = "online";
				}
				else
				{
					$filter['online_status'] = "offline";
				}

				if ($filterdata['mobile'] == '') {
					$filter['mobile'] = '';
				} else {
					$filter['mobile'] = $filterdata['mobile'];
				}

				$select_fav = "SELECT * FROM g6t1u_jblance_favourite WHERE actor= '".$user_id."' AND target= '".$user_id_tm."'";
				//echo "SELECT * FROM g6t1u_jblance_favourite WHERE actor= '".$user_id."' AND target= '".$user_id_tm."'" . '<br>';
				 $query_fav = mysql_query($select_fav);
				
				 while($fetch_fav = mysql_fetch_assoc($query_fav))
				{
					$fav_id = $fetch_fav['id'];
					$fav_user_id = $fetch_fav['target'];
					
					if ($user_id_tm == $fav_user_id) {
						$filter['favourite'] = 1;
					} else {
						$filter['favourite'] = 0;
					}
				}

				$select_rating = mysql_query("SELECT * FROM g6t1u_jblance_rating WHERE actor = '".$user_id_tm."' ");
				$num_rating = mysql_num_rows($select_rating);
				$select_rating_avg = 
						mysql_query("	SELECT 
										round(AVG(quality_clarity),1) AS quality_clarity_avg,
										round(AVG(communicate),1) AS communicate_avg,
										round(AVG(expertise_payment),1) AS expertise_payment_avg,
										round(AVG(professional),1) AS professional_avg,
										round(AVG(hire_work_again),1) AS hire_work_again_avg 
										FROM g6t1u_jblance_rating 
										WHERE actor = '".$user_id_tm."'
									");

				
				$fetch_communicate_rating = mysql_fetch_assoc($select_rating_avg);

				$quality_clarity_avg = 
				$fetch_communicate_rating['quality_clarity_avg'];

				$communicate_avg = 
				$fetch_communicate_rating['communicate_avg'];

				$expertise_payment_avg = 
				$fetch_communicate_rating['expertise_payment_avg'];

				$professional_avg = 
				$fetch_communicate_rating['professional_avg'];

				$hire_work_again_avg = 
				$fetch_communicate_rating['hire_work_again_avg'];
			
				$quality_clarity = round($quality_clarity_avg);	
				$communicate = round($communicate_avg);	
				$expertise_payment = round($quality_clarity_avg);	
				$professional = round($quality_clarity_avg);	
				$hire_work_again = round($quality_clarity_avg);	

				if($num_rating>=1)
				{
					$avg = ($quality_clarity + $communicate + $expertise_payment + $professional + $hire_work_again)/5;
					$filter['avg_ratings'] = $avg;	
				}
					
				if($filter['picture']=="")
				{
					$filter['picture'] = "";
				}
				else
				{
					$filter['picture'] = ($filter['picture']) ? $imagepath.$filter['picture']:'';
				}

				if($num_rating=="0")
				{
					$filter['avg_ratings'] = "";
					$filter['total_ratings'] = "";
				}
				else
				{
					$filter['avg_ratings'] = ''.$avg.'';
					$filter['total_ratings'] = ''.$num_rating.'';
				}

				$categoryss = $filterdata['id_category'];
				//$filter['categorys'] = $categorys;
				//$categorys = array($categoryss);
				$categorys=explode(',',$categoryss);
				$filter['category'] = "";
				unset($elements);
				foreach ($categorys as $category)
				{
					$select1 = "select `category` from `g6t1u_jblance_category` WHERE `id`='".$category."'";
					$selectquery1 = mysql_query($select1);
					$filterdata1 = mysql_fetch_row($selectquery1);
					if(!empty($filterdata1)){
						$elements[] = implode(',', $filterdata1);
					}
				}

				if(!empty($elements)){
					$filter['category'] = implode(',', $elements);
				}
				

				if($rating!="")
				{
						if($avg<=$rating)
						{		
							$array_temp['Success'] = 'true';
							$array_temp['Message'] = 'tradesman found.';
							$array_temp1[]=$filter;
							$array_temp1 = array_unique($array_temp1, SORT_REGULAR);
							$array_temp['result'] = $array_temp1;
						}
						else
						{
							$array_temp['Success'] = 'false';
							$array_temp['Message'] = 'No tradesman found.';
						}
				}
				else
				{
					$array_temp['Success'] = 'true';
					$array_temp['Message'] = 'tradesman found.';
					$array_temp1[]=$filter;
					$array_temp1 = array_unique($array_temp1, SORT_REGULAR);
					$array_temp['result'] = $array_temp1;
				}
			}

			if(count($filter)=='0')
			{
				$array_temp['Success'] = 'false';
				$array_temp['Message'] = 'No tradesman found.';
			}
			else
			{
				if($rating=="")
				{
						$array_temp['Success'] = 'true';
						$array_temp['Message'] = 'tradesman found.';
						$array_temp1[]=$filter;
						$array_temp1 = array_unique($array_temp1, SORT_REGULAR);
						$array_temp['result'] = array_values($array_temp1);
				}
			}
	}

	$final_result = $array_temp;	
	echo json_encode($final_result);
?>