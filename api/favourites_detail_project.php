<?php
	include('config.php');
	date_default_timezone_set('Asia/Calcutta');
	
	$user_id = $_REQUEST['user_id'];
	$page_size 		= $_REQUEST['page_size'];
	$current_page 	= $_REQUEST['current_page'];
	$image_path = 'http://'.$_SERVER['SERVER_NAME'].'/images/jblance/profile/';

	if(@$page_size=='' || @$page_size==''){
		$page_size=10;
	}
	if(@$current_page=='' || @$current_page==''){
		$current_page=0;
	}else{
		$current_page=($current_page*$page_size)-$page_size;
	}

	if($user_id=="")
	{
		$array_temp['success'] = 'false';
		$array_temp['message'] = 'Required field missing user_id.';	
	}
	else
	{
		$select_project = "SELECT * FROM g6t1u_jblance_project WHERE publisher_userid = '".$user_id."' AND status = 'COM_JBLANCE_OPEN' ORDER BY id DESC LIMIT ".$current_page.", ".$page_size;
		$query_project = mysql_query($select_project);
		
		while($fetch_project = mysql_fetch_assoc($query_project))
		{
			$select_pic = "SELECT picture FROM g6t1u_jblance_user WHERE user_id = '".$user_id."' ";
			$query_pic = mysql_query($select_pic);
			$fetch_pic = mysql_fetch_assoc($query_pic);
			$pic['picture'] = ($fetch_pic['picture']) ? $image_path.$fetch_pic['picture'] : '';

			$id['project_id'] = $fetch_project['id'];
			$project_title['project_title'] = $fetch_project['project_title'];
			$description['description'] = $fetch_project['description'];

			$array_temp['success'] = 'true';
			$array_temp['message'] = 'project details';
			$array_temp['result'][] = array_merge($id,$project_title,$description,$pic);
		}
	}

	$final_result = $array_temp;
	echo json_encode($final_result);
?>