<?php
	include 'config.php';

	$final_result 		= array();
	$imagepath 		= 'http://'.$_SERVER['SERVER_NAME'].'/images/jblance/profile/';
	$default_imagepath	= 'http://'.$_SERVER['SERVER_NAME'].'/components/com_jblance/images/nophoto_sm.png';

	$user_id 			= $_REQUEST['user_id'];
	$keyword 			= $_REQUEST['keyword'];
	$name 				= $_REQUEST['name'];
	$user_name 			= $_REQUEST['user_name'];
	$country 			= $_REQUEST['country_id'];
	$city 				= $_REQUEST['city'];
	$rate 				= $_REQUEST['rate'];
	$ratings 			= $_REQUEST['ratings'];
	$skills 			= $_REQUEST['skills'];
	$page_size 			= $_REQUEST['page_size'];
	$current_page 			= $_REQUEST['current_page'];
	$skillsexplode 			= explode(',',$_REQUEST['skills']);
	$cat_num 			= count($skillsexplode);
	
	if(@$page_size=='' || @$page_size==''){
		$page_size=10;
	}
	if(@$current_page=='' || @$current_page==''){
		$current_page=0;
	}else{
		$current_page=($current_page*$page_size)-$page_size;
	}

	if($user_id == "")
	{
		$array_temp['Success'] = 'false';
		$array_temp['Message'] = 'Required field missing user_id.';		
	}
	else
	{

		if($keyword!="")
		{
			$and = " and b.name LIKE '%".$keyword."%' ";
		}

		if($country!="")
		{
			$and.= " and a.id_location = '".$country."' ";
		}

		if($city!="")
		{
			$and.= " and a.id_location = '".$city."' ";
		}

		if($user_name!="")
		{
			$and.= " and b.username LIKE '%".$user_name."%' ";
		}

		if($rate!="")
		{
			$and.= " and a.rate LIKE '%".$rate."%' ";
		}

		if($skills!="")
		{
			$and_cat1 = '';
			/*for($i=0;$i<$cat_num;$i++)
				{
					$and_cat[$i] = " AND a.id_category like '%".$skillsexplode[$i]."%' ";
					if($i>0)
					{

						$and_cat[$i] = " AND a.id_category like '%".$skillsexplode[$i]."%' ";
					}
					
					if(isset($_REQUEST['skills']))
					{
						$and.= " ".$and_cat[$i]."";
					}
				}*/

			foreach ($skillsexplode as $key=>$value) {

				if($and_cat1==''){
					if($value){
						$and_cat[$key] = " FIND_IN_SET(".$value.", a.id_category) ";
						$and_cat1 = " FIND_IN_SET(".$value.", a.id_category) ";
					}
				}else{
					if($value){
						$and_cat[$key] = " OR FIND_IN_SET(".$value.", a.id_category) ";
						$and_cat1.= " OR FIND_IN_SET(".$value.", a.id_category) ";
					}
				}
			}
					
			if(isset($_REQUEST['skills']) && $and_cat1!='')
			{
				$and.= " AND (".$and_cat1.") ";
			}
		}

		if($ratings!="")
		{
			$and.= " AND round((SELECT AVG((quality_clarity+communicate+expertise_payment+professional+hire_work_again)/5) FROM g6t1u_jblance_rating WHERE actor = a.user_id)) = round(IFNULL(".$ratings.",0)) ";
		}

		// if($rating!="")
		// {

		// 	$and.= "rating = '".$rating."'";

		// 	$select = "(SELECT AVG((quality_clarity+communicate+expertise_payment+professional+hire_work_again)/5) FROM g6t1u_jblance_rating
	 // 						WHERE $and) AS rating";
		// 	echo "(SELECT AVG((quality_clarity+communicate+expertise_payment+professional+hire_work_again)/5) FROM g6t1u_jblance_rating
	 // 						WHERE $and) AS rating";
		// 	echo "<pre>"; print_r($select);
		// }

		if($name!="")
		{
			
			if($name=="name_asc")
			{
				$and.= " ORDER BY b.name ASC ";
			}

			if($name=="name_desc")
			{
				$and.= " ORDER BY b.name DESC ";
			}
			
			if($name=="username_asc")
			{
				$and.= " ORDER BY b.username ASC ";
			}

			if($name=="username_desc")
			{
				$and.= " ORDER BY b.username DESC ";
			}
			
			if($name=="rate_asc")
			{
				$and.= " ORDER BY a.rate ASC ";
			}

			if($name=="rate_desc")
			{
				$and.= " ORDER BY a.rate DESC ";
			}

			if($name=="rating_asc")
			{
				
			}

			if($name=="rating_desc")
			{
				
			}
			
		}

$flag_rating=0;
$array_temp1=array();
	$k = 0;
		$select = "SELECT a.user_id,a.picture,a.ug_id,a.id_category,a.address,a.postcode,a.mobile,a.home,b.username,b.name,c.value as basic_info,d.title as location,a.biz_name as company_name FROM g6t1u_jblance_user as a inner join g6t1u_users as b on a.user_id=b.id inner join g6t1u_jblance_custom_field_value as c on a.user_id=c.userid left join g6t1u_jblance_location as d on a.id_location=d.id WHERE a.ug_id='2' and b.block=0 and c.fieldid='3' ". $and. " LIMIT ".$current_page.", ".$page_size;
		$selectquery = mysql_query($select);

			while($filterdata = mysql_fetch_assoc($selectquery))
			{
		
				$filter[$k] = $filterdata;
				$user_id_tm = $filter[$k]['user_id'];
				$filter[$k]['basic_info'] = utf8_encode($filter[$k]['basic_info']);
				$location_tm = $filter[$k]['location'];
				if(!isset($filter[$k]['location'])){
					$filter[$k]['location']="";
				}
				$usergroup = $filter[$k]['ug_id'];
				if ($filter[$k]['ug_id'] == 1) 
				{
					$filter[$k]['usergroup'] = "Tradesmen";
				}
				if ($filter[$k]['ug_id'] == 2) 
				{
					$filter[$k]['usergroup'] = "Company";
				}
				
				if ($filter[$k]['hourly_rate'] == "") 
				{
					$filter[$k]['hourly_rate'] = "";
				}
				
				$location 		= "SELECT title,path FROM g6t1u_jblance_location WHERE title='".$location_tm."' ";
				$location_query = mysql_query($location);
				$location_fetch = mysql_fetch_assoc($location_query);
				$location_name['location_name'] 	= $location_fetch['title'];
				$location_parent['parent_id'] 		= $location_fetch['parent_id'];
				$location_path['path'] 			= $location_fetch['path'];
				$country_name 				= explode('/',$location_path['path']);
				$filter[$k]['parent_location'] 		= $country_name[0];

				$select_online_user 	= mysql_query("SELECT * FROM g6t1u_session  WHERE userid='".$user_id_tm."' ");
				$num_ol_users 		= mysql_num_rows($select_online_user);


				if($num_ol_users>=1)
				{
					$filter[$k]['online_status'] = "online";
				}
				else
				{
					$filter[$k]['online_status'] = "offline";
				}

				if ($filterdata['mobile'] == '') {
					$filter[$k]['mobile'] = '';
				} else {
					$filter[$k]['mobile'] = $filterdata['mobile'];
				}

				$select_fav = "SELECT * FROM g6t1u_jblance_favourite WHERE actor= '".$user_id."' AND target= IFNULL('".$user_id_tm."','-1')";
				//echo "SELECT * FROM g6t1u_jblance_favourite WHERE actor= '".$user_id."' AND target= '".$user_id_tm."'" . '<br>';
				 $query_fav = mysql_query($select_fav);
				if(mysql_num_rows($query_fav)>0){
				while($fetch_fav = mysql_fetch_assoc($query_fav))
				{
                                        $filter[$k]['favourite'] = 0;
					//$fav_id = $fetch_fav['id'];
					$fav_user_id = $fetch_fav['target'];
					
					if ($user_id_tm == $fav_user_id) {
						$filter[$k]['favourite'] = 1;
					}
				}
                                }else{ $filter[$k]['favourite'] = 0; }

				$select_rating = mysql_query("SELECT * FROM g6t1u_jblance_rating WHERE actor = '".$user_id_tm."' ");
				$num_rating = mysql_num_rows($select_rating);
				$select_rating_avg = 
						mysql_query("	SELECT 
										AVG(quality_clarity) AS quality_clarity_avg,
										AVG(communicate) AS communicate_avg,
										AVG(expertise_payment) AS expertise_payment_avg,
										AVG(professional) AS professional_avg,
										AVG(hire_work_again) AS hire_work_again_avg 
										FROM g6t1u_jblance_rating 
										WHERE actor = '".$user_id_tm."'
									");

				
				$fetch_communicate_rating = mysql_fetch_assoc($select_rating_avg);

				$quality_clarity_avg = 
				$fetch_communicate_rating['quality_clarity_avg'];

				$communicate_avg = 
				$fetch_communicate_rating['communicate_avg'];

				$expertise_payment_avg = 
				$fetch_communicate_rating['expertise_payment_avg'];

				$professional_avg = 
				$fetch_communicate_rating['professional_avg'];

				$hire_work_again_avg = 
				$fetch_communicate_rating['hire_work_again_avg'];
			
				$quality_clarity = round($quality_clarity_avg);	
				$communicate = round($communicate_avg);	
				$expertise_payment = round($quality_clarity_avg);	
				$professional = round($quality_clarity_avg);	
				$hire_work_again = round($quality_clarity_avg);	

				if($num_rating>=1)
				{
					$avg = ($quality_clarity + $communicate + $expertise_payment + $professional + $hire_work_again)/5;
					$filter[$k]['avg_ratings'] = $avg;	
				}
					
				if($filter[$k]['picture']=="")
				{
					$filter[$k]['picture'] = $default_imagepath;
				}
				else
				{
					$filter[$k]['picture'] = ($filter[$k]['picture']) ? $imagepath.$filter[$k]['picture'] : $default_imagepath;
				}

				if($num_rating=="0")
				{
					$filter[$k]['avg_ratings'] = "";
					$filter[$k]['total_ratings'] = "";
				}
				else
				{
					$filter[$k]['avg_ratings'] = ''.$avg.'';
					$filter[$k]['total_ratings'] = ''.$num_rating.'';
				}

				$categoryss = $filterdata['id_category'];
				//$filter[$k]['categorys'] = $categorys;
				//$categorys = array($categoryss);
				$categorys=explode(',',$categoryss);
				$filter[$k]['category'] = "";
				unset($elements);
				foreach ($categorys as $category)
				{
					$select1 = "select `category` from `g6t1u_jblance_category` WHERE `id`='".$category."'";
					$selectquery1 = mysql_query($select1);
					$filterdata1 = mysql_fetch_row($selectquery1);
					if(!empty($filterdata1)){
						$elements[] = implode(',', $filterdata1);
					}
				}

				if(!empty($elements)){
					$filter[$k]['category'] = implode(',', $elements);
				}


	        	$likeStatusQuery = mysql_query("SELECT status FROM g6t1u_users_follow WHERE user_id=".$user_id_tm." AND follower=".$user_id);
				$row_val = mysql_fetch_assoc($likeStatusQuery);
				$filter[$k]['follow_status'] = $row_val['status'];


				//$array_temp1=array_merge($array_temp1,$filter[$k]);
				
				if($rating!="")
				{ 
					if($avg<=$rating)
					{		
						$flag_rating=1;
					}
				}
				else
				{ 
					$flag_rating=1;
				}

				/*if($rating!="")
				{ 
						if($avg<=$rating)
						{		
							$array_temp['Success'] = 'true';
							$array_temp['Message'] = 'company found.';
							$array_temp1=$filter[$k];
							//$array_temp1 = array_unique($filter[$k], SORT_REGULAR);
							$array_temp['result'] = array($array_temp1);
						}
						else
						{
							$array_temp['Success'] = 'false';
							$array_temp['Message'] = 'No company found.';
						}
				}
				else
				{ 
					$array_temp['Success'] = 'true';
					$array_temp['Message'] = 'company found.';
					$array_temp1 = $filter[$k]; print_R($array_temp1);
					//$array_temp1 = array_unique($filter[$k], SORT_REGULAR);
					$array_temp['result'] = array($array_temp1);
				}*/
				$k++;
			}

			if(count($filter)=='0')
			{
				$array_temp['Success'] = 'false';
				$array_temp['Message'] = 'No company found.';
			}
			else
			{ 
				if($rating=="" || $flag_rating==1)
				{ 
						$array_temp['Success'] = 'true';
						$array_temp['Message'] = 'company found.';
						$array_temp1 = $filter;
						//$array_temp1 = array_unique($filter, SORT_REGULAR);
						$array_temp['result'] = $array_temp1;
				}else{
					$array_temp['Success'] = 'false';
					$array_temp['Message'] = 'No company found.';
				}
			}
	}

	$final_result = $array_temp;	
	echo json_encode($final_result);
?>