<?php

// Address

$address = $_REQUEST['address'];

// Get JSON results from this request

$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

// Convert the JSON to an array

$geo = json_decode($geo, true);



if ($geo['status'] == 'OK') 
{
	  $result['success'] = "true";
	  $result['message'] = "latitude and longitude of address.";

	  // Get Lat & Long
	  $latitude = $geo['results'][0]['geometry']['location']['lat'];

	  $longitude = $geo['results'][0]['geometry']['location']['lng'];

	  $add = $geo['results'][0]['formatted_address'];

	  $result['result']['address'] = "".$add."";

	  $result['result']['lat'] = "".$latitude."";

	  $result['result']['lon'] = "".$longitude."";

    
}
else
{
	$result['success'] = "false";
	$result['message'] = "No result found.";
}

echo json_encode($result);

?>