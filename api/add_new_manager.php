<?php

include('config.php');

function check_email_address($email) {
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
        return false;
    }

    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
        if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
            return false;
        }
    }

    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
            return false;
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
                return false;
            }
        }
    }

    return true;
}

date_default_timezone_set("Asia/Kolkata");
$name = $_REQUEST['name'];
$username = $_REQUEST['username'];
$parent_company = @$_REQUEST['parent_company'];
$email = $_REQUEST['email'];
$password = $_REQUEST['password'];
$encryted_pass = md5($password);
$device_id = $_REQUEST['device_id'];
$registration_id = @$_REQUEST['registration_id'];
$device_type = $_REQUEST['device_type'];


/* for tradesmen user */

$user_plan = "SELECT * FROM g6t1u_jblance_plan where id=1";
$user_planquery = mysql_query($user_plan);
$user_plannum = mysql_num_rows($user_planquery);

$user_plandata = mysql_fetch_assoc($user_planquery);

$user_plandata_name = $user_plandata['name'];
$user_plandata_days = $user_plandata['days'];
$user_plandata_days_type = $user_plandata['days_type'];
$user_plandata_params = $user_plandata['params'];
$user_plandata_params_array = json_decode($user_plandata_params, true);
$user_plandata_params_array_portfolio_count = $user_plandata_params_array['portfolioCount'];
$user_plandata_params_array_featured_fee = $user_plandata_params_array['buyFeePerFeaturedProject'];

/* end tradesmen user */

/* for Company User */

$company_plan = "SELECT * FROM g6t1u_jblance_plan where id=3";
$company_planquery = mysql_query($company_plan);
$company_plannum = mysql_num_rows($company_planquery);

$company_plandata = mysql_fetch_assoc($company_planquery);

$company_plandata_name = $company_plandata['name'];
$company_plandata_days = $company_plandata['days'];
$company_plandata_days_type = $company_plandata['days_type'];
$company_plandata_params = $company_plandata['params'];
$company_plandata_params_array = json_decode($company_plandata_params, true);
$company_plandata_params_array_portfolio_count = $company_plandata_params_array['buyProjectCount'];
$company_plandata_params_array_featured_fee = $company_plandata_params_array['buyFeePerFeaturedProject'];

/* end company user */


    $group_id = 2;
    $plan_id = 3;
    $approved = 1;
    $acceess_count = 1;
    $gateway_id = rand(1451111111, 1459999999);
    $date_buy = $date = date("Y-m-d h:i:s");
    $date_approvel = $date = date("Y-m-d h:i:s");
    $date_expire = $date = date("Y-m-d h:i:s", strtotime("+90 days"));
    $company_plandata_params_array_project_count = $company_plandata_params_array['buyProjectCount'];
    $group = 'Company';


$date = date("Y-m-d h:i:s");
$final_result = array();


$Isexist = mysql_query('SELECT id FROM g6t1u_users WHERE email="' . $email . '" OR username="' . $username . '"');

$num_user = mysql_num_rows($Isexist);

if ($name == '') {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Missing required field name.';
} else if ($username == '') {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Missing required field username.';
} else if ($email == '') {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Missing required field email.';
} else if (!check_email_address($email)) {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Invalid email.';
} else if ($password == '') {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Missing required field password.';
} else if ($num_user > 0) {
    $array_temp['Success'] = 'false';
    $array_temp['Message'] = 'Username or Email address already exist.';
} else {

    if ($username != '' && $email != '') {
        $code = rand(10000, 99999);


        $query = 'INSERT INTO g6t1u_users (name,username,block,email,password,registerDate,params,code_verification, `group`,parent_company ) VALUES ("' . $name . '","' . $username . '","0","' . $email . '","' . $encryted_pass . '","' . $date . '","","' . $code . '","' . $group . '","' . $parent_company . '")';
        $insert = mysql_query($query);
        $last = mysql_insert_id();

        
        $activelink = 'http://' . $_SERVER['HTTP_HOST'] . '/activate.php?email=' . $email . '&code=' . $code;
        $subject = "Tradesmen Networking E-mail Confirmation";
        $message = "Please Click on below URL and get registered in Tradesmen Networking.<br/>";
        $message .= "URL:" . $activelink . "<br/>";
        $headers = "MIME-Version:1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $mail = mail($email, $subject, $message, $headers);


        if (mysql_affected_rows() == 1) { 
            $grpassign = 'INSERT INTO g6t1u_user_usergroup_map (user_id,group_id) VALUES ("' . $last . '","2")';
            $grpassign_query = mysql_query($grpassign);
        }

        if (mysql_affected_rows() == 1) {
            $delete = "DELETE FROM g6t1u_tmp_push_notification WHERE user_id = $last OR device_id = '$device_id';";
            $deletequery = mysql_query($delete);

            $insertPush = "insert into g6t1u_tmp_push_notification (user_id,device_id,registration_id,device_type) values ('$last','$device_id','$registration_id','$device_type')";
            $insertPushQuery = mysql_query($insertPush);
        }

        if (mysql_affected_rows() == 1) {
            $invoice = "SELECT invoiceNo FROM g6t1u_jblance_plan_subscr ORDER BY g6t1u_jblance_plan_subscr.id DESC 
                        LIMIT 1";
            $invoicequery = mysql_query($invoice);
            $invoicenum = mysql_num_rows($invoicequery);

            $invoicedata = mysql_fetch_assoc($invoicequery);
            $invoiceno = $invoicedata['invoiceNo'];

            $invoiceno_data = explode("-", $invoiceno);
            $invoiceno_data_no = $invoiceno_data[2];
            $invoiceno_data_no2 = $invoiceno_data_no + 1;

            $invoiceno_data_new = $invoiceno_data[0] . '-' . $invoiceno_data[1] . '-' . $invoiceno_data_no2;
            $planassign = 'INSERT INTO g6t1u_jblance_plan_subscr (user_id,plan_id,approved,access_count,gateway_id,date_buy,date_approval,date_expire,invoiceNo,projects_allowed) VALUES ("' . $last . '","' . $plan_id . '","' . $approved . '","' . $acceess_count . '","' . $gateway_id . '","' . $date_buy . '","' . $date_approvel . '","' . $date_expire . '","' . $invoiceno_data_new . '","' . $company_plandata_params_array_project_count . '")';
            $planassign_query = mysql_query($planassign);
        }
        if (mysql_affected_rows() == 1) {
            $genregister = 'INSERT INTO g6t1u_jblance_custom_field_value (fieldid,userid,value) VALUES ("2","' . $last . '","")';
            $genregisterquery = mysql_query($genregister);
        }
        if (mysql_affected_rows() == 1) {
            $basicregister = 'INSERT INTO g6t1u_jblance_custom_field_value (fieldid,userid,value) VALUES ("3","' . $last . '","")';
            $basicregisterquery = mysql_query($basicregister);
        }
        if (mysql_affected_rows() == 1) { 
            $userquery = 'INSERT INTO g6t1u_jblance_user (user_id, ug_id, featured) VALUES ("' . $last . '", "' . $group_id . '", "0")';
            $userinsert = mysql_query($userquery);
            if (mysql_affected_rows() == 1) {
                $findgroup = "select a.picture,b.name from g6t1u_jblance_user as a inner join g6t1u_jblance_usergroup as b on a.ug_id=b.id where a.user_id='" . $last . "'";
                $findgroupquery = mysql_query($findgroup);
                $groupname = mysql_fetch_assoc($findgroupquery);
                $group = $groupname['name'];
                $imagepath = $_SERVER['SERVER_NAME'] . '/images/jblance/profile/';

                $select = "select a.id,a.name,a.username,a.email,a.registerDate,b.picture from g6t1u_users as a inner join g6t1u_jblance_user as b on a.id=b.user_id where a.id='" . $last . "' and b.user_id='" . $last . "'";
                $selectquery = mysql_query($select);
                $lastuser = mysql_fetch_assoc($selectquery);

                $array_temp1['userid'] = $lastuser['id'];
                $array_temp1['name'] = $lastuser['name'];
                $array_temp1['username'] = $lastuser['username'];
                $array_temp1['useremail'] = $lastuser['email'];

                if ($lastuser['picture'] == "") {
                    $array_temp1['image'] = $imagepath . "profile.png";
                } else {
                    $array_temp1['image'] = $imagepath . $lastuser['picture'];
                }
                $array_temp1['registerDate'] = $lastuser['registerDate'];
                $array_temp1['usergroup'] = $group;

                $array_temp['Success'] = 'true';
                $array_temp['Message'] = 'You are registered successfully to Tradesmen Networking.';
                $array_temp['result'] = $array_temp1;
            }
        }
    }
}
$final_result = $array_temp;
echo json_encode($final_result);

?>