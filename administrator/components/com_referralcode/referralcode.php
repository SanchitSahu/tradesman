<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

$user = JFactory::getUser();


JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

/*$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));*/
$loggeduser = JFactory::getUser();
//$sidebar = JHtmlSidebar::render();


		// Display the cpanel modules
		//$modules = JModuleHelper::getModules('cpanel');

?>
<form action="<?php echo JRoute::_('index.php?option=com_users&view=referralcode');?>" method="post" name="adminForm" id="adminForm">
	<?php /*if (!empty( $sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
		<?php echo $sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; */ ?>
	<div class="row-fluid">
		<?php $iconmodules = JModuleHelper::getModules('icon');
		if ($iconmodules) : ?>
			<div class="span3">
				<div class="cpanel-links">
					<?php
					// Display the submenu position modules
					foreach ($iconmodules as $iconmodule)
					{
						echo JModuleHelper::renderModule($iconmodule);
					}
					?>
				</div>
			</div>
		<?php endif; ?>
		<div class="span<?php echo ($iconmodules) ? 9 : 12; ?>">
			<?php if ($user->authorise('core.manage', 'com_postinstall')) : ?>
				<div class="row-fluid">
					<?php /*if ($this->postinstall_message_count): ?>
						<div class="alert alert-info">
						<h4>
							<?php echo JText::_('COM_CPANEL_MESSAGES_TITLE'); ?>
						</h4>
						<p>
							<?php echo JText::_('COM_CPANEL_MESSAGES_BODY_NOCLOSE'); ?>
						</p>
						<p>
							<?php echo JText::_('COM_CPANEL_MESSAGES_BODYMORE_NOCLOSE'); ?>
						</p>
						<p>
							<a href="index.php?option=com_postinstall&amp;eid=700" class="btn btn-primary">
							<?php echo JText::_('COM_CPANEL_MESSAGES_REVIEW'); ?>
							</a>
						</p>
						</div>
					<?php endif; */?>
				</div>
			<?php endif; ?>
			<div class="row-fluid">
				<?php
				/*$spans = 0;

				foreach ($modules as $module)
				{
					// Get module parameters
					$params = new Registry;
					$params->loadString($module->params);
					$bootstrapSize = $params->get('bootstrap_size');
					if (!$bootstrapSize)
					{
						$bootstrapSize = 12;
					}
					$spans += $bootstrapSize;
					if ($spans > 12)
					{
						echo '</div><div class="row-fluid">';
						$spans = $bootstrapSize;
					}
					echo JModuleHelper::renderModule($module, array('style' => 'well'));
				}
				*/

				array(
						'a.code' => JText::_('COM_REFFERAL_CODE_HEADING_CODE'),
						'a.title' => JText::_('COM_REFFERAL_CODE_HEADING_TITLE'),
						'a.company' => JText::_('COM_REFFERAL_CODE_COMPANY'),
						'a.tradesmen' => JText::_('COM_REFFERAL_CODE_TRADESMEN'),
						'a.description' => JText::_('COM_REFFERAL_CODE_DESCRIPTION'),
						'a.id' => JText::_('COM_REFFERAL_CODE_ID')
				);

        		$db = JFactory::getDbo();

				$query = "SELECT * FROM #__referralcode";
		 		$db->setQuery($query);
		 		$refferals = $db->loadObjectList();
				?>
				<div class="tab-pane active" id="1">
				    <span class="msg_success hide"><!-- Messages --></span>
					<fieldset>
						<legend class="edit-profile-heading"><?php echo "Refferal Code" ; ?></legend>
						<div class="profile-table-box">
							<table class="table table-striped">
								<thead>
									<th>Code</th>
									<th>Title</th>
									<th>Description</th>
									<th>Company</th>
									<th>Tradesmen</th>
									<th>Action</th>
								</thead>
								<tbody>
								<?php foreach($refferals as $refferalrow) { ?>
									<tr class="manager_<?php echo $refferalrow->id;?>">
										<td>
											<h4>
												<?php //echo LinkHelper::GetProfileLink($refferalrow->id, $refferalrow->name); 
												echo $refferalrow->code;?>
											</h4>
										</td>
										<td><h4><?php echo $refferalrow->title?></h4></td>
										<td><h4><?php echo $refferalrow->description?></h4></td>
										<td><h4><?php echo $refferalrow->company?></h4></td>
										<td><h4><?php echo $refferalrow->tradesmen?></h4></td>
										<td><a for="<?php echo $refferalrow->id;?>" onclick="DeleteReferral(this);">Delete</a></td>
									</tr>
								<?php } ?>
										
								</tbody>
							</table>
						</div>
					</fieldset>
					</div>


			</div>
		</div>
	</div>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	<?php /* </div> */ ?>
</form>